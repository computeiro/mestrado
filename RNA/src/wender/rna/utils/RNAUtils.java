package wender.rna.utils;

import java.util.Arrays;

public class RNAUtils {

	public static double[] sorteiaArray(int size){
		double[] arr = new double[size];
		
		for(int i = 0; i < size; i++){
			arr[i] = sorteiaPeso();
		}
		
		return arr;
	}
	
	public static double sorteiaPeso(){
		double rnd = 0;
		
		while(rnd == 0){
			rnd = Math.random();
		}
		
		//0.5 <= rnd < 0
		return rnd / 2;
	}
	
	public static void fill(Object arrOrMatrix, double value){
		if(arrOrMatrix instanceof Object[]){
			Object[]  arr = (Object[]) arrOrMatrix;
			
			for(int i = 0; i < arr.length; i++){
				fill(arr[i], value);
			}
		}else if(arrOrMatrix instanceof double[]){
			Arrays.fill((double[]) arrOrMatrix, value);
		}
	}
	
	public static void fillStb(StringBuilder stb, Object arrOrMatrix){
		if(arrOrMatrix instanceof Object[]){
			stb.append("\n{");
			Object[]  arr = (Object[]) arrOrMatrix;
			
			for(int i = 0; i < arr.length; i++){
				fillStb(stb, arr[i]);
				
				if(i+1 < arr.length){
					stb.append(", ");
				}
				
			}
			
			stb.append("\n}");
		}else if(arrOrMatrix instanceof double[]){
			stb.append(Arrays.toString((double[]) arrOrMatrix));
		}
	}
	
	public static double[][] clone(double[][] matrix){
		double[][] clone = new double[matrix.length][];
		 
		for(int i = 0; i < matrix.length; i++){
			clone[i] = new double[matrix[i].length];
			
			for(int j = 0; j < matrix[i].length; j++){
				clone[i][j] = matrix[i][j];
			}
		}
		
		
		return clone;
	}
	
	
	public static void printArrOrMatrix(Object arrOrMatrix){
		StringBuilder stb = new StringBuilder();
		fillStb(stb, arrOrMatrix);
		System.out.println(stb.toString());
	}

	
	
}
