package wender.rna.utils;

import javax.swing.JFrame;

import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import wender.rna.functions.ActivationFunction;
import wender.rna.functions.SigmoidBipolar;
import wender.rna.trab07.ui.treino.XYChart;

public class GraficoErroTreinamento implements ErroPorEpocaListener, Runnable{
	private XYSeries errSerie;
	private XYChart xyc;
	private String title;
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
	}

	@Override
	public void update(int epoca, double erro) {
		errSerie.add(epoca, erro);
	}
	
	
	public GraficoErroTreinamento(){
		
		ActivationFunction act = new SigmoidBipolar();
		
		
		final XYSeriesCollection dataset = new XYSeriesCollection();
		errSerie = new XYSeries("Erros");
		dataset.addSeries(errSerie);
		
		if(title == null){
			title = "Erro no decorrer das epocas do treinamento";
		}
		
		xyc = new XYChart(title, dataset, 800, 600);
		
		Thread t = new Thread(this);
		t.start();
		
	}
	
	public void run(){
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(800, 600);
		frame.add(xyc.getChartPanel());
		
		frame.setVisible(true);
	}

}
