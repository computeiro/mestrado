package wender.rna.utils;

public interface ErroPorEpocaListener {
	public void update(int epoca, double erro);
}
