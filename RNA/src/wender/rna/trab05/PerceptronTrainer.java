package wender.rna.trab05;


public class PerceptronTrainer extends Trainer {

	public String getDescription(){
		return "Perceptron";
	}

	public void treinar() {
		int y;
		boolean treinou = false;

		while (!treinou && epoca <= maxEpoca) {
			epoca++;

			for (int i = 0; i < amostras.length; i++) {
				y = Trainer.calculaY(amostras[i], pesos, bias, limiar);

				if (y != target[i]) {
					atualizaPesos(i, y, target[i]);
					treinou = false;
					break;
				} else {
					treinou = true;
				}

			}

		}

	}

	private void atualizaPesos(int amostraIndx, int saida, int target) {
		
		for (int i = 0; i < pesos.length; i++) {
			
			for (int j = 0; j < pesos[i].length; j++) {
				pesos[i][j] = pesos[i][j] + alfa * target * amostras[amostraIndx][i][j];
			}
		}

		bias = bias + alfa * target;

	}

}
