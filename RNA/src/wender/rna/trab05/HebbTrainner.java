package wender.rna.trab05;

public class HebbTrainner extends Trainer {

	public String getDescription() {
		return "Hebb";
	}

	@Override
	public void treinar() {

		for (int a = 0; a < amostras.length; a++) {
			for (int l = 0; l < Trainer.LINHAS; l++) {
				for (int c = 0; c < Trainer.COLUNAS; c++) {
					pesos[l][c] = pesos[l][c] + amostras[a][l][c] * target[a];
				}

			}

			bias += target[a];
		}

	}
}
