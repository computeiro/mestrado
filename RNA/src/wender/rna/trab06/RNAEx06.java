package wender.rna.trab06;

import wender.rna.ui.MatrixConstants;

public class RNAEx06 {
	private int limiar = 0;
	private int[][][] amostras;
	private int[][] targets;
	private double alfa;
	
	private final NeuronPerceptronEx6[] neuron = new NeuronPerceptronEx6[10];
	
	public RNAEx06(int[][][] amostras, int[][] targets, int limiar, double alfa){
		this.limiar = limiar;
		this.amostras = amostras;
		this.targets = targets;
		this.alfa = alfa;
	}
	
	
	public void efetuaTreino(){
		
		for(int n = 0; n < neuron.length; n++){
			neuron[n] = new NeuronPerceptronEx6();
			neuron[n].treinar(amostras, targets[n], limiar, alfa);
		}
	}
	
	public int[] calculaY(int[][] amostra){
		int[] y = new int[neuron.length];
		
		for(int n = 0; n < neuron.length; n ++){
			y[n] = neuron[n].calculaY(amostra);
		}
		
		return y;
	}
	
}
