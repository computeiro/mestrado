package wender.rna.trab06;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import wender.rna.trab04.Perceptron;
import wender.rna.ui.CharMatrix;
import wender.rna.ui.MatrixConstants;

import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

public class Trab06App {
	private final int LIMIAR = 1;

	private JFrame frame;
		
	private JButton btnNro1;
	private JButton btnNro2;
	private JButton btnNro3;
	private JButton btnNro4;
	private JButton btnNro5;
	private JButton btnNro6;
	private JButton btnNro7;
	private JButton btnNro8;
	private JButton btnNro9;
	private JButton btnNro0;
	
	private CharMatrix cMatriz;
	
	private JTextField txBias;
	private JTextField txAlfa;
	
	private JButton btnTreinar;
	private JButton btnTestar;
	
	private JTextField txNro0;
	private JTextField txNro1;
	private JTextField txNro2;
	private JTextField txNro3;
	private JTextField txNro4;
	private JTextField txNro5;
	private JTextField txNro6;
	private JTextField txNro7;
	private JTextField txNro8;
	private JTextField txNro9;
	
	private RNAEx06 rede;
	
	private final int[][][] amostras = new int[][][]{
			MatrixConstants.NRO0, MatrixConstants.NRO1, MatrixConstants.NRO2,
			MatrixConstants.NRO3, MatrixConstants.NRO4, MatrixConstants.NRO5, 
			MatrixConstants.NRO6, MatrixConstants.NRO7, MatrixConstants.NRO8, MatrixConstants.NRO9};
	
	private final int[][] targets = new int[][]{
			{ 1, -1, -1, -1, -1, -1, -1, -1, -1, -1,},
			{-1,  1, -1, -1, -1, -1, -1, -1, -1, -1,},
			{-1, -1,  1, -1, -1, -1, -1, -1, -1, -1,},
			{-1, -1, -1,  1, -1, -1, -1, -1, -1, -1,},
			{-1, -1, -1, -1,  1, -1, -1, -1, -1, -1,},
			{-1, -1, -1, -1, -1,  1, -1, -1, -1, -1,},
			{-1, -1, -1, -1, -1, -1,  1, -1, -1, -1,},
			{-1, -1, -1, -1, -1, -1, -1,  1, -1, -1,},
			{-1, -1, -1, -1, -1, -1, -1, -1,  1, -1,},
			{-1, -1, -1, -1, -1, -1, -1, -1, -1,  1,},
	};
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Trab06App window = new Trab06App();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Trab06App() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 417, 397);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setTitle("Wender - RNA 06");
		frame.getContentPane().setLayout(null);
		
		cMatriz = new CharMatrix(Perceptron.LINHAS, Perceptron.COLUNAS);
		cMatriz.setBounds(183, 11, 140, 180);
		frame.getContentPane().add(cMatriz);
		
		txNro0 = new JTextField();
		txNro0.setHorizontalAlignment(SwingConstants.CENTER);
		txNro0.setText("0");
		txNro0.setEditable(false);
		txNro0.setColumns(10);
		txNro0.setBounds(346, 11, 34, 20);
		frame.getContentPane().add(txNro0);
		
		txNro1 = new JTextField();
		txNro1.setText("1");
		txNro1.setHorizontalAlignment(SwingConstants.CENTER);
		txNro1.setEditable(false);
		txNro1.setColumns(10);
		txNro1.setBounds(346, 42, 34, 20);
		frame.getContentPane().add(txNro1);
		
		txNro2 = new JTextField();
		txNro2.setText("2");
		txNro2.setHorizontalAlignment(SwingConstants.CENTER);
		txNro2.setEditable(false);
		txNro2.setColumns(10);
		txNro2.setBounds(346, 73, 34, 20);
		frame.getContentPane().add(txNro2);
		
		txNro3 = new JTextField();
		txNro3.setText("3");
		txNro3.setHorizontalAlignment(SwingConstants.CENTER);
		txNro3.setEditable(false);
		txNro3.setBounds(346, 104, 34, 20);
		frame.getContentPane().add(txNro3);
		txNro3.setColumns(10);
		
		txNro4 = new JTextField();
		txNro4.setText("4");
		txNro4.setHorizontalAlignment(SwingConstants.CENTER);
		txNro4.setEditable(false);
		txNro4.setColumns(10);
		txNro4.setBounds(346, 135, 34, 20);
		frame.getContentPane().add(txNro4);
		
		txNro5 = new JTextField();
		txNro5.setText("5");
		txNro5.setHorizontalAlignment(SwingConstants.CENTER);
		txNro5.setEditable(false);
		txNro5.setColumns(10);
		txNro5.setBounds(346, 166, 34, 20);
		frame.getContentPane().add(txNro5);
		
		txNro6 = new JTextField();
		txNro6.setText("6");
		txNro6.setHorizontalAlignment(SwingConstants.CENTER);
		txNro6.setEditable(false);
		txNro6.setColumns(10);
		txNro6.setBounds(346, 197, 34, 20);
		frame.getContentPane().add(txNro6);
		
		txNro7 = new JTextField();
		txNro7.setText("7");
		txNro7.setHorizontalAlignment(SwingConstants.CENTER);
		txNro7.setEditable(false);
		txNro7.setColumns(10);
		txNro7.setBounds(346, 228, 34, 20);
		frame.getContentPane().add(txNro7);
		
		txNro8 = new JTextField();
		txNro8.setText("8");
		txNro8.setHorizontalAlignment(SwingConstants.CENTER);
		txNro8.setEditable(false);
		txNro8.setColumns(10);
		txNro8.setBounds(346, 259, 34, 20);
		frame.getContentPane().add(txNro8);
		
		txNro9 = new JTextField();
		txNro9.setText("9");
		txNro9.setHorizontalAlignment(SwingConstants.CENTER);
		txNro9.setEditable(false);
		txNro9.setColumns(10);
		txNro9.setBounds(346, 290, 34, 20);
		frame.getContentPane().add(txNro9);
		
		btnNro1 = new JButton("1");
		btnNro1.setBounds(10, 11, 42, 23);
		frame.getContentPane().add(btnNro1);
		btnNro1.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				cMatriz.fill(MatrixConstants.NRO1);
				
			}
		});
		
		
		btnNro2 = new JButton("2");
		btnNro2.setBounds(59, 11, 42, 23);
		frame.getContentPane().add(btnNro2);
		btnNro2.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				cMatriz.fill(MatrixConstants.NRO2);
				
			}
		});
		
		btnNro3 = new JButton("3");
		btnNro3.setBounds(108, 11, 42, 23);
		frame.getContentPane().add(btnNro3);
		btnNro3.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				cMatriz.fill(MatrixConstants.NRO3);
				
			}
		});
		
		btnNro4 = new JButton("4");
		btnNro4.setBounds(10, 45, 42, 23);
		frame.getContentPane().add(btnNro4);
		btnNro4.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				cMatriz.fill(MatrixConstants.NRO4);
				
			}
		});
		
		btnNro5 = new JButton("5");
		btnNro5.setBounds(59, 45, 42, 23);
		frame.getContentPane().add(btnNro5);
		btnNro5.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				cMatriz.fill(MatrixConstants.NRO5);
				
			}
		});
		
		btnNro6 = new JButton("6");
		btnNro6.setBounds(108, 45, 42, 23);
		frame.getContentPane().add(btnNro6);
		btnNro6.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				cMatriz.fill(MatrixConstants.NRO6);
				
			}
		});
		
		btnNro7 = new JButton("7");
		btnNro7.setBounds(10, 79, 42, 23);
		frame.getContentPane().add(btnNro7);
		btnNro7.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				cMatriz.fill(MatrixConstants.NRO7);
				
			}
		});
		
		btnNro8 = new JButton("8");
		btnNro8.setBounds(59, 79, 42, 23);
		frame.getContentPane().add(btnNro8);
		btnNro8.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				cMatriz.fill(MatrixConstants.NRO8);
				
			}
		});
		
		btnNro9 = new JButton("9");
		btnNro9.setBounds(108, 79, 42, 23);
		frame.getContentPane().add(btnNro9);
		btnNro9.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				cMatriz.fill(MatrixConstants.NRO9);
				
			}
		});
		
		btnNro0 = new JButton("0");
		btnNro0.setBounds(59, 114, 42, 23);
		frame.getContentPane().add(btnNro0);
		btnNro0.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				cMatriz.fill(MatrixConstants.NRO0);
				
			}
		});
		
		
		JLabel lbBias = new JLabel("Bias:");
		lbBias.setHorizontalAlignment(SwingConstants.RIGHT);
		lbBias.setBounds(10, 228, 103, 20);
		frame.getContentPane().add(lbBias);
		
		txBias = new JTextField();
		txBias.setHorizontalAlignment(SwingConstants.RIGHT);
		txBias.setColumns(10);
		txBias.setBounds(116, 228, 34, 20);
		txBias.setText("1");
		frame.getContentPane().add(txBias);
		
		JLabel lbAlfa = new JLabel("Tx. Aprendizado:");
		lbAlfa.setHorizontalAlignment(SwingConstants.RIGHT);
		lbAlfa.setBounds(10, 258, 103, 20);
		frame.getContentPane().add(lbAlfa);
		
		txAlfa = new JTextField();
		txAlfa.setHorizontalAlignment(SwingConstants.RIGHT);
		txAlfa.setColumns(10);
		txAlfa.setBounds(116, 259, 34, 20);
		txAlfa.setText("0.1");
		frame.getContentPane().add(txAlfa);
		
		btnTreinar = new JButton("Treinar");
		btnTreinar.setBounds(10, 289, 140, 23);
		frame.getContentPane().add(btnTreinar);
		btnTreinar.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				treinarClicked();
			}
		});
		
		btnTestar = new JButton("Testar");
		btnTestar.setBounds(183, 289, 140, 23);
		btnTestar.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				testarClicked();
			}
		});
		
		frame.getContentPane().add(btnTestar);
		
	}
	
	private void treinarClicked(){
		double alfa;
		
		try{
			alfa = Double.valueOf(txAlfa.getText());
		}catch(Exception e){
			JOptionPane.showMessageDialog(frame, "Preencha o campo 'Taxa de aprendizagem' com um valor inteiro ou decimal v�lido (separador decimal = '.')", "Taxa de aprendizagem inv�lida!", JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		
		rede = new RNAEx06(amostras, targets, LIMIAR, alfa);
		
		JTextField[] flags = {txNro0, txNro1, txNro2, txNro3, txNro4, txNro5, txNro6, txNro7, txNro8,  txNro9};
		for(int i = 0; i < flags.length; i++){
			flags[i].setBackground(Color.GRAY);
		}
		
		rede.efetuaTreino();
	}
	
	private void testarClicked(){
		
		JTextField[] flags = {txNro0, txNro1, txNro2, txNro3, txNro4, txNro5, txNro6, txNro7, txNro8,  txNro9};
		int[] saida = rede.calculaY(cMatriz.getValue());
		
		for(int i = 0; i < saida.length; i++){
			flags[i].setBackground(saida[i] == 1 ? Color.GREEN : Color.RED);
		}
	}
}
