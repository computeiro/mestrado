package wender.rna.trab06;

import wender.rna.ui.MatrixConstants;


public class NeuronPerceptronEx6 {
	public static final int COLUNAS = 7;
	public static final int LINHAS = 9;

	private int[][][] amostras;
	private double[][] pesos;
	private int[] targets;
	
	private double bias;
	private int epoca ;
	private double alfa;
	private int maxEpoca = 999;
	private double limiar;

	public NeuronPerceptronEx6(){
		pesos = new double[LINHAS][COLUNAS];
	}
	
	public void treinar(int[][][] amostras, int[] targets, double limiar, double alfa) {
		this.amostras = amostras;
		this.targets = targets;
		this.limiar = limiar;
		this.alfa = alfa;
		
		int y;
		boolean treinou = false;

		while (!treinou && epoca <= maxEpoca) {
			epoca++;

			for (int i = 0; i < amostras.length; i++) {
				y = calculaY(amostras[i]);

				if (y != this.targets[i]) {
					atualizaPesos(i, y, this.targets[i]);
					treinou = false;
					break;
				} else {
					treinou = true;
				}

			}

		}
		
		System.out.println("Epocas: " + epoca);

	}

	private void atualizaPesos(int amostraIndx, int saida, int target) {
		
		for (int i = 0; i < pesos.length; i++) {
			
			for (int j = 0; j < pesos[i].length; j++) {
				pesos[i][j] = pesos[i][j] + alfa * target * amostras[amostraIndx][i][j];
			}
		}

		bias = bias + alfa * target;

	}
	
	
	public int calculaY(int entrada[][]) {
		double yL = 0;

		for (int i = 0; i < entrada.length; i++) {
			for (int j = 0; j < entrada[i].length; j++) {
				yL += entrada[i][j] * pesos[i][j];
			}
		}

		yL += bias;

		if (yL > limiar) {
			return 1;
		} else {
			return -1;
		}
	}
	
	public static void main(String[] args) {
		int[][][] amostras = new int[][][]{
				MatrixConstants.NRO0, MatrixConstants.NRO1, MatrixConstants.NRO2,
				MatrixConstants.NRO3, MatrixConstants.NRO4, MatrixConstants.NRO5, 
				MatrixConstants.NRO6, MatrixConstants.NRO7, MatrixConstants.NRO8, MatrixConstants.NRO9};
		
		NeuronPerceptronEx6 n = new NeuronPerceptronEx6();
		n.treinar(amostras, new int[]{1, -1, -1, -1, -1, -1, -1, -1, -1, -1}, 1, 00.1);
		System.out.println(n.calculaY(MatrixConstants.NRO0));
	}

}
