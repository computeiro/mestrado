package wender.rna.trab09;

import wender.rna.functions.ActivationFunction;
import wender.rna.functions.SigmoidBipolar;
import wender.rna.utils.ErroPorEpocaListener;
import wender.rna.utils.GraficoErroTreinamento;
import wender.rna.utils.RNAUtils;

public class MLP {
	private double alfa;
	// camada / neuronio / entrada
	private double[][] bias;
	private double[][][] pesos;
	private double[][] deltinha;
	private double[][] alvo;
	private double[][] y_in;
	private double[][] y;
	private int[] camadasNeuronios;
	private int qtdEntradas;
	private int indUltCamada;
	private ErroPorEpocaListener erroListener;
	private ActivationFunction act;

	public static void main(String[] args) {
		double alfa = 0.1;
		int maxEpocas = 100000;
		double erroMinimo = 0.11;

		MLP mpl = new MLP(new int[] { 2, 1 });

		double[][] tabelaXor = new double[][] { { -1, -1 }, { 1, -1 }, { -1, 1 }, { 1, 1 } };

		// [amostra][neuronio de saida], como temos um neuronio so sao 4 arrays
		// com 1 elemento
		double alvo[][] = new double[][] { { -1 }, { 1 }, { 1 }, { -1 } };

		mpl.setErroListener(new GraficoErroTreinamento());

		mpl.treina(alfa, tabelaXor, alvo, erroMinimo, maxEpocas);

		StringBuilder out = new StringBuilder();

		out.append("\nResultado final: \n\n");
		for (int i = 0; i < tabelaXor.length; i++) {
			mpl.propaga(tabelaXor[i]);
			RNAUtils.fillStb(out, tabelaXor[i]);
			out.append("\n");
			out.append(mpl.y[mpl.indUltCamada][0]);
			out.append("\n\n");
		}

		System.out.println(out.toString());
	}

	public void setErroListener(ErroPorEpocaListener erroListener) {
		this.erroListener = erroListener;
	}

	public MLP(int[] camadasNeuronios) {
		// O �ndice da coluna de target � igual a quantidade de entradas
		// Por que o array possui �ndice 0 (zero)
		this.camadasNeuronios = camadasNeuronios;
		this.indUltCamada = camadasNeuronios.length - 1;
		this.act = new SigmoidBipolar();

		
	}

	private void inicializaPesos() {
		bias = new double[camadasNeuronios.length][];
		deltinha = new double[camadasNeuronios.length][];
		pesos = new double[camadasNeuronios.length][][];

		for (int c = 0; c < camadasNeuronios.length; c++) {
			bias[c] = new double[camadasNeuronios[c]];
			deltinha[c] = new double[camadasNeuronios[c]];
			pesos[c] = new double[camadasNeuronios[c]][];

			for (int n = 0; n < camadasNeuronios[c]; n++) {
				if (c == 0) {
					pesos[c][n] = RNAUtils.sorteiaArray(qtdEntradas);
				} else {
					pesos[c][n] = RNAUtils.sorteiaArray(camadasNeuronios[c - 1]);
				}

				bias[c][n] = RNAUtils.sorteiaPeso();
			}
		}

		System.out.println("Pesos iniciais:");
		RNAUtils.printArrOrMatrix(pesos);

		System.out.println("\nBias iniciais:");
		RNAUtils.printArrOrMatrix(bias);
	}

	public void treina(double alfa, double[][] amostras, double[][] alvo, double erroMinimo, int maxEpocas) {
		qtdEntradas = amostras[0].length;
		this.alfa = alfa;
		this.alvo = alvo;
		
		inicializaPesos();
		
		boolean trained = false;
		int epoca = 0;

		double erroTotal = 0;
		
		while (!trained) {
			epoca++;
			erroTotal = 0;

			for (int a = 0; a < amostras.length; a++) {
				propaga(amostras[a]);
				erroTotal += caculaErro(a);
				atualizaPesos(amostras[a], alvo[a]);
			}
			
			//Inteface que prov� liga��o entre interface gr�fica e algoritmo
			if (erroListener != null) {
				erroListener.update(epoca, erroTotal);
			}


			trained = erroTotal <= erroMinimo || epoca == maxEpocas;
		}
		
		if(erroTotal <= erroMinimo){
			System.out.println("Erro m�nimo atingido na �poca: " + epoca );
		}else{
			System.out.println("O treinamento alcan�ou o n�mero m�ximo de �pocas definidas. Erro encontrado: " + erroTotal);
		}
	}

	public void propaga(double[] entrada) {
		y_in = RNAUtils.clone(bias);
		y = new double[y_in.length][];

		for (int c = 0; c < camadasNeuronios.length; c++) {
			y[c] = new double[camadasNeuronios[c]];

			for (int n = 0; n < camadasNeuronios[c]; n++) {

				if (c == 0) {
					for (int e = 0; e < qtdEntradas; e++) {
						y_in[c][n] += pesos[c][n][e] * entrada[e];
					}

					y[c][n] = act.f(y_in[c][n]);
				} else {
					for (int e = 0; e < y[c - 1].length; e++) {
						y_in[c][n] += pesos[c][n][e] * y[c - 1][e];
					}

					y[c][n] = act.f(y_in[c][n]);
				}

			}
		}
	}

	public double caculaErro(int indAmostra) {
		double erro = 0;

		for (int n = 0; n < camadasNeuronios[indUltCamada]; n++) {
			// Valor absoluto da diferen�a entre alvo e y encontrado
			erro += Math.abs(alvo[indAmostra][n] - y[indUltCamada][n]);
		}

		return erro;
	}

	private void atualizaPesos(double[] amostra, double[] alvo) {

		// Calculando o deltinha da ultima camada
		for (int n = 0; n < camadasNeuronios[indUltCamada]; n++) {
			deltinha[indUltCamada][n] = (alvo[n] - y[indUltCamada][n]) * act.fl(y_in[indUltCamada][n]);
		}

		// La�o come�ando da penultima camada (a �ltima j� foi)
		for (int c = indUltCamada - 1; c > -1; c--) {
			for (int n = 0; n < camadasNeuronios[c]; n++) {
				deltinha[c][n] = 0;

				// Step 7 deltinha in_j
				for (int neuronK = 0; neuronK < camadasNeuronios[c + 1]; neuronK++) {
					for (int w = 0; w < pesos[c + 1][neuronK].length; w++) {
						deltinha[c][n] += deltinha[c + 1][neuronK] * pesos[c + 1][neuronK][w];
					}
				}

				// deltinha = deltinha_in * f'(z_inj)
				deltinha[c][n] = deltinha[c][n] * act.fl(y_in[c][n]);
			}
		}

		// Atualiza��o dos pesos
		for (int c = 0; c < camadasNeuronios.length; c++) {
			for (int n = 0; n < camadasNeuronios[c]; n++) {

				for (int w = 0; w < pesos[c][n].length; w++) {
					double entrada = c == 0 ? amostra[w] : y[c - 1][w];

					pesos[c][n][w] += alfa * deltinha[c][n] * entrada;
				}

				bias[c][n] += alfa * deltinha[c][n];
			}
		}

	}
}
