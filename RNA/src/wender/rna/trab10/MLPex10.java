package wender.rna.trab10;

import wender.rna.functions.ActivationFunction;
import wender.rna.functions.SigmoidBipolar;
import wender.rna.utils.ErroPorEpocaListener;
import wender.rna.utils.GraficoErroTreinamento;
import wender.rna.utils.RNAUtils;

public class MLPex10 {
	private static final double INTERVALO = 0.1;

	private double alfa;
	// camada / neuronio / entrada
	private double[][] bias;
	private double[][][] pesos;
	private double[][] deltinha;
	private double[][] alvo;
	private double[][] y_in;
	private double[][] y;
	private int[] camadasNeuronios;
	private int qtdEntradas;
	private int indUltCamada;
	private ActivationFunction act;
	private Runnable testeFunction;
	private ErroPorEpocaListener erroListener;

	private static double f(double x) {
		return Math.sin(x) * Math.sin(2 * x);
	}

	public static void main(String[] args) {
		double alfa = 0.001;
		int maxEpocas = 10000000;
		double erroMinimo = 0;

		final GraficoAproximacaoFuncional gfc = new GraficoAproximacaoFuncional();

		final int size = (int) Math.round((2 * Math.PI) / INTERVALO);

		double[][] amostras = new double[size][];
		double[][] alvos = new double[size][];

		double x, y;

		for (int i = 0; i < size; i++) {
			x = i * INTERVALO;
			y = f(x);

			amostras[i] = new double[] { x };
			alvos[i] = new double[] { y };
			gfc.setFuncValue(x, y);

		}

		final MLPex10 mpl = new MLPex10(new int[] { 9, 1 });
		ErroPorEpocaListener erroListner = new GraficoErroTreinamento();
		mpl.erroListener = erroListner;

		mpl.testeFunction = new Runnable() {

			@Override
			public void run() {
				double x;
				double[] result;

				for (int i = 0; i < size; i++) {
					x = i * INTERVALO;
					mpl.propaga(new double[] { x });

					gfc.setRnaValue(x, mpl.getSaida()[0]);

				}

			}
		};

		mpl.treina(alfa, amostras, alvos, erroMinimo, maxEpocas);

		StringBuilder out = new StringBuilder();

		out.append("\nResultado final: \n\n");

		for (int i = 0; i < amostras.length; i++) {
			mpl.propaga(amostras[i]);
			RNAUtils.fillStb(out, amostras[i]);
			out.append("\n");
			out.append(mpl.y[mpl.indUltCamada][0]);
			out.append("\n\n");
		}

		System.out.println(out.toString());
	}

	public MLPex10(int[] camadasNeuronios) {
		// O �ndice da coluna de target � igual a quantidade de entradas
		// Por que o array possui �ndice 0 (zero)
		this.camadasNeuronios = camadasNeuronios;
		this.indUltCamada = camadasNeuronios.length - 1;
		this.act = new SigmoidBipolar();

	}

	private void inicializaPesos() {
		bias = new double[camadasNeuronios.length][];
		deltinha = new double[camadasNeuronios.length][];
		pesos = new double[camadasNeuronios.length][][];

		for (int c = 0; c < camadasNeuronios.length; c++) {
			bias[c] = new double[camadasNeuronios[c]];
			deltinha[c] = new double[camadasNeuronios[c]];
			pesos[c] = new double[camadasNeuronios[c]][];

			for (int n = 0; n < camadasNeuronios[c]; n++) {
				if (c == 0) {
					pesos[c][n] = RNAUtils.sorteiaArray(qtdEntradas);
				} else {
					pesos[c][n] = RNAUtils.sorteiaArray(camadasNeuronios[c - 1]);
				}

				bias[c][n] = RNAUtils.sorteiaPeso();
			}
		}

		System.out.println("Pesos iniciais:");
		RNAUtils.printArrOrMatrix(pesos);

		System.out.println("\nBias iniciais:");
		RNAUtils.printArrOrMatrix(bias);
	}

	public void treina(double alfa, double[][] amostras, double[][] alvo, double erroMinimo, int maxEpocas) {
		qtdEntradas = amostras[0].length;
		this.alfa = alfa;
		this.alvo = alvo;

		inicializaPesos();

		boolean trained = false;
		int epoca = 0;

		double erroTotal = 0;

		while (!trained) {
			epoca++;
			erroTotal = 0;

			for (int a = amostras.length - 1; a > -1; a--) {
				propaga(amostras[a]);
				erroTotal += caculaErro(a);
				atualizaPesos(amostras[a], alvo[a]);
			}

			if (epoca % 1000 == 0) {
				erroListener.update(epoca, erroTotal);
				testeFunction.run();
			}

			trained = erroTotal <= erroMinimo || epoca == maxEpocas;
		}

		if (erroTotal <= erroMinimo) {
			System.out.println("Erro m�nimo atingido na �poca: " + epoca);
		} else {
			System.out.println("O treinamento alcan�ou o n�mero m�ximo de �pocas definidas. Erro encontrado: " + erroTotal);
		}
	}

	public void propaga(double[] entrada) {
		y_in = RNAUtils.clone(bias);
		y = new double[y_in.length][];

		for (int c = 0; c < camadasNeuronios.length; c++) {
			y[c] = new double[camadasNeuronios[c]];

			for (int n = 0; n < camadasNeuronios[c]; n++) {

				if (c == 0) {
					for (int e = 0; e < qtdEntradas; e++) {
						y_in[c][n] += pesos[c][n][e] * entrada[e];
					}

					y[c][n] = act.f(y_in[c][n]);
				} else {
					for (int e = 0; e < y[c - 1].length; e++) {
						y_in[c][n] += pesos[c][n][e] * y[c - 1][e];
					}

					y[c][n] = act.f(y_in[c][n]);
				}

			}
		}
	}

	public double caculaErro(int indAmostra) {
		double erro = 0;

		for (int n = 0; n < camadasNeuronios[indUltCamada]; n++) {
			// Valor absoluto da diferen�a entre alvo e y encontrado
			erro += Math.abs(alvo[indAmostra][n] - y[indUltCamada][n]);
		}

		return erro;
	}

	private void atualizaPesos(double[] amostra, double[] alvo) {

		// Calculando o deltinha da ultima camada
		for (int n = 0; n < camadasNeuronios[indUltCamada]; n++) {
			deltinha[indUltCamada][n] = (alvo[n] - y[indUltCamada][n]) * act.fl(y_in[indUltCamada][n]);
		}

		// La�o come�ando da penultima camada (a �ltima j� foi)
		for (int c = indUltCamada - 1; c > -1; c--) {
			for (int n = 0; n < camadasNeuronios[c]; n++) {
				deltinha[c][n] = 0;

				// Step 7 deltinha in_j
				for (int neuronK = 0; neuronK < camadasNeuronios[c + 1]; neuronK++) {
					for (int w = 0; w < pesos[c + 1][neuronK].length; w++) {
						deltinha[c][n] += deltinha[c + 1][neuronK] * pesos[c + 1][neuronK][w];
					}
				}

				// deltinha = deltinha_in * f'(z_inj)
				deltinha[c][n] = deltinha[c][n] * act.fl(y_in[c][n]);
			}
		}

		// Atualiza��o dos pesos
		for (int c = 0; c < camadasNeuronios.length; c++) {
			for (int n = 0; n < camadasNeuronios[c]; n++) {

				for (int w = 0; w < pesos[c][n].length; w++) {
					double entrada = c == 0 ? amostra[w] : y[c - 1][w];

					pesos[c][n][w] += alfa * deltinha[c][n] * entrada;
				}

				bias[c][n] += alfa * deltinha[c][n];
			}
		}

	}

	public double[] getSaida() {
		return y[indUltCamada];
	}
}
