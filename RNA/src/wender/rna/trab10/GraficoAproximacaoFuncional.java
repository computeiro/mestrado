package wender.rna.trab10;

import java.awt.Color;

import javax.swing.JFrame;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import wender.rna.utils.ErroPorEpocaListener;

public class GraficoAproximacaoFuncional implements Runnable {
	private XYSeries funcSerie; 
	private XYSeries errSerie; 
	private JFreeChart chart;

	public static void main(String[] args) {

	}

	public void setRnaValue(double x, double y) {
		errSerie.add(x, y);
	}
	
	public void setFuncValue(double x, double y){
		funcSerie.add(x, y);
	}

	public GraficoAproximacaoFuncional() {
		final XYSeriesCollection dataset = new XYSeriesCollection();

		funcSerie = new XYSeries("Fun��o");
		dataset.addSeries(funcSerie);
		
		errSerie = new XYSeries("RNA");
		dataset.addSeries(errSerie);

		chart = ChartFactory.createXYLineChart("Aproxima��o funcional", // chart
																								// title
				"X", // x axis label
				"Y", // y axis label
				dataset, // data
				PlotOrientation.VERTICAL, true, // include legend
				true, // tooltips
				false // urls
				);

		XYPlot plot = chart.getXYPlot();
		XYItemRenderer render = plot.getRenderer();
		render.setSeriesPaint(0, Color.BLUE);
		render.setSeriesPaint(1, Color.RED);

		Thread t = new Thread(this);
		t.start();

	}

	public void run() {
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(800, 600);

		ChartPanel chartPanel = new ChartPanel(chart);
		frame.add(chartPanel);

		frame.setVisible(true);
	}

}
