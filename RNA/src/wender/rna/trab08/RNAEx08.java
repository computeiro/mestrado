package wender.rna.trab08;

import wender.rna.utils.ErroPorEpocaListener;



public class RNAEx08 {
	private int[][][] amostras;
	private int[][] targets;
	private double alfa;
	private int maxEpocas;
	private double erroToleravel;
	
	private final NeuronAdalaineEx8[] neuron = new NeuronAdalaineEx8[10];
	
	public RNAEx08(int[][][] amostras, int[][] targets, double alfa, int maxEpocas, double erroToleravel){
		this.amostras = amostras;
		this.targets = targets;
		this.alfa = alfa;
		this.maxEpocas = maxEpocas;
		this.erroToleravel = erroToleravel;
		
	}
	
	
	public void efetuaTreino(ErroPorEpocaListener erroListener){
		
		for(int n = 0; n < neuron.length; n++){
			neuron[n] = new NeuronAdalaineEx8();
			
			if(n == 8){
				neuron[n].setErroQuadraticoListener(erroListener);
			}
			
			neuron[n].treinar(amostras, targets[n], alfa, maxEpocas, erroToleravel);
			
			System.out.printf("\nTreino Conclu�do [%s]: �poca: %s | Erro: %s", n, neuron[n].getEpoca(), neuron[n].getErroTotal());
		}
	}
	
	public double[] calculaY(int[][] amostra){
		double[] y = new double[neuron.length];
		
		for(int n = 0; n < neuron.length; n ++){
			y[n] = neuron[n].calculaY(amostra);
		}
		
		return y;
	}
	
}
