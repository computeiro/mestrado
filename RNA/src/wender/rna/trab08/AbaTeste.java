package wender.rna.trab08;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;

import wender.rna.trab04.Perceptron;
import wender.rna.ui.CharMatrix;
import wender.rna.ui.MatrixConstants;

import javax.swing.JSpinner;
import javax.swing.JLabel;
import javax.swing.event.ChangeListener;

public class AbaTeste extends JPanel {
	private RNAEx08 rede;

	private JButton btnNro1;
	private JButton btnNro2;
	private JButton btnNro3;
	private JButton btnNro4;
	private JButton btnNro5;
	private JButton btnNro6;
	private JButton btnNro7;
	private JButton btnNro8;
	private JButton btnNro9;
	private JButton btnNro0;

	private CharMatrix cMatriz;

	private JButton btnTestar;

	private JTextField txValue0;
	private JTextField txValue1;
	private JTextField txValue2;
	private JTextField txValue3;
	private JTextField txValue4;
	private JTextField txValue5;
	private JTextField txValue6;
	private JTextField txValue7;
	private JTextField txValue8;
	private JTextField txValue9;

	private JTextField txNro0;
	private JTextField txNro1;
	private JTextField txNro2;
	private JTextField txNro3;
	private JTextField txNro4;
	private JTextField txNro5;
	private JTextField txNro6;
	private JTextField txNro7;
	private JTextField txNro8;
	private JTextField txNro9;
	
	private JSpinner spSecurity;

	public AbaTeste() {
		setLayout(null);

		cMatriz = new CharMatrix(Perceptron.LINHAS, Perceptron.COLUNAS);
		cMatriz.setBounds(183, 11, 140, 180);
		add(cMatriz);

		txNro0 = new JTextField();
		txNro0.setHorizontalAlignment(SwingConstants.CENTER);
		txNro0.setText("0");
		txNro0.setEditable(false);
		txNro0.setColumns(10);
		txNro0.setBounds(472, 11, 34, 20);
		add(txNro0);

		txNro1 = new JTextField();
		txNro1.setText("1");
		txNro1.setHorizontalAlignment(SwingConstants.CENTER);
		txNro1.setEditable(false);
		txNro1.setColumns(10);
		txNro1.setBounds(472, 42, 34, 20);
		add(txNro1);

		txNro2 = new JTextField();
		txNro2.setText("2");
		txNro2.setHorizontalAlignment(SwingConstants.CENTER);
		txNro2.setEditable(false);
		txNro2.setColumns(10);
		txNro2.setBounds(472, 73, 34, 20);
		add(txNro2);

		txNro3 = new JTextField();
		txNro3.setText("3");
		txNro3.setHorizontalAlignment(SwingConstants.CENTER);
		txNro3.setEditable(false);
		txNro3.setBounds(472, 104, 34, 20);
		add(txNro3);
		txNro3.setColumns(10);

		txNro4 = new JTextField();
		txNro4.setText("4");
		txNro4.setHorizontalAlignment(SwingConstants.CENTER);
		txNro4.setEditable(false);
		txNro4.setColumns(10);
		txNro4.setBounds(472, 135, 34, 20);
		add(txNro4);

		txNro5 = new JTextField();
		txNro5.setText("5");
		txNro5.setHorizontalAlignment(SwingConstants.CENTER);
		txNro5.setEditable(false);
		txNro5.setColumns(10);
		txNro5.setBounds(472, 166, 34, 20);
		add(txNro5);

		txNro6 = new JTextField();
		txNro6.setText("6");
		txNro6.setHorizontalAlignment(SwingConstants.CENTER);
		txNro6.setEditable(false);
		txNro6.setColumns(10);
		txNro6.setBounds(472, 197, 34, 20);
		add(txNro6);

		txNro7 = new JTextField();
		txNro7.setText("7");
		txNro7.setHorizontalAlignment(SwingConstants.CENTER);
		txNro7.setEditable(false);
		txNro7.setColumns(10);
		txNro7.setBounds(472, 228, 34, 20);
		add(txNro7);

		txNro8 = new JTextField();
		txNro8.setText("8");
		txNro8.setHorizontalAlignment(SwingConstants.CENTER);
		txNro8.setEditable(false);
		txNro8.setColumns(10);
		txNro8.setBounds(472, 259, 34, 20);
		add(txNro8);

		txNro9 = new JTextField();
		txNro9.setText("9");
		txNro9.setHorizontalAlignment(SwingConstants.CENTER);
		txNro9.setEditable(false);
		txNro9.setColumns(10);
		txNro9.setBounds(472, 290, 34, 20);
		add(txNro9);

		btnTestar = new JButton("Testar");
		btnTestar.setBounds(183, 202, 140, 23);
		btnTestar.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				testarClicked();
			}
		});

		add(btnTestar);

		txValue0 = new JTextField();
		txValue0.setBounds(333, 11, 129, 20);
		add(txValue0);
		txValue0.setColumns(10);

		txValue1 = new JTextField();
		txValue1.setColumns(10);
		txValue1.setBounds(333, 42, 129, 20);
		add(txValue1);

		txValue2 = new JTextField();
		txValue2.setColumns(10);
		txValue2.setBounds(333, 73, 129, 20);
		add(txValue2);

		txValue3 = new JTextField();
		txValue3.setColumns(10);
		txValue3.setBounds(333, 104, 129, 20);
		add(txValue3);

		txValue4 = new JTextField();
		txValue4.setColumns(10);
		txValue4.setBounds(333, 135, 129, 20);
		add(txValue4);

		txValue5 = new JTextField();
		txValue5.setColumns(10);
		txValue5.setBounds(333, 166, 129, 20);
		add(txValue5);

		txValue6 = new JTextField();
		txValue6.setColumns(10);
		txValue6.setBounds(333, 197, 129, 20);
		add(txValue6);

		txValue7 = new JTextField();
		txValue7.setColumns(10);
		txValue7.setBounds(333, 228, 129, 20);
		add(txValue7);

		txValue8 = new JTextField();
		txValue8.setColumns(10);
		txValue8.setBounds(333, 259, 129, 20);
		add(txValue8);

		txValue9 = new JTextField();
		txValue9.setColumns(10);
		txValue9.setBounds(333, 290, 129, 20);
		add(txValue9);

		btnNro1 = new JButton("1");
		btnNro1.setBounds(10, 11, 42, 23);
		add(btnNro1);
		btnNro1.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				cMatriz.fill(MatrixConstants.NRO1);

			}
		});

		btnNro2 = new JButton("2");
		btnNro2.setBounds(59, 11, 42, 23);
		add(btnNro2);
		btnNro2.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				cMatriz.fill(MatrixConstants.NRO2);

			}
		});

		btnNro3 = new JButton("3");
		btnNro3.setBounds(108, 11, 42, 23);
		add(btnNro3);
		btnNro3.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				cMatriz.fill(MatrixConstants.NRO3);

			}
		});

		btnNro4 = new JButton("4");
		btnNro4.setBounds(10, 45, 42, 23);
		add(btnNro4);
		btnNro4.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				cMatriz.fill(MatrixConstants.NRO4);

			}
		});

		btnNro5 = new JButton("5");
		btnNro5.setBounds(59, 45, 42, 23);
		add(btnNro5);
		btnNro5.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				cMatriz.fill(MatrixConstants.NRO5);

			}
		});

		btnNro6 = new JButton("6");
		btnNro6.setBounds(108, 45, 42, 23);
		add(btnNro6);
		btnNro6.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				cMatriz.fill(MatrixConstants.NRO6);

			}
		});

		btnNro7 = new JButton("7");
		btnNro7.setBounds(10, 79, 42, 23);
		add(btnNro7);
		btnNro7.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				cMatriz.fill(MatrixConstants.NRO7);

			}
		});

		btnNro8 = new JButton("8");
		btnNro8.setBounds(59, 79, 42, 23);
		add(btnNro8);
		btnNro8.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				cMatriz.fill(MatrixConstants.NRO8);

			}
		});

		btnNro9 = new JButton("9");
		btnNro9.setBounds(108, 79, 42, 23);
		add(btnNro9);
		btnNro9.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				cMatriz.fill(MatrixConstants.NRO9);

			}
		});

		btnNro0 = new JButton("0");
		btnNro0.setBounds(59, 114, 42, 23);
		add(btnNro0);
		
		
		SpinnerModel model = new SpinnerNumberModel(0.95, 0.01, 0.99, 0.01);
		
		spSecurity = new JSpinner(model);
		
		spSecurity.setBounds(183, 290, 140, 20);
		add(spSecurity);
		
		JLabel lbSeguranca = new JLabel("N\u00EDvel de alerta");
		lbSeguranca.setBounds(186, 275, 129, 14);
		add(lbSeguranca);
		btnNro0.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				cMatriz.fill(MatrixConstants.NRO0);

			}
		});
	}

	private void testarClicked() {
		
		if(rede == null){
			JOptionPane.showMessageDialog(this, "A rede n�o foi treinada, por favor efetue o treino", "Efetue o treino primeiro", JOptionPane.WARNING_MESSAGE);
		}
		

		JTextField[] flags = { txNro0, txNro1, txNro2, txNro3, txNro4, txNro5, txNro6, txNro7, txNro8, txNro9 };
		JTextField[] txValues = {txValue0, txValue1, txValue2, txValue3, txValue4, txValue5, txValue6, txValue7, txValue8, txValue9};
		
		double[] saida = rede.calculaY(cMatriz.getValue());

		for (int i = 0; i < saida.length; i++) {
			txValues[i].setText(String.valueOf(saida[i]) );
			
			Color color;
			
			if(Math.abs(saida[i]) < (Double)spSecurity.getValue()){
				 color = Color.ORANGE;
			}else if(saida[i] < 0){
				color = Color.RED;
			}else{
				color = Color.GREEN;
			}
			
			flags[i].setBackground(color);
		}
	}

	public void resetFlags() {
		JTextField[] flags = { txNro0, txNro1, txNro2, txNro3, txNro4, txNro5, txNro6, txNro7, txNro8, txNro9 };
		
		for (int i = 0; i < flags.length; i++) {
			flags[i].setBackground(Color.GRAY);
		}
	}

	public void setRede(RNAEx08 rede) {
		this.rede = rede;
	}
}
