package wender.rna.trab08;

import java.awt.EventQueue;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;

import org.jfree.data.xy.XYSeries;

import wender.rna.ui.MatrixConstants;
import wender.rna.utils.ErroPorEpocaListener;

public class Trab08App {
	private final int LIMIAR = 1;

	private JFrame frame;
	private AbaTreino abaTreino;
	private AbaTeste abaTeste;
		
	
	private RNAEx08 rede;
	
	private final int[][][] amostras = new int[][][]{
			MatrixConstants.NRO0, MatrixConstants.NRO1, MatrixConstants.NRO2,
			MatrixConstants.NRO3, MatrixConstants.NRO4, MatrixConstants.NRO5, 
			MatrixConstants.NRO6, MatrixConstants.NRO7, MatrixConstants.NRO8, MatrixConstants.NRO9};
	
	private final int[][] targets = new int[][]{
			{ 1, -1, -1, -1, -1, -1, -1, -1, -1, -1,},
			{-1,  1, -1, -1, -1, -1, -1, -1, -1, -1,},
			{-1, -1,  1, -1, -1, -1, -1, -1, -1, -1,},
			{-1, -1, -1,  1, -1, -1, -1, -1, -1, -1,},
			{-1, -1, -1, -1,  1, -1, -1, -1, -1, -1,},
			{-1, -1, -1, -1, -1,  1, -1, -1, -1, -1,},
			{-1, -1, -1, -1, -1, -1,  1, -1, -1, -1,},
			{-1, -1, -1, -1, -1, -1, -1,  1, -1, -1,},
			{-1, -1, -1, -1, -1, -1, -1, -1,  1, -1,},
			{-1, -1, -1, -1, -1, -1, -1, -1, -1,  1,},
	};
	
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Trab08App window = new Trab08App();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Trab08App() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 532, 397);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setTitle("Wender - RNA 08 - Adaline");
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 0};
		gridBagLayout.rowHeights = new int[]{0, 0};
		gridBagLayout.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{1.0, Double.MIN_VALUE};
		frame.getContentPane().setLayout(gridBagLayout);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		GridBagConstraints gbc_tabbedPane = new GridBagConstraints();
		gbc_tabbedPane.fill = GridBagConstraints.BOTH;
		gbc_tabbedPane.gridx = 0;
		gbc_tabbedPane.gridy = 0;
		
		abaTreino = new AbaTreino(new TrainningActionImpl());
		tabbedPane.add("Treino", abaTreino);
		
		abaTeste = new AbaTeste();
		tabbedPane.add("Teste", abaTeste);
		
		
		frame.getContentPane().add(tabbedPane, gbc_tabbedPane);
		frame.setVisible(true);
	}
	
	
	private class TrainningActionImpl implements TrainningListener{

		@Override
		public void start(final XYSeries errSerie, double alfa, int maxEpocas, double erroToleravel) {
			if(alfa <= 0){
				JOptionPane.showMessageDialog(frame, "Preencha o campo 'Taxa de aprendizagem' com um valor inteiro ou decimal v�lido (separador decimal = '.')", "Taxa de aprendizagem inv�lida!", JOptionPane.ERROR_MESSAGE);
				return;
			}
			
			System.out.println("\nIniciando treinamento...");
			abaTeste.resetFlags();
			
			ErroPorEpocaListener  erroListener  = new ErroPorEpocaListener() {
				
				@Override
				public void update(int epoca, double erro) {
					errSerie.add(epoca, erro);
					
				}
			};
			
			rede =  new RNAEx08(amostras, targets, alfa, maxEpocas, erroToleravel);
			rede.efetuaTreino(erroListener);
			
			abaTeste.setRede(rede);
			
		}
		
	}
	
	public interface TrainningListener{
		public void start(XYSeries errSerie, double alfa, int maxEpocas, double erroToleravel);
	}
	
}
