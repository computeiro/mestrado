package wender.rna.trab08;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

import java.awt.Color;
import java.awt.GridBagLayout;

import javax.swing.JLabel;

import java.awt.GridBagConstraints;

import javax.swing.JTextField;

import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import wender.rna.trab07.ui.treino.XYChart;
import wender.rna.trab08.Trab08App.TrainningListener;
import wender.rna.ui.DoubleJTextField;

import javax.swing.JTabbedPane;

public class AbaTreino extends JPanel {
	private static final String TAXA_PADRAO = "0.029";
	
	private DoubleJTextField txTaxaAprend;
	private JSpinner spMaxEpocas;
	private DoubleJTextField txErroToleravel;
	
	private XYSeries errSerie;
	private XYChart chart;
	private JTextField textField;
	
	public AbaTreino(final TrainningListener tAction) {
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 0, 0, 0, 0};
		gridBagLayout.rowHeights = new int[]{0, 0, 0, 0};
		gridBagLayout.columnWeights = new double[]{1.0, 1.0, 1.0, 0.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 1.0, Double.MIN_VALUE};
		setLayout(gridBagLayout);
		
		JLabel lbTxAprend = new JLabel("Tx. Aprend.");
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel.gridx = 0;
		gbc_lblNewLabel.gridy = 0;
		add(lbTxAprend, gbc_lblNewLabel);
		
		JLabel lbErroTolerável = new JLabel("Erro Tolerável");
		GridBagConstraints gbc_lbErroToleravel = new GridBagConstraints();
		gbc_lbErroToleravel.anchor = GridBagConstraints.WEST;
		gbc_lbErroToleravel.insets = new Insets(0, 0, 5, 5);
		gbc_lbErroToleravel.gridx = 1;
		gbc_lbErroToleravel.gridy = 0;
		add(lbErroTolerável, gbc_lbErroToleravel);
		
		JLabel lbMaxEpocas = new JLabel("Max. Épocas");
		GridBagConstraints gbc_lbMaxEpocas = new GridBagConstraints();
		gbc_lbMaxEpocas.anchor = GridBagConstraints.WEST;
		gbc_lbMaxEpocas.insets = new Insets(0, 0, 5, 5);
		gbc_lbMaxEpocas.gridx = 2;
		gbc_lbMaxEpocas.gridy = 0;
		add(lbMaxEpocas, gbc_lbMaxEpocas);
		
		txTaxaAprend = new DoubleJTextField();
		txTaxaAprend.setValue(0.029);
		GridBagConstraints gbc_txAprend = new GridBagConstraints();
		gbc_txAprend.fill = GridBagConstraints.HORIZONTAL;
		gbc_txAprend.insets = new Insets(0, 0, 5, 5);
		gbc_txAprend.gridx = 0;
		gbc_txAprend.gridy = 1;
		add(txTaxaAprend, gbc_txAprend);
		
		txErroToleravel = new DoubleJTextField();
		txErroToleravel.setText("0.0000001");
		GridBagConstraints gbc_txErroToleravel = new GridBagConstraints();
		gbc_txErroToleravel.fill = GridBagConstraints.HORIZONTAL;
		gbc_txErroToleravel.insets = new Insets(0, 0, 5, 5);
		gbc_txErroToleravel.gridx = 1;
		gbc_txErroToleravel.gridy = 1;
		add(txErroToleravel, gbc_txErroToleravel);
		
		spMaxEpocas = new JSpinner(new SpinnerNumberModel(20000, 1, 100000, 1));
		GridBagConstraints gbc_spMaxEpocas = new GridBagConstraints();
		gbc_spMaxEpocas.anchor = GridBagConstraints.WEST;
		gbc_spMaxEpocas.insets = new Insets(0, 0, 5, 5);
		gbc_spMaxEpocas.gridx = 2;
		gbc_spMaxEpocas.gridy = 1;
		add(spMaxEpocas, gbc_spMaxEpocas);
		
		JButton btnTreinar = new JButton("Treinar");
		GridBagConstraints gbc_btnTreinar = new GridBagConstraints();
		gbc_btnTreinar.insets = new Insets(0, 0, 5, 0);
		gbc_btnTreinar.gridx = 3;
		gbc_btnTreinar.gridy = 1;
		add(btnTreinar, gbc_btnTreinar);
		
		final XYSeriesCollection dataset = new XYSeriesCollection();
		errSerie = new XYSeries("Erros");
		dataset.addSeries(errSerie);
		chart = new XYChart("Treinamento", dataset, 800, 600);
		
		GridBagConstraints gbc_chartPanel = new GridBagConstraints();
		gbc_chartPanel.gridwidth = 4;
		gbc_chartPanel.insets = new Insets(0, 0, 0, 5);
		gbc_chartPanel.fill = GridBagConstraints.BOTH;
		gbc_chartPanel.gridx = 0;
		gbc_chartPanel.gridy = 2;
		add(chart.getChartPanel(), gbc_chartPanel);
		
		
		btnTreinar.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				errSerie.clear();
				tAction.start(errSerie, txTaxaAprend.getValueOrZero(), (int) spMaxEpocas.getValue(), txErroToleravel.getValueOrZero());
			}
		});
		
	}
	
	public static void main(String[] args) {
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		frame.getContentPane().add(new AbaTreino(null));
		frame.setSize(800, 600);
		

		
		
		frame.setVisible(true);
	}
}
