package wender.rna.trab08;

import wender.rna.ui.MatrixConstants;
import wender.rna.utils.ErroPorEpocaListener;


public class NeuronAdalaineEx8 {
	public static final int COLUNAS = 7;
	public static final int LINHAS = 9;
	
	private int[][][] amostras;
	private double[][] pesos;
	private int[] targets;
	
	private double bias;
	private int epoca ;
	private double alfa;
	
	private double erroTotal;
	private ErroPorEpocaListener erroListener;

	public NeuronAdalaineEx8(){
		pesos = new double[LINHAS][COLUNAS];
	}
	
	public void treinar(int[][][] amostras, int[] targets, double alfa, int maxEpocas, double erroToleravel) {
		this.amostras = amostras;
		this.targets = targets;
		this.alfa = alfa;
		
		double yL;
		boolean treinou = false;
		double erroLocal;
		

		while (!treinou && epoca <= maxEpocas) {
			epoca++;
			erroTotal = 0;

			for (int i = 0; i < amostras.length; i++) {
				yL = calculaY(amostras[i]);
				erroLocal = 0.5 * Math.pow( (targets[i] - yL), 2);
				erroTotal += erroLocal;
				
				atualizaPesos(i, targets[i] - yL);
			}
			
			if(erroListener != null){
				erroListener.update(epoca, erroTotal);
			}
			
			treinou = erroTotal <= erroToleravel;
		}
		

	}

	private void atualizaPesos(int amostraIndx, double diferenca) {
		
		for (int i = 0; i < pesos.length; i++) {
			
			for (int j = 0; j < pesos[i].length; j++) {
				pesos[i][j] = pesos[i][j] + alfa * diferenca * amostras[amostraIndx][i][j];
			}
		}

		bias += alfa * diferenca;

	}

	public void setErroQuadraticoListener(ErroPorEpocaListener erroListener){
		this.erroListener = erroListener;
	}
	
	
	public double calculaY(int entrada[][]) {
		double yL = 0;

		for (int i = 0; i < entrada.length; i++) {
			for (int j = 0; j < entrada[i].length; j++) {
				yL += entrada[i][j] * pesos[i][j];
			}
		}

		yL += bias;
		
		return yL;
	}
	
	public static void main(String[] args) {
		int[][][] amostras = new int[][][]{
				MatrixConstants.NRO0, MatrixConstants.NRO1, MatrixConstants.NRO2,
				MatrixConstants.NRO3, MatrixConstants.NRO4, MatrixConstants.NRO5, 
				MatrixConstants.NRO6, MatrixConstants.NRO7, MatrixConstants.NRO8, MatrixConstants.NRO9};
		
		NeuronAdalaineEx8 n = new NeuronAdalaineEx8();
		n.treinar(amostras, new int[]{1, -1, -1, -1, -1, -1, -1, -1, -1, -1}, 00.1, 20000, 0.00001);
		System.out.println(n.calculaY(MatrixConstants.NRO0));
	}
	

	public double getEpoca() {
		return epoca;
	}
	
	public double getErroTotal(){
		return erroTotal;
	}

}
