package wender.rna.trab07;

import java.io.File;

import wender.rna.trab07.XlsxReader.XlsxData;
import wender.rna.trab07.ui.treino.FaceXlsx;


public class Neuronio {
	private final int MAX_EPOCAS = 1000000;
	private InfoHandler infoHndl;
	
	private double bias;
	private int target_idx;
	
	private double[] pesos;
	
	public static void main(String[] args) throws Exception{
		File xlsxFile = new File(System.getProperty("user.dir"), "planilhas/Tabela de treinamento Adaline.xlsx");
		
		XlsxData xlsx = XlsxReader.load(xlsxFile, FaceXlsx.XLSX_HEADER_ROW, FaceXlsx.XLSX_ROW_DATA_START, FaceXlsx.XLSX_TOTAL_ROWS, FaceXlsx.XLSX_TOTAL_COLS);
		
		//a ultima coluna � o "d" (target) 
		Neuronio n = new Neuronio();
		
		n.treina(xlsx.getPrimitiveData(), 0.003);
		
		int target_idx = xlsx.getHeaders().length -1;
		
		StringBuilder out = new StringBuilder();
		
		for(int a = 0; a < xlsx.getData().length; a++){
			out.append("\n amostra: ").append(a).append("| ");
			for(double c : xlsx.getPrimitiveData()[a]){
				out.append(c).append(" | ");
			}
			
			out.append("target: ").append(xlsx.getData()[a][target_idx]).append(" -> ");
			out.append(n.calculaY(xlsx.getPrimitiveData()[a]));
		}
		
		System.out.println(out.toString());
			
		
	}
	
	public void treina(double[][] amostras, double alfa){
		//A quantidade de pesos e o �ndice do target �  mesmo n�mero (think about!)
		target_idx = amostras[0].length -1;
		
		pesos = new double[target_idx];
		
		
		inicializaPesos();
		
		int epoca = 0;
		
		boolean finalizouTreino = false;
		while(! finalizouTreino && epoca < MAX_EPOCAS){
			double erroTotal = 0;
			epoca++;
			
			for(int a = 0; a < pesos.length; a ++){
				double yl = calculaY(amostras[a]);
				double diferenca = amostras[a][target_idx] - yl;
				
				double erroLocal = 0.5 * (Math.pow(diferenca, 2) );
				erroTotal += erroLocal; 
				
				atualizaPesos(amostras[a], diferenca, alfa);
			}
			
			if(infoHndl != null){
				infoHndl.updateInfo(epoca, erroTotal);
			}
			
			finalizouTreino = erroTotal <= 0.000001;
			
		}
		
	}
	
	private void atualizaPesos(double[] amostra, double diferenca, double alfa){
		
		for(int i = 0; i < pesos.length; i ++){
			//alfa *  (t[p] - y[p] ) * xi
			pesos[i] += alfa * diferenca * amostra[i];
		}
		
		bias += alfa * diferenca;
	}
	
	
	public double calculaY(double[] amostra){
		double y = 0;
		
		for(int i = 0; i < pesos.length; i++){
			y += amostra[i] * pesos[i];
		}
		
		y += bias;
		
		return y;
	}
	
	public double funcaoDegrau(double valor){
		if( valor > 0){
			return 1;
		}else{
			return -1; 
		}
	}
	
	private void inicializaPesos(){
		
		for(int i = 0; i < pesos.length; i++){
			
			double value = (int) (Math.random() * 1000);
				
			if(value > 0){
				pesos[i] = value / 2000;	
			}else{
				pesos[i] = value;
			}
			
		}
	}
	
	public void setInfoHandler(InfoHandler infoHndl){
		this.infoHndl = infoHndl;
	}
	
	public double getBias() {
		return bias;
	}

	public double[] getPesos() {
		return pesos;
	}

	public interface InfoHandler {
		void updateInfo(double epoca, double erro);
	}
}
