package wender.rna.trab07;

import java.awt.EventQueue;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JFrame;
import javax.swing.JTabbedPane;

import wender.rna.trab07.XlsxReader.XlsxData;
import wender.rna.trab07.ui.RnaTestePanel;
import wender.rna.trab07.ui.TreinoPanel;
import wender.rna.trab07.ui.TreinoPanel.TrainedListener;

public class Trab07App {

	private JFrame frame;
	private Neuronio neuronio;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Trab07App window = new Trab07App();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Trab07App() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		Neuronio neuronio = new Neuronio();
		
		frame = new JFrame();
		frame.setBounds(10, 10, 800, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 0};
		gridBagLayout.rowHeights = new int[]{0, 0};
		gridBagLayout.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{1.0, Double.MIN_VALUE};
		frame.getContentPane().setLayout(gridBagLayout);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		GridBagConstraints gbc_tabbedPane = new GridBagConstraints();
		gbc_tabbedPane.fill = GridBagConstraints.BOTH;
		gbc_tabbedPane.gridx = 0;
		gbc_tabbedPane.gridy = 0;
		frame.getContentPane().add(tabbedPane, gbc_tabbedPane);
		
		TreinoPanel treinoPanel = new TreinoPanel(neuronio);
		tabbedPane.addTab("Treino", null, treinoPanel, null);
		
		final RnaTestePanel rnaTestePanel = new RnaTestePanel();
		tabbedPane.addTab("Teste", null, rnaTestePanel, null);
		
		treinoPanel.setTrainedListener(new TrainedListener() {
			
			@Override
			public void setXlsxData(XlsxData data) {
				rnaTestePanel.setXlsxData(data);
				
			}
			
			@Override
			public void setNeuronio(Neuronio neuronio) {
				rnaTestePanel.setNeuronio(neuronio);
			}
		});
	}

}
