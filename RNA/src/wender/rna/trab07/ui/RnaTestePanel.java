package wender.rna.trab07.ui;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import wender.rna.trab07.Neuronio;
import wender.rna.trab07.XlsxReader.XlsxData;
import wender.rna.trab07.ui.treino.XlsxTable;
import wender.rna.trab07.ui.treino.XlsxTable.RowListener;
import wender.rna.ui.DoubleJTextField;

public class RnaTestePanel extends JPanel {
	private DoubleJTextField txPeso1;
	private DoubleJTextField txPeso2;
	private DoubleJTextField txPeso3;
	private DoubleJTextField txPeso4;
	private DoubleJTextField txX1;
	private DoubleJTextField txX2;
	private DoubleJTextField txX3;
	private DoubleJTextField txX4;
	private XlsxData xlsxData;
	private Neuronio neuronio;

	/**
	 * Create the panel.
	 */
	public RnaTestePanel() {
		setLayout(null);
		setBackground(Color.WHITE);

		ImageIcon imgExcel = new ImageIcon(RnaTestePanel.class.getResource("resources/excellIco.gif"));
		JButton btnChooseExcel = new JButton("Tabela de treino", imgExcel);
		
		btnChooseExcel.setBounds(84, 100, 188, 20);
		add(btnChooseExcel);
		btnChooseExcel.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if(xlsxData == null){
					JOptionPane.showMessageDialog(getParent(), "Voc� precisa carregar o arquivo (*.xlsx) que corresponde a PLANILHA de dados e realizar o TREINAMENTO", "Informa��es n�o carregadas", JOptionPane.WARNING_MESSAGE);
					return;
				}
				
				final JFrame jframe = new JFrame("Selecione uma linha para o teste");
				
				XlsxTable table = new XlsxTable();
				table.load(xlsxData.getData(), xlsxData.getHeaders());
				
				JScrollPane scrollPanel = new JScrollPane(table);
				jframe.add(scrollPanel);
				jframe.setBounds(getBounds().x + 50, getBounds().y + 50, 400, 400);
				
				table.addRowListener(new RowListener() {
					
					@Override
					public void doubleClick(Double[] row) {
						txX1.setText(String.valueOf(row[0]));
						txX2.setText(String.valueOf(row[1]));
						txX3.setText(String.valueOf(row[2]));
						txX4.setText(String.valueOf(row[3]));
						
					}
				});
				
				jframe.setVisible(true);
				
			}
		});
		
		
		txX1 = new DoubleJTextField();
		txX1.setBounds(134, 132, 86, 20);
		add(txX1);
		txX1.setColumns(10);
		
		txX2 = new DoubleJTextField();
		txX2.setBounds(134, 172, 86, 20);
		add(txX2);
		txX2.setColumns(10);
		
		txX3 = new DoubleJTextField();
		txX3.setBounds(134, 209, 86, 20);
		add(txX3);
		txX3.setColumns(10);
		
		txX4 = new DoubleJTextField();
		txX4.setBounds(134, 248, 86, 20);
		add(txX4);
		txX4.setColumns(10);
		
		JLabel lbX1 = new JLabel("x1");
		lbX1.setBounds(105, 124, 18, 20);
		add(lbX1);
		
		JLabel lbX2 = new JLabel("x2");
		lbX2.setBounds(105, 164, 18, 20);
		add(lbX2);
		
		JLabel lbX3 = new JLabel("x3");
		lbX3.setBounds(105, 204, 18, 20);
		add(lbX3);
		
		JLabel lbX4 = new JLabel("x4");
		lbX4.setBounds(105, 244, 18, 20);
		add(lbX4);
		
		txPeso1 = new DoubleJTextField();
		txPeso1.setBounds(264, 132, 137, 20);
		add(txPeso1);
		txPeso1.setColumns(10);
		
		txPeso2 = new DoubleJTextField();
		txPeso2.setBounds(264, 172, 137, 20);
		add(txPeso2);
		txPeso2.setColumns(10);
		
		txPeso3 = new DoubleJTextField();
		txPeso3.setBounds(264, 209, 137, 20);
		add(txPeso3);
		txPeso3.setColumns(10);
		
		txPeso4 = new DoubleJTextField();
		txPeso4.setBounds(264, 248, 137, 20);
		add(txPeso4);
		txPeso4.setColumns(10);
		
		ImageIcon imgGo = new ImageIcon(RnaTestePanel.class.getResource("resources/go.png"));
		JButton btnTestar = new JButton("Efetuar Teste", imgGo);
		btnTestar.setBounds(420, 175, 157, 20);
		add(btnTestar);
		
		final DoubleJTextField yTesteValue = new DoubleJTextField();
		yTesteValue.setBounds(420, 205, 157, 20);
		add(yTesteValue);
		
		final JLabel lbValvulaA = new JLabel("V�lvula A");
		lbValvulaA.setBackground(Color.WHITE);
		lbValvulaA.setBounds(442, 38, 77, 77);
		lbValvulaA.setHorizontalAlignment(JLabel.CENTER);
		lbValvulaA.setOpaque(true);
		add(lbValvulaA);
		
		final JLabel lbValvulaB = new JLabel("V�lvula B");
		lbValvulaB.setBackground(Color.WHITE);
		lbValvulaB.setHorizontalAlignment(JLabel.CENTER);
		lbValvulaB.setBounds(442, 284, 77, 77);
		lbValvulaB.setOpaque(true);
		add(lbValvulaB);
		
		ImageIcon imgComutador = new ImageIcon(RnaTestePanel.class.getResource("resources/comutadorImg.png"));
		JLabel lbImgComutador = new JLabel(imgComutador);
		lbImgComutador.setBounds(0, 0, 600, 400);
		
		add(lbImgComutador);
		
		btnTestar.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				double amostra[] = {
					txX1.getValueOrZero(),
					txX2.getValueOrZero(),
					txX3.getValueOrZero(),
					txX4.getValueOrZero()
				};
				double yValue = neuronio.calculaY(amostra);
				yTesteValue.setText(String.valueOf(yValue));
				
				
				
				if(yValue > 0){
					lbValvulaA.setBackground(Color.GREEN);
					lbValvulaB.setBackground(Color.RED);
				}else{
					lbValvulaA.setBackground(Color.RED);
					lbValvulaB.setBackground(Color.GREEN);
				}
				
				
			}
		});

	}
	
	public void setXlsxData(XlsxData xlsxData){
		this.xlsxData = xlsxData;
	}
	
	public void setNeuronio(Neuronio neuronio){
		this.neuronio = neuronio;
		txPeso1.setText(String.valueOf(neuronio.getPesos()[0])); 
		txPeso2.setText(String.valueOf(neuronio.getPesos()[1])); 
		txPeso3.setText(String.valueOf(neuronio.getPesos()[2])); 
		txPeso4.setText(String.valueOf(neuronio.getPesos()[3]));
	}
}
