package wender.rna.trab07.ui;

import java.awt.CardLayout;
import java.awt.Cursor;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import wender.rna.trab07.Neuronio;
import wender.rna.trab07.Neuronio.InfoHandler;
import wender.rna.trab07.XlsxReader.XlsxData;
import wender.rna.trab07.ui.treino.FaceXlsx;
import wender.rna.trab07.ui.treino.XYChart;

public class TreinoPanel extends JPanel {
	private Neuronio neuronio;

	private CardLayout cardLayout;
	private JPanel cardPanel;

	private XYSeries errSerie;

	private XYChart chart;
	private FaceXlsx faceXlsx;

	private JButton btnTreinar;

	private final String FACE_FORM = "form";
	private final String FACE_CHART = "chart";
	private String currentFace;
	
	private TrainedListener trainedListener;

	public TreinoPanel(Neuronio neuronio) {
		this.neuronio = neuronio;

		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 0 };
		gridBagLayout.rowHeights = new int[] { 0, 0 };
		gridBagLayout.columnWeights = new double[] { 1.0 };
		gridBagLayout.rowWeights = new double[] { 1.0, 1.0 };
		setLayout(gridBagLayout);

		cardPanel = new JPanel();
		GridBagConstraints gbc_cardPanel = new GridBagConstraints();
		gbc_cardPanel.fill = GridBagConstraints.BOTH;
		gbc_cardPanel.insets = new Insets(0, 0, 5, 0);
		gbc_cardPanel.gridx = 0;
		gbc_cardPanel.gridy = 0;
		add(cardPanel, gbc_cardPanel);
		cardLayout = new CardLayout();
		cardPanel.setLayout(cardLayout);

		final XYSeriesCollection dataset = new XYSeriesCollection();
		errSerie = new XYSeries("Erros");
		dataset.addSeries(errSerie);

		chart = new XYChart("Treinamento", dataset, 600, 300);
		cardPanel.add(chart.getChartPanel(), FACE_CHART);

		faceXlsx = new FaceXlsx();
		cardPanel.add(faceXlsx, FACE_FORM);

		btnTreinar = new JButton("Treinar");
		btnTreinar.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				Thread t = new Thread(new Runnable() {

					@Override
					public void run() {

						setCursor(new Cursor(Cursor.WAIT_CURSOR));

						try {

							changeFace(false);

							if (currentFace.equals(FACE_CHART)) {
								iniciaTreino();
							}

						} catch (Exception e) {
							e.printStackTrace();
							changeFace(true);
						} finally {
							setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
						}
					}
				});

				t.start();

			}
		});

		GridBagConstraints gbc_btn = new GridBagConstraints();
		gbc_btn.anchor = GridBagConstraints.SOUTHEAST;
		gbc_btn.gridx = 0;
		gbc_btn.gridy = 1;
		add(btnTreinar, gbc_btn);

		changeFace(true);

	}

	public void changeFace(boolean init) {

		if (init || currentFace.equals(FACE_CHART)) {
			btnTreinar.setText("Treinar");
			cardLayout.show(cardPanel, FACE_FORM);
			currentFace = FACE_FORM;
		} else {
			btnTreinar.setText("Voltar");
			cardLayout.show(cardPanel, FACE_CHART);
			currentFace = FACE_CHART;
		}

	}

	private void iniciaTreino() {
		XlsxData dados = faceXlsx.getXlsxData();
		
		if(dados == null){
			JOptionPane.showMessageDialog(getParent(), "Voc� precisa carregar o arquivo (*.xlsx) que corresponde a planilha de dados", "Informa��es n�o carregadas", JOptionPane.WARNING_MESSAGE);
			return;
		}
		
		
		if(faceXlsx.getAlfa() == 0){
			JOptionPane.showMessageDialog(getParent(), "Voc� precisa preencher uma valor para alfa (voc� retirou a sugest�o de '0.03') ", "Preencha o valor do alfa", JOptionPane.WARNING_MESSAGE);
			return;
		}
		
		
		
		errSerie.clear();

		neuronio.setInfoHandler(new InfoHandler() {

			@Override
			public void updateInfo(double epoca, double erro) {
				if (epoca % 100 == 0) {
					try {
						System.out.printf("\n�poca: %s Erro: %s", epoca, erro);
					} catch (Exception e) {
					}
				}

				errSerie.add(epoca, erro);
			}
		});

		neuronio.treina(dados.getPrimitiveData(), faceXlsx.getAlfa());
		
		if(trainedListener != null){
			trainedListener.setXlsxData(dados);
			trainedListener.setNeuronio(neuronio);
		}
	}
	
	public void setTrainedListener(TrainedListener trainedListener){
		this.trainedListener = trainedListener;
	}
	
	public interface TrainedListener{
		void setXlsxData(XlsxData data);
		void setNeuronio(Neuronio neuronio);
	}

}
