package wender.rna.trab07.ui.treino;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.filechooser.FileFilter;

import wender.rna.trab07.XlsxReader;
import wender.rna.trab07.XlsxReader.XlsxData;
import wender.rna.ui.DoubleJTextField;

public class FaceXlsx extends JPanel {
	public static final int XLSX_ROW_DATA_START = 2;
	public static final int XLSX_HEADER_ROW = 1;
	public static final int XLSX_TOTAL_ROWS = -1;
	public static final int XLSX_TOTAL_COLS = 5;

	private JTextField txXlsPath;
	private JTextField txAlfa;
	private XlsxTable table;
	private XlsxData xlsxData;

	public FaceXlsx() {
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 0, 0, 0 };
		gridBagLayout.rowHeights = new int[] { 0, 0, 0 };
		gridBagLayout.columnWeights = new double[] { 0.0, 1.0, 0.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 0.0, 1.0, Double.MIN_VALUE };
		setLayout(gridBagLayout);

		JLabel lbAlfa = new JLabel("Alfa:");
		GridBagConstraints gc_lbAlfa = new GridBagConstraints();
		gc_lbAlfa.insets = new Insets(0, 0, 5, 5);
		gc_lbAlfa.anchor = GridBagConstraints.EAST;
		gc_lbAlfa.gridx = 0;
		gc_lbAlfa.gridy = 0;
		add(lbAlfa, gc_lbAlfa);
		
		txAlfa = new DoubleJTextField();
		txAlfa.setColumns(10);
		txAlfa.setHorizontalAlignment(JTextField.RIGHT);
		txAlfa.setText("0.0003");
		
		GridBagConstraints gc_txAlfa = new GridBagConstraints();
		gc_txAlfa.insets = new Insets(0, 0, 5, 5);
		gc_txAlfa.anchor = GridBagConstraints.WEST;
		gc_txAlfa.gridx = 1;
		gc_txAlfa.gridy = 0;
		add(txAlfa, gc_txAlfa);

		JLabel lblXlsDados = new JLabel("XLS Dados:");
		GridBagConstraints gbc_lblXlsDados = new GridBagConstraints();
		gbc_lblXlsDados.insets = new Insets(0, 0, 5, 5);
		gbc_lblXlsDados.anchor = GridBagConstraints.EAST;
		gbc_lblXlsDados.gridx = 0;
		gbc_lblXlsDados.gridy = 1;
		add(lblXlsDados, gbc_lblXlsDados);

		txXlsPath = new JTextField();
		txXlsPath.setEditable(false);
		GridBagConstraints gbc_textField = new GridBagConstraints();
		gbc_textField.insets = new Insets(0, 0, 5, 5);
		gbc_textField.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField.gridx = 1;
		gbc_textField.gridy = 1;
		add(txXlsPath, gbc_textField);
		txXlsPath.setColumns(10);

		final JButton btnNewButton = new JButton("Procurar...");
		btnNewButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent event) {
				String userDir = System.getProperty("user.dir");
				File currentDir = new File(userDir, "planilhas");
				JFileChooser fc = new JFileChooser(currentDir);

				fc.setDialogTitle("Escolha a planilha de dados");
				fc.setFileFilter(new FileFilter() {

					@Override
					public String getDescription() {
						return "*.xlsx";
					}

					@Override
					public boolean accept(File f) {
						return f.getName().toLowerCase().endsWith(".xlsx");
					}
				});

				if (fc.showOpenDialog(btnNewButton) == JFileChooser.APPROVE_OPTION) {
					txXlsPath.setText(fc.getSelectedFile().getAbsolutePath());
					try {
						xlsxData = XlsxReader.load(fc.getSelectedFile(), XLSX_HEADER_ROW, XLSX_ROW_DATA_START, XLSX_TOTAL_ROWS, XLSX_TOTAL_COLS);
						xlsxData.print();
						table.load(xlsxData.getData(), xlsxData.getHeaders());
					} catch (Exception e) {
						e.printStackTrace();
						JOptionPane.showMessageDialog(getParent(), "Por favor verifique se voc� escolheu o arquivo correto.\n" + e.getMessage(), "Erro ao abrir planilha de dados", JOptionPane.ERROR_MESSAGE);
					}
				}

			}
		});

		/*// FIXME isso aqui j� carrega tumaticamente o que eu quero, largae
		try {
			File f = new File(System.getProperty("user.dir"), "planilhas\\Tabela de treinamento Adaline.xlsx");
			txXlsPath.setText(f.getAbsolutePath());
			xls = XlsxReader.load(f, 1, 2, -1, 5);
		} catch (Exception e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(getParent(), "Por favor verifique se voc� escolheu o arquivo correto.\n" + e.getMessage(), "Erro ao abrir planilha de dados", JOptionPane.ERROR_MESSAGE);
		}*/

		GridBagConstraints gbc_btnNewButton = new GridBagConstraints();
		gbc_btnNewButton.insets = new Insets(0, 0, 5, 0);
		gbc_btnNewButton.gridx = 2;
		gbc_btnNewButton.gridy = 1;
		add(btnNewButton, gbc_btnNewButton);
		
		table = new XlsxTable();

		JScrollPane scrollPanel = new JScrollPane(table);

		GridBagConstraints gbc_table = new GridBagConstraints();
		gbc_table.gridwidth = 3;
		gbc_table.insets = new Insets(0, 0, 0, 5);
		gbc_table.fill = GridBagConstraints.BOTH;
		gbc_table.gridx = 0;
		gbc_table.gridy = 2;
		add(scrollPanel, gbc_table);

	}

	public XlsxTable getTable() {
		return table;
	}
	
	public XlsxData getXlsxData(){
		return xlsxData;
	}
	
	public double getAlfa(){
		try{
			return Double.parseDouble(txAlfa.getText());
		}catch(Exception ignored){
		}
		
		return 0.001;
	}

	public static void main(String[] args) {
		JFrame frame = new JFrame();
		frame.setTitle("Teste XLS Reader");
		frame.setBounds(100, 100, 900, 400);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 0, 0 };
		gridBagLayout.rowHeights = new int[] { 0, 0 };
		gridBagLayout.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 1.0, Double.MIN_VALUE };
		frame.getContentPane().setLayout(gridBagLayout);

		FaceXlsx xlsPanel = new FaceXlsx();
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.fill = GridBagConstraints.BOTH;
		gbc.gridx = 0;
		gbc.gridy = 0;
		frame.getContentPane().add(xlsPanel, gbc);

		xlsPanel.getTable().addRowListener(new XlsxTable.RowListener() {

			@Override
			public void doubleClick(Double[] row) {
				for (double d : row) {
					System.out.println(d);
				}

			}
		});

		frame.setVisible(true);
	}

}
