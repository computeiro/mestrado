package wender.rna.trab07.ui.treino;

import javax.swing.JPanel;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import java.awt.GridBagConstraints;
import javax.swing.JTextField;
import java.awt.Insets;
import javax.swing.JButton;

public class FaceFormPanel extends JPanel {
	private JTextField textField;
	private JTextField textField_1;
	public FaceFormPanel() {
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 0, 0, 0};
		gridBagLayout.rowHeights = new int[]{0, 0, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 1.0, 0.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		setLayout(gridBagLayout);
		
		JLabel lblAlfa = new JLabel("Alfa:");
		GridBagConstraints gbc_lblAlfa = new GridBagConstraints();
		gbc_lblAlfa.insets = new Insets(0, 0, 5, 5);
		gbc_lblAlfa.anchor = GridBagConstraints.EAST;
		gbc_lblAlfa.gridx = 0;
		gbc_lblAlfa.gridy = 0;
		add(lblAlfa, gbc_lblAlfa);
		
		textField = new JTextField();
		GridBagConstraints gbc_textField = new GridBagConstraints();
		gbc_textField.anchor = GridBagConstraints.WEST;
		gbc_textField.insets = new Insets(0, 0, 5, 5);
		gbc_textField.gridx = 1;
		gbc_textField.gridy = 0;
		add(textField, gbc_textField);
		textField.setColumns(10);
		
		JLabel lblPlanilha = new JLabel("Planilha:");
		GridBagConstraints gbc_lblPlanilha = new GridBagConstraints();
		gbc_lblPlanilha.anchor = GridBagConstraints.EAST;
		gbc_lblPlanilha.insets = new Insets(0, 0, 0, 5);
		gbc_lblPlanilha.gridx = 0;
		gbc_lblPlanilha.gridy = 1;
		add(lblPlanilha, gbc_lblPlanilha);
		
		textField_1 = new JTextField();
		GridBagConstraints gbc_textField_1 = new GridBagConstraints();
		gbc_textField_1.insets = new Insets(0, 0, 0, 5);
		gbc_textField_1.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_1.gridx = 1;
		gbc_textField_1.gridy = 1;
		add(textField_1, gbc_textField_1);
		textField_1.setColumns(10);
		
		JButton btnAbrirXls = new JButton("Abrir...");
		GridBagConstraints gbc_btnAbrirXls = new GridBagConstraints();
		gbc_btnAbrirXls.gridx = 2;
		gbc_btnAbrirXls.gridy = 1;
		add(btnAbrirXls, gbc_btnAbrirXls);
	}

}
