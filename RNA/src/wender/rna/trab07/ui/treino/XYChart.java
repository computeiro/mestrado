package wender.rna.trab07.ui.treino;

import java.awt.Color;

import javax.swing.JFrame;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

public class XYChart {
	private ChartPanel chartPanel;
	private JFreeChart chart;

	public XYChart(final String title, XYDataset dataSet, int width, int height) {
		chart = createChart(dataSet);
		chartPanel = new ChartPanel(chart);
		chartPanel.setPreferredSize(new java.awt.Dimension(width, height));
	}

	public ChartPanel getChartPanel() {
		return chartPanel;
	}

	private JFreeChart createChart(final XYDataset dataset) {

		final JFreeChart chart = ChartFactory.createXYLineChart("Quantidade de Erros no decorrer das Eras", // chart
				// title
				"Eras", // x axis label
				"Quantidade", // y axis label
				dataset, // data
				PlotOrientation.VERTICAL, false, // include legend
				true, // tooltips
				false // urls
				);

		chart.setBackgroundPaint(Color.white);

		final XYPlot plot = chart.getXYPlot();
		plot.setBackgroundPaint(Color.lightGray);
		plot.setDomainGridlinePaint(Color.white);
		plot.setRangeGridlinePaint(Color.white);

		final XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
		renderer.setSeriesLinesVisible(1, false);
		renderer.setSeriesShapesVisible(0, false);
		plot.setRenderer(renderer);
		

		final NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
		rangeAxis.setStandardTickUnits(NumberAxis.createStandardTickUnits());

		return chart;

	}
	
	public static void main(String[] args) {
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setBounds(100, 100, 800, 600);
		
		final XYSeriesCollection dataset = new XYSeriesCollection();
		XYSeries errSerie = new XYSeries("Erros");
		dataset.addSeries(errSerie);
		
		XYChart xyChart = new XYChart("Treinamento", dataset, 600, 300);
		
		errSerie.clear();
		for(int epoca = 1; epoca < 1000; epoca++){
			errSerie.add(epoca, 1000 / epoca );
		}
		
		frame.add(xyChart.getChartPanel());
		frame.setVisible(true);
	}
}