package wender.rna.trab07.ui.treino;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class XlsxTable extends JTable {
	private MouseAdapter mouseListener;
	private RowListener rowListener;
	private Double data[][];
	
	
	public void load(Double[][] xlsData, String[] colNames){
		setModel(new XlsxTableModel(xlsData, colNames));
		
		this.data = xlsData;
		
		mouseListener = new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() == 2) {
					JTable target = (JTable) e.getSource();
					int row = target.getSelectedRow();
					
					if(rowListener != null){
						rowListener.doubleClick(data[row]);
					}
				}
			}
		};
		
		addMouseListener(mouseListener);
	}
	
	public void addRowListener(RowListener listener){
		this.rowListener = listener;
	}
	
	
	private static class XlsxTableModel extends DefaultTableModel {
		
		public XlsxTableModel(Double data[][], String[] columsNames){
			super(data, columsNames);
		}
		
		@Override
		public boolean isCellEditable(int rowIndex, int columnIndex) {
			return false;
		}

	}
	
	public interface RowListener{
		void doubleClick(Double[] row);
	}

}
