package wender.rna.trab07;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Iterator;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


public class XlsxReader {

	public static XlsxData load(File xlsxFile, int headerRow, int dataStarRow, int qtdDataRows, int qtdCols) throws Exception{

		if (dataStarRow < 0) {
			throw new IllegalArgumentException("Linha negativa n�o faz sentido, a linha inicial de dados da planilha � sempre maior ou igual a zero!");
		}

		XlsxData xlsx = new XlsxData();

		InputStream in = null;

		try {
			in = new FileInputStream(xlsxFile);
			XSSFWorkbook wb = new XSSFWorkbook(in);

			XSSFSheet sheet = wb.getSheetAt(0);

			int currentRow = 0;
			
			if (headerRow > 0) {

				currentRow = headerRow;
				
				XSSFRow rowHeader = sheet.getRow(currentRow);
				
				if(qtdCols <= 0){
					qtdCols = rowHeader.getLastCellNum() +1;
				}
				
				xlsx.headers = new String[qtdCols];
				
				for(int c = 0; c < qtdCols; c++){
					xlsx.headers[c] = rowHeader.getCell(c).toString();
				}

			}
			
			currentRow = dataStarRow;
			
			if(qtdDataRows <= 0){
				qtdDataRows = sheet.getLastRowNum() +1 - dataStarRow;
			}
			
			
			if(qtdCols <= 0){
				XSSFRow firtDataRow =sheet.getRow(currentRow);
				qtdCols = firtDataRow.getLastCellNum() +1;
			}
			
			
			xlsx.data = new Double[qtdDataRows][qtdCols];
			xlsx.primitiveData = new double[qtdDataRows][qtdCols];
			
			while(currentRow < (dataStarRow + qtdDataRows)){
				XSSFRow row = sheet.getRow(currentRow);
				int linha = currentRow - dataStarRow;

				for (int col = 0; col < qtdCols; col++) {
					XSSFCell cell = row.getCell(col);
					String value = cell.toString();
					
					try{
						xlsx.data[linha][col] = Double.parseDouble(value);
						xlsx.primitiveData[linha][col] = Double.parseDouble(value);
					}catch(Exception e){
						e.printStackTrace();
						System.out.printf("\nErro ao processar linha %s coluna %s valor %s", linha, col, value);
					}
				}
				
				currentRow++;
			}
			

		} finally {
			if (in != null) {
				in.close();
			}
		}

		return xlsx;
	}
	
	public static class XlsxData {
		private String[] headers;
		private Double[][] data;
		private double[][] primitiveData;
		
		public String[] getHeaders() {
			return headers;
		}
		
		public Double[][] getData() {
			return data;
		}
		
		public double[][] getPrimitiveData(){
			return primitiveData;
		}
		
		public void print(){
			int count = 0;
			
			for(String h : headers){
				System.out.println(count + " -> " + h);
				count++;
			}
			
			StringBuilder stb = new StringBuilder();
			
			
			for(int i = 0; i < data.length; i++){
				stb.append("\n").append(i).append(" -> ");
				
				for(int j = 0; j < data[i].length; j ++){
					stb.append(data[i][j]).append(" ");
				}
				
			}
			
			System.out.println(stb.toString());
			
		}
	}
	
	public static void main(String[] args) throws Exception{
		File f = new File(System.getProperty("user.dir"), "planilhas\\Tabela de treinamento Adaline.xlsx");
		XlsxData xlsx = load(f, 1, 2, -1, 5);
		
		xlsx.print();
	}

}
