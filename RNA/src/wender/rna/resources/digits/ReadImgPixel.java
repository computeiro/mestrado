package wender.rna.resources.digits;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.util.HashSet;

import javax.imageio.ImageIO;

public class ReadImgPixel {
    public static final int ALPHA_255 = 16777216;
    public static final int LINHAS = 128;
    public static final int COLUNAS = 128;

    public static final int BLACK = 1;
    public static final int WHITE = -1;
    
    public static final String TIMES = "times_";
    public static final String COURRIER = "courier_";
    public static final String ARIAL = "arial_";
    
    private static int[][][][] amostras;

    public static void main(String[] args) throws Exception {
    }

    public static int[][][][] getAmostras() throws Exception {
	
	if(amostras == null){
	    amostras = new int[3][10][][];
	    int fonteIndex = 0;
	    
	    for(String fonte : new String[]{TIMES, COURRIER, ARIAL} ){
		for(int n = 0; n < 10; n++){
		    amostras[fonteIndex][n] = getMatrix(ImageIO.read(ReadImgPixel.class.getResource( fonte + n + ".png")));
		}
		
		fonteIndex++;
	    }
	}
	
	return amostras;
    }
    

    private static int[][] getMatrix(BufferedImage image) {

	final byte[] pixels = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
	final int width = image.getWidth();
	final int height = image.getHeight();
	final boolean hasAlphaChannel = image.getAlphaRaster() != null;

	int[][] result = new int[height][width];
	if (hasAlphaChannel) {
	    final int pixelLength = 4;
	    for (int pixel = 0, row = 0, col = 0; pixel < pixels.length; pixel += pixelLength) {
		long argb = 0;
		argb += (((int) pixels[pixel] & 0xff) << 24); // alpha
		argb += ((int) pixels[pixel + 1] & 0xff); // blue
		argb += (((int) pixels[pixel + 2] & 0xff) << 8); // green
		argb += (((int) pixels[pixel + 3] & 0xff) << 16); // red
		argb += argb + ALPHA_255; // n�o consideramos o Alpha

		if (argb == 0) {
		    result[row][col] = BLACK;
		} else {
		    result[row][col] = WHITE;
		}

		col++;
		if (col == width) {
		    col = 0;
		    row++;
		}
	    }
	} else {
	    HashSet<Integer> hash = new HashSet<Integer>();
	    final int pixelLength = 3;
	    for (int pixel = 0, row = 0, col = 0; pixel < pixels.length; pixel += pixelLength) {
		// int argb = 0;

		if (((int) pixels[pixel]) == WHITE) {
		    result[row][col] = WHITE;
		} else {
		    result[row][col] = BLACK;
		}

		/*
		 * //argb -= ALPHA_255; // 255 alpha argb += ((int)
		 * pixels[pixel] & 0xff); // blue argb += (((int) pixels[pixel +
		 * 1] & 0xff) << 8); // green argb += (((int) pixels[pixel + 2]
		 * & 0xff) << 16); // red
		 * 
		 * if(argb == 0){ result[row][col] = BLACK; }else{
		 * result[row][col] = WHITE; }
		 */
		col++;
		if (col == width) {
		    col = 0;
		    row++;
		}
	    }

	    System.out.println("Hash:");
	    for (Integer i : hash) {
		System.out.println(i);
	    }
	}

	return result;
    }
}