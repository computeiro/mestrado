package wender.rna.functions;

import javax.swing.JFrame;

import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import wender.rna.trab07.ui.treino.XYChart;

public class SigmoidBipolar implements ActivationFunction {
	
	public double f(double x){
		return  (2 / (1 + Math.exp(- x) ) - 1) ;
	}
	
	public double fl(double x){
		return 0.5 * (1 + f(x)) * (1 - f(x)) ;
	}
	
	public static void main(String[] args) {
		ActivationFunction act = new SigmoidBipolar();
		
		
		final XYSeriesCollection dataset = new XYSeriesCollection();
		XYSeries errSerie = new XYSeries("Erros");
		dataset.addSeries(errSerie);
		
		XYChart xyc = new XYChart("Teste fun��o", dataset, 800, 600);
		
		
		for(double i = -5; i < 5; i = i + 0.1){
			errSerie.add(i, act.f(i));
		}
		
		
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(800, 600);
		frame.add(xyc.getChartPanel());
		
		frame.setVisible(true);
		
		
	}

}
