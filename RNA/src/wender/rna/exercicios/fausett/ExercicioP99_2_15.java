package wender.rna.exercicios.fausett;

public class ExercicioP99_2_15 {
	private static final double alfa = 0.01;
	private static final int w_bias = 4;
	private static final int y_col = 4;
	private static final int LIMIAR = 0;

	private int[][] inf;
	private int[] weight;
	private int age;

	public static void main(String[] args) {
		ExercicioP99_2_15 exerc = new ExercicioP99_2_15();
		exerc.trainning();
		exerc.showResult();

	}

	public ExercicioP99_2_15() {
		// x1, x2, x3, x4, bias
		this.weight = new int[] { 0, 0, 0, 0, 0 };

		this.inf = new int[][] {
// @formatter:off
				// x1 x2  x3  x4   y
				{  1,  1,  1,  1,  1 }, 
				{ -1,  1, -1, -1,  1 }, 
				{  1,  1,  1, -1, -1 }, 
				{  1, -1, -1,  1, -1 }

// @formatter:on
		};
	}

	public int calculateY(int[] line) {
		int yl = 0;

		for (int i = 0; i < y_col; i++) {
			yl += line[i] * weight[i];
		}

		yl += weight[w_bias];

		if (yl >= LIMIAR) {
			return 1;
		}

		return -1;
	}

	public void trainning() {
		weight = new int[] { 0, 0, 0, 0, 0 };
		
		try{
			while (true) {
	
				if (weightTest()) {
					break;
				} else {
					age++;
					System.out.println("�poca: " + age);
	
					for (int l = 0; l < inf.length; l++) {
	
						for (int c = 0; c < y_col; c++) {
							weight[c] += alfa * inf[l][c] * inf[l][y_col];
						}
	
						weight[w_bias] += alfa * inf[l][y_col];
					}
					
					showResult();
				}
	
			}
		}catch(StackOverflowError stackErr){
			System.out.println("Rodou a at� envermelhar e estourar a mem�ria. �poca: " + age);
		}
	}

	public boolean weightTest(){
		for(int i = 0; i < inf.length -1; i++){
			if(calculateY(inf[i]) != inf[i][y_col]){
				return false;
			}
		}
		
		return true;
	}

	public void showResult() {
		for (int l = 0; l < inf.length; l++) {
			int yValue = calculateY(inf[l]);

			if (inf[l][y_col] == yValue) {
				System.out.printf("\nAmostra %s correta! ", l);
			} else {
				System.out.printf("\nAmostra %s INCORRETA! Esperado %s encontrado %s", l, inf[l][y_col], yValue);
			}
		}
		System.out.println("\n\n");
		for (int p = 0; p < w_bias; p++) {
			System.out.println("w" + (p + 1) + ": " + weight[p]);
		}

		System.out.println("bias: " + weight[w_bias]);
		System.out.println("\n\n");
	}

}
