package wender.rna.exercicios.fausett;


public class ExercicioP98_2_5 {
	private static final int w_bias = 4;
	private static final int y_col = 4;
	private static final int LIMIAR = 0;
	
	private int[][] inf;
	private int[] weight;

	public static void main(String[] args) {
		ExercicioP98_2_5 exerc = new ExercicioP98_2_5();
		exerc.trainning();
		exerc.test();
		
	}

	public ExercicioP98_2_5() {
		this.weight = new int[] { 0, 0, 0, 0, 0 };

		this.inf = new int[][] {
//@formatter:off
//			  x1  x2  x3  x4   y
			{  1,  1,  1,  1,  1},
			{ -1,  1, -1, -1,  1},
			{  1,  1,  1, -1, -1},
			{  1, -1, -1,  1, -1}
						
//@formatter:on
		};
	}

	public int calculateY(int[] line) {
		int y = 0;

		for (int i = 0; i < line.length; i++) {
			y += line[i] * weight[i];
		}

		y += weight[w_bias];
		
		if(y >= LIMIAR){
			return 1;
		}
		
		return -1;
	}
	
	public void trainning(){
		int[] newWeight = new int[] { 0, 0, 0, 0, 0 };
		
		for(int l = 0; l < inf.length; l++){
			

			for(int c = 0; c < inf[l].length; c++){
				newWeight[c] += inf[l][c] * inf[l][y_col];
			}
			
			newWeight[w_bias] += inf[l][y_col];
		}
		
		weight = newWeight;
		
	}
	
	public void test(){
		for(int l = 0; l < inf.length; l++){
			int yValue = calculateY(inf[l]);
			
			if(inf[l][y_col] == yValue){
				System.out.printf("\nLinha %s correta! ", l);
			}else{
				System.out.printf("\nLinha %s ERRADA! Esperado %s encontrado %s\n\n", l, inf[l][y_col],  yValue);
			}
		}
		System.out.println("\n");
		
		for(int p = 0; p < w_bias; p++){
			System.out.println("w"+ (p+1) + ": " + weight[p]);
		}
		
		System.out.println("bias: " + weight[w_bias]);
	}

}
