package wender.ag;

public class Cromossomo {
    private int value;
    private String genes;

    public static void main(String[] args) {
	Cromossomo c1 = new Cromossomo(0, 9);
	Cromossomo c2 = new Cromossomo(511, 9);
	
	
	System.out.println(c1.genes);
	System.out.println(c2.genes);
	System.out.println(c1.crossOverUmPonto(c2).genes);
	
    }

    public Cromossomo(int value, int qtdGenes) {
	this.value = value;
	StringBuilder sb = new StringBuilder();

	sb.append(Integer.toBinaryString(Math.abs(value)));

	while (sb.length() < qtdGenes) {
	    sb.insert(1, "0");
	}

	genes = sb.toString();
    }

    public Cromossomo(String genes) {
	this.genes = genes;

	value = Integer.parseInt(genes.substring(0), 2);
    }

    public int getValue() {
	return value;
    }

    public void setValue(int value) {
	this.value = value;
    }

    public String getGenes() {
	return genes;
    }

    public void setGenes(String genes) {
	this.genes = genes;
    }

    public double getAvaliacao() {
	return Genoma.funcaoAvaliacao(value);
    }
    
    public void  mutacao(double change){
	String inicio, fim = null;
	
	for(int i = 0; i < genes.length(); i++){
	    if(Math.random() < change){
		char aux = genes.charAt(i);
		aux = (aux == '0') ? '1' : '0';
		inicio = genes.substring(0, i);
		fim = genes.substring(i+1, genes.length());
		Cromossomo c = new Cromossomo(inicio + aux + fim);
		
		this.genes = c.getGenes();
		this.value = c.getValue();
	    }
	}
    }
    
    public Cromossomo crossOverUmPonto(Cromossomo p2) {
	int ponto = (int)  ((genes.length() -1) * Math.random());
   	return new Cromossomo(this.getGenes().substring(0, ponto) + p2.getGenes().substring(ponto) );
    }
}
