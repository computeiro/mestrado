package wender.ag;


import wender.ag.populacao.ModuloPopulacional;
import wender.ag.selecao.Selecao;

public interface AgConf {
    Selecao getSelecao();
    double getTaxaMutacao();
    ModuloPopulacional getModuloPopulacional();
    int getTamanhoPopulacao();
    int getLimiteGeracao();
}
