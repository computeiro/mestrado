package wender.ag.selecao;

import java.util.List;

import wender.ag.Cromossomo;

public interface Selecao {
    void init(List<Cromossomo> populacao);
    Cromossomo get();
}
