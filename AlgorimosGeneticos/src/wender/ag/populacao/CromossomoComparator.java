package wender.ag.populacao;

import java.util.Comparator;

import wender.ag.Cromossomo;

public class CromossomoComparator implements Comparator<Cromossomo> {

    @Override
    public int compare(Cromossomo o1, Cromossomo o2) {
	double a1 = o1.getAvaliacao();
	double a2 = o2.getAvaliacao();
	
	if(a1 == a2){
	    return 0;
	}
	
	if(a1 > a2){
	    return 1;
	}
	
	return -1;
    }

}
