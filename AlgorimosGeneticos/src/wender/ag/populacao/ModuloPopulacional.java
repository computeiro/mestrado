package wender.ag.populacao;

import java.util.List;

import wender.ag.Cromossomo;

public interface ModuloPopulacional {
    List<Cromossomo> atualizaPopulacao(List<Cromossomo> antiga, List<Cromossomo> nova);
}
