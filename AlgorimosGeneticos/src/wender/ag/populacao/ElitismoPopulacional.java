package wender.ag.populacao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import wender.ag.Cromossomo;
import wender.ag.Genoma;

public class ElitismoPopulacional implements ModuloPopulacional {
    private int individuosElite;
    private CromossomoComparator comparator;
    
    @Override
    public List<Cromossomo> atualizaPopulacao(List<Cromossomo> antiga, List<Cromossomo> nova) {
	
	if(individuosElite > 0){
	
	    Collections.sort(antiga, comparator);
	    Collections.sort(nova, comparator);
	    int size = nova.size();

	    for (int i = 0; i < individuosElite; i++) {
		nova.set(size - i - 1, antiga.get(i));
	    }
	}
	
	return nova;
    }
    
    public ElitismoPopulacional(int individuosElite){
	this.individuosElite = individuosElite;
	this.comparator = new CromossomoComparator();
    }
    
    public static void main(String[] args) {
	List<Cromossomo> antiga = new ArrayList<Cromossomo>();
	
	int[] a = new int[]{0, 410, 421, 10, 100};
	StringBuilder stb = new StringBuilder();
	
	stb.append("Antiga:");
	for(int v : a){
	    Cromossomo c = new Cromossomo(v, Genoma.TC);
	    stb.append("\nValor: ").append(v);
	    stb.append(" Rate: ").append(c.getAvaliacao());
	    antiga.add(c);
	}
	
	List<Cromossomo> nova = new ArrayList<Cromossomo>();
	int[] n = new int[]{4, 1, 200, 300, 400};
	
	stb.append("\n\nNova:");
	
	for(int v : n){
	    Cromossomo c = new Cromossomo(v, Genoma.TC);
	    stb.append("\nValor: ").append(v);
	    stb.append(" Rate: ").append(c.getAvaliacao());
	    nova.add(c);
	}
	
	nova =  new ElitismoPopulacional(2).atualizaPopulacao(antiga, nova);
	Collections.sort(nova, new CromossomoComparator());
	
	stb.append("\n\nResultado:");
	for(Cromossomo c : nova){
	    stb.append("\nValor: ").append(c.getValue());
	    stb.append(" Rate: ").append(c.getAvaliacao());
	}
	
	System.out.println(stb.toString());
    }
    
}
