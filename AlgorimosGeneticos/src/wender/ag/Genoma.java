package wender.ag;

import java.awt.EventQueue;
import java.util.ArrayList;
import java.util.List;

import wender.ag.populacao.PopulacaoChangeListner;
import wender.ag.selecao.Selecao;

public class Genoma {
    public static final int TC = 9; // tamanho do cromossomo
    public static final int MAIOR_CROMOSSOMO = 512;

    private List<Cromossomo> geracaoAtual;
    private List<Cromossomo> geracaoNova;

    private AgConf c;
    private int geracao;

    public static void main(String[] args) {
	for (int i = 0; i < 20; i++)
	    System.out.println(funcaoAvaliacao(i));
    }

    public Genoma(AgConf conf) {
	this.c = conf;

    }

    public void geracao(final PopulacaoChangeListner listener) {

	Runnable r = new Runnable() {

	    @Override
	    public void run() {
		// TODO Auto-generated method stub

		if (geracaoAtual == null) {
		    inicializaPopulacao();
		}

		Selecao sel = c.getSelecao();
		sel.init(geracaoAtual);

		for (int g = 0; g < c.getLimiteGeracao(); g++) {
		    geracaoNova = new ArrayList<Cromossomo>();
		    c.getSelecao().init(geracaoAtual);

		    for (int i = 0; i < c.getTamanhoPopulacao(); i++) {
			Cromossomo p1 = sel.get();
			Cromossomo p2 = sel.get();

			Cromossomo filho = p1.crossOverUmPonto(p2);
			filho.mutacao(c.getTaxaMutacao());
			geracaoNova.add(filho);
		    }

		    geracaoAtual = c.getModuloPopulacional().atualizaPopulacao(geracaoAtual, geracaoNova);
		    geracao++;

		    if (listener != null) {
			try {
			    EventQueue.invokeAndWait(listener);
			} catch (Exception e) {

			}
		    }

		}

	    }
	};
	
	Thread t = new Thread(r);
	t.start();
    }

    public void inicializaPopulacao() {
	geracaoAtual = new ArrayList<Cromossomo>();

	for (int i = 0; i < c.getTamanhoPopulacao(); i++) {
	    double v = 1;
	    while (v == 1) {
		v = Math.random() * MAIOR_CROMOSSOMO;
	    }

	    Cromossomo c = new Cromossomo((int) v, TC);
	    geracaoAtual.add(c);
	}
    }

    public static double funcaoAvaliacao(long x) {
	double y = -Math.abs(x * Math.sin(Math.sqrt(Math.abs(x))));
	return y;
    }

    public AgConf getConfiguracao() {
	return c;
    }

    public List<Cromossomo> getGeracaoAtual() {
	return geracaoAtual;
    }

    public List<Cromossomo> getGeracaoNova() {
	return geracaoNova;
    }

    public int getGeracao() {
	return geracao;
    }

}
