package wender.ag;


import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.Marker;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.ValueMarker;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import wender.ag.populacao.PopulacaoChangeListner;

public class ExecucaoPanel extends JPanel {
    private static final long serialVersionUID = 1L;
    private ChartPanel chartPanel;
    private Genoma genoma;
    private boolean iniciouPopulacao;

    public ExecucaoPanel(AgConf conf) {
	this.genoma = new Genoma(conf);

	GridBagLayout gridBagLayout = new GridBagLayout();
	gridBagLayout.columnWidths = new int[] { 0, 0, 0 };
	gridBagLayout.rowHeights = new int[] { 0, 0, 0 };
	gridBagLayout.columnWeights = new double[] { 0.0, 1.0, Double.MIN_VALUE };
	gridBagLayout.rowWeights = new double[] { 0.0, 1.0, Double.MIN_VALUE };
	setLayout(gridBagLayout);

	JButton btnInicializaPopulao = new JButton("Inicializa População");
	btnInicializaPopulao.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent e) {
		iniciouPopulacao = true;
		genoma.inicializaPopulacao();
		atualizaMarcadores();
	    }
	});

	GridBagConstraints gbc_btnInicializaPopulao = new GridBagConstraints();
	gbc_btnInicializaPopulao.anchor = GridBagConstraints.WEST;
	gbc_btnInicializaPopulao.insets = new Insets(0, 0, 5, 5);
	gbc_btnInicializaPopulao.gridx = 0;
	gbc_btnInicializaPopulao.gridy = 0;
	add(btnInicializaPopulao, gbc_btnInicializaPopulao);

	btnInicializaPopulao.setIcon(new ImageIcon(Ag3App.class.getResource("resources/populacao.png")));

	final JButton btnExecutar = new JButton("Executar!");
	GridBagConstraints gbc_btnExecutar = new GridBagConstraints();
	gbc_btnExecutar.anchor = GridBagConstraints.WEST;
	gbc_btnExecutar.insets = new Insets(0, 0, 5, 0);
	gbc_btnExecutar.gridx = 1;
	gbc_btnExecutar.gridy = 0;
	add(btnExecutar, gbc_btnExecutar);
	btnExecutar.setIcon(new ImageIcon(Ag3App.class.getResource("resources/raio.png")));

	chartPanel = createChartPanel();
	GridBagConstraints gbc_panel = new GridBagConstraints();
	gbc_panel.gridwidth = 2;
	gbc_panel.insets = new Insets(0, 0, 0, 5);
	gbc_panel.fill = GridBagConstraints.BOTH;
	gbc_panel.gridx = 0;
	gbc_panel.gridy = 1;
	add(chartPanel, gbc_panel);
	
	btnExecutar.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent e) {
		
		btnExecutar.setEnabled(false);
		
		try{
		    	if(!iniciouPopulacao){
		    	    genoma.inicializaPopulacao();
		    	}
		    	
		    	atualizaMarcadores();
		    	
        		genoma.geracao(new PopulacaoChangeListner() {
			    
			    public void run() {
				atualizaMarcadores();
			    }
        		});
		}finally{
		    btnExecutar.setEnabled(true);
		}
	    }
	});

    }

    public void atualizaMarcadores() {
	XYPlot p = chartPanel.getChart().getXYPlot();
	p.clearDomainMarkers();

	for (Cromossomo c : genoma.getGeracaoAtual()) {
	    Marker m = new ValueMarker(c.getValue());
	    m.setPaint(Color.red);
	    p.addDomainMarker(m);
	}
    }

    public ChartPanel createChartPanel() {
	final XYSeriesCollection dataset = new XYSeriesCollection();
	XYSeries serie = new XYSeries("Função");
	dataset.addSeries(serie);

	// Inicializa valores no gráfico
	for(long i = 0; i < 513; i++){
	    serie.add(i, Genoma.funcaoAvaliacao(i));
	}
	

	JFreeChart chart = ChartFactory.createXYLineChart(null,// title
		"x", // x axis label
		"f(x)", // y axis label
		dataset, // data
		PlotOrientation.VERTICAL, false, // include legend
		true, // tooltips
		false // urls
		);

	chart.setBackgroundPaint(Color.white);

	final XYPlot plot = chart.getXYPlot();
	plot.setBackgroundPaint(Color.lightGray);
	plot.setDomainGridlinePaint(Color.white);
	plot.setRangeGridlinePaint(Color.white);
	plot.getRenderer().setSeriesPaint(0, new Color(0x00, 0x00, 0xFF));

	final NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
	rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());

	return new ChartPanel(chart);
    }
}
