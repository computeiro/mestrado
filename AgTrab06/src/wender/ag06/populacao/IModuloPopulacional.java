package wender.ag06.populacao;

import java.util.List;

import wender.ag06.Cromossomo;

public interface IModuloPopulacional {
    List<Cromossomo> atualizaPopulacao(List<Cromossomo> antiga, List<Cromossomo> nova);
}
