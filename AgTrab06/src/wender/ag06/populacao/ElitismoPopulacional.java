package wender.ag06.populacao;

import java.util.Collections;
import java.util.List;

import wender.ag06.Cromossomo;
import wender.ag06.CromossomoComparator;

public class ElitismoPopulacional implements IModuloPopulacional {
    private int individuosElite;
    private CromossomoComparator comparator;
    
    @Override
    public List<Cromossomo> atualizaPopulacao(List<Cromossomo> antiga, List<Cromossomo> nova) {
	
	if(individuosElite > 0){
	
	    Collections.sort(antiga, comparator);
	    Collections.sort(nova, comparator);
	    int size = nova.size();

	    for (int i = 0; i < individuosElite; i++) {
		nova.set(size - i - 1, antiga.get(i));
	    }
	}
	
	return nova;
    }
    
    public ElitismoPopulacional(int individuosElite){
	this.individuosElite = individuosElite;
	this.comparator = new CromossomoComparator();
    }
    
    public static void main(String[] args) {
	
    }
    
}
