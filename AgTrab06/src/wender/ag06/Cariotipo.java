package wender.ag06;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import wender.ag06.populacao.ElitismoPopulacional;
import wender.ag06.populacao.IModuloPopulacional;
import wender.ag06.selecao.ISelecao;
import wender.ag06.selecao.Rank;

public class Cariotipo {
    public final int BOARD_SIZE = 64;
    
    private AgConf conf;
    private List<Cromossomo> populacao;
    private int geracao;
    private Cromossomo melhorIndividuo;
    private double mediaPopulacao;
    
    private Cariotipo(AgConf conf){
	this.conf = conf;
    }

    public int getGeracao() {
	return geracao;
    }

    public Cromossomo getMelhorIndividuo() {
	return melhorIndividuo;
    }

    public double getMediaPopulacao() {
	return mediaPopulacao;
    }

    public void executaEvoluacao() {
	ExecutorEvolucao ee = new ExecutorEvolucao();
	new Thread(ee).start();
    }

    public static String asString(List<Integer> list) {
	StringBuilder s = new StringBuilder();

	s.append("{");

	for (Iterator<Integer> ite = list.iterator(); ite.hasNext();) {
	    s.append(ite.next());

	    if (ite.hasNext()) {
		s.append(", ");
	    }
	}
	s.append("}");
	return s.toString();
    }

    private class ExecutorEvolucao implements Runnable {

	ExecutorEvolucao() {
	}

	@Override
	public void run() {

	    final StringBuilder log = new StringBuilder();

	    ArrayList<BoardSquare> base = new ArrayList<BoardSquare>();

	    for (int i = 0; i < BOARD_SIZE; i++) {
		base.add(new BoardSquare(i));
	    }

	    // Inicializando populacação
	    populacao = new ArrayList<>();

	    for (int i = 0; i < conf.getTamanhoPopulacao(); i++) {
		ArrayList<BoardSquare> ordem = (ArrayList<BoardSquare>) base.clone();
		Collections.shuffle(ordem);

		populacao.add(new Cromossomo(ordem));
	    }

	    ISelecao selecao = conf.getSelecao();
	    selecao.init(populacao);

	    IModuloPopulacional modPop = conf.getModuloPopulacional();

	    for (geracao = 0; geracao < conf.getLimiteGeracoes(); geracao++) {
		List<Cromossomo> novaPopulacao = new ArrayList<>();

		for (int p = 0; p < populacao.size(); p++) {
		    Cromossomo c1 = selecao.get();
		    Cromossomo c2 = selecao.get();

		    Cromossomo f = c1.crossover(c2);
		    
		    if(Math.random() > conf.getTaxaMutacao()){
			f.mutacao();
		    }

		    novaPopulacao.add(p, f);
		}

		populacao = modPop.atualizaPopulacao(populacao, novaPopulacao);

		double soma = populacao.get(0).getAvaliacao();
		melhorIndividuo = populacao.get(0);

		for (int i = 1; i < populacao.size(); i++) {

		    if (populacao.get(i).getAvaliacao() < melhorIndividuo.getAvaliacao()) {
			melhorIndividuo = populacao.get(i);
		    }

		    soma += populacao.get(i).getAvaliacao();
		}

		mediaPopulacao = soma / populacao.size();

		System.out.println("         Geração: " + geracao);
		System.out.println("Melhor indivíduo: " + melhorIndividuo.getAvaliacao());
		System.out.println("           Média: " + mediaPopulacao);

	    }

	    System.out.println(log.toString());
	    

	}

    }
    
    public static void main(String[] args) {
	AgConf conf = new AgConf();
	conf.setLimiteGeracoes(50000);
	conf.setModuloPopulacional(new ElitismoPopulacional(2));
	conf.setSelecao(new Rank());
	conf.setTamanhoPopulacao(50);
	conf.setTaxaMutacao(0.01);
	
	new Cariotipo(conf).executaEvoluacao();;
    }

}
