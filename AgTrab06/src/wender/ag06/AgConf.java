package wender.ag06;

import wender.ag06.populacao.IModuloPopulacional;
import wender.ag06.selecao.ISelecao;

public class AgConf {
    private int tamanhoPopulacao;
    private int limiteGeracoes;
    public int getLimiteGeracoes() {
        return limiteGeracoes;
    }

    public void setLimiteGeracoes(int limiteGeracoes) {
        this.limiteGeracoes = limiteGeracoes;
    }

    private double taxaMutacao;
    private ISelecao selecao;
    private IModuloPopulacional moduloPopulacional;

    public ISelecao getSelecao() {
	return selecao;
    }

    public void setSelecao(ISelecao selecao) {
	this.selecao = selecao;
    }

    public int getTamanhoPopulacao() {
	return tamanhoPopulacao;
    }

    public void setTamanhoPopulacao(int tamanhoPopulacao) {
	this.tamanhoPopulacao = tamanhoPopulacao;
    }

    public double getTaxaMutacao() {
	return taxaMutacao;
    }

    public void setTaxaMutacao(double taxaMutacao) {
	this.taxaMutacao = taxaMutacao;
    }

    public IModuloPopulacional getModuloPopulacional() {
        return moduloPopulacional;
    }

    public void setModuloPopulacional(IModuloPopulacional moduloPopulacional) {
        this.moduloPopulacional = moduloPopulacional;
    }
}
