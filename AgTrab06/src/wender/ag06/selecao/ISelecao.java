package wender.ag06.selecao;

import java.util.List;

import wender.ag06.Cromossomo;

public interface ISelecao {
    void init(List<Cromossomo> populacao);
    Cromossomo get();
}
