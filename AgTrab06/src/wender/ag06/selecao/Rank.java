package wender.ag06.selecao;

import java.util.List;

import wender.ag06.Cromossomo;

public class Rank implements ISelecao {
    private List<Cromossomo> populacao;
    
    @Override
    public void init(List<Cromossomo> populacao) {
	this.populacao = populacao;
    }

    @Override
    public Cromossomo get() {
	int index = (int) Math.random() * (populacao.size() -1);
	
	return populacao.get(index);
    }

}
