package wender.ag06.selecao;

import java.util.List;

import wender.ag06.Cromossomo;


public class Roleta implements ISelecao {
    private List<Cromossomo> populacao;
    private int somaAvaliacoes;
    

    @Override
    public void init(List<Cromossomo> populacao) {
	this.populacao = populacao;
	somaAvaliacoes = 0;
	
	for(Cromossomo c : populacao){
	    somaAvaliacoes += c.getAvaliacao();
	}
    }

    @Override
    public Cromossomo get() {
	double limite = Math.random() * Math.abs( this.somaAvaliacoes );
	double aux = 0;
	int i = 0;
	
	for( ; (i < populacao.size() && aux < limite); i++){
	    aux += populacao.get(i).getAvaliacao();
	}
	
	i--;
	return populacao.get(i);
    }

}
