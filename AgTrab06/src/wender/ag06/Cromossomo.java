package wender.ag06;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class Cromossomo {
    public static final int BOARD_SIZE = 64;
    private List<BoardSquare> moves;
    private int avalicao;

    public Cromossomo(List<BoardSquare> moves) {
	if (moves.size() != 64) {
	    throw new IllegalArgumentException("Quantidade de casas inválidas: " + moves.size());
	}

	this.moves = moves;

	for (int i = 1; i < moves.size(); i++) {
	    if (moves.get(i - 1).canMove(moves.get(i))) {
		avalicao++;
	    }
	}
    }

    public Integer getAvaliacao() {
	return avalicao;
    }

    public Cromossomo crossover(Cromossomo c2) {
	int p = sorteiaPonto(new ArrayList<Integer>());

	List<BoardSquare> news = new ArrayList<BoardSquare>(Arrays.asList(new BoardSquare[BOARD_SIZE]));

	for (int i = 0; i < this.moves.size(); i++) {
	    if (i <= p) {
		news.set(i, this.moves.get(i));
	    } else {
		news.set(i, pmx(news, this.moves, c2.moves, c2.moves.get(i)));
	    }
	}

	return new Cromossomo(news);
    }

    private int sorteiaPonto(List<Integer> jaFoi) {
	if (jaFoi.size() == 62) {
	    Collections.sort(jaFoi);
	    StringBuilder msg = new StringBuilder();
	    msg.append("Todos os pontos possíveis já foram sorteados:\n");

	    for (Iterator<Integer> ite = jaFoi.iterator(); ite.hasNext();) {
		msg.append(ite.next());

		if (ite.hasNext()) {
		    msg.append(", ");
		}
	    }

	    throw new IllegalArgumentException("Todos os pontos possíveis já foram sorteados!!");
	}

	int ponto = 0;

	while (ponto == 0) {
	    ponto = (int) (Math.random() * 62);
	}

	if (jaFoi.contains(ponto)) {
	    return sorteiaPonto(jaFoi);
	}

	while (this.moves.get(ponto).canMove(this.moves.get(ponto - 1)) && ponto < this.moves.size()) {
	    ponto++;
	}

	if (ponto == this.moves.size()) {
	    jaFoi.add(ponto);
	    return sorteiaPonto(jaFoi);
	}

	return ponto;
    }

    private static BoardSquare pmx(List<BoardSquare> novo, List<BoardSquare> p1, List<BoardSquare> p2, BoardSquare valor) {
	int idx = novo.indexOf(valor);

	if (idx == -1) {
	    return valor;
	} else {
	    return pmx(novo, p2, p1, p1.get(idx));
	}
    }

    public void mutacao() {
	
	int start = (int)  (Math.random() * 62);
	
	int p1 = searchLoose(start);

	if (p1 == -1) {
	    return;
	}
	
	int p2 = -1;
	
	for(int i = p1 +1;  i < moves.size(); i++){
	    
	    if(beforeIsLoose(i) && moves.get(p1).canMove(moves.get(i)) ){
		p2 = i;
		break;
	    }
	}
	
	if(p2 == -1){
	    return;
	}
	
	
	ArrayList<BoardSquare> newMoves = new ArrayList<BoardSquare>();
	
	
	for(int i = 0; i < moves.size(); i ++){
	    if(i != p1 && i != p2){
		newMoves.add(moves.get(i));
	    }
	    
	    if(i == p1){
		newMoves.add(moves.get(p1));
		newMoves.add(moves.get(p2));
	    }
	}
	
	if(newMoves.size() !=  64){
	    throw new IllegalArgumentException("Montou errado");
	}
	
	System.out.println("Mutou!");
	this.moves = newMoves;
    }

    private int searchLoose(int start) {

	for (int i = start; i < moves.size() -1; i++) {
	   
	    if(!moves.get(i).canMove(moves.get(i + 1)) ){
		return i;
	    }
	    
	}

	return -1;
    }
    
    private boolean beforeIsLoose(int index){
	if(index == 0){
	    return false;
	}
	
	return moves.get(index).canMove(moves.get(index -1));
    }

    private boolean isLooseIndex(int i) {
	if (i > 0 && moves.get(i).canMove(moves.get(i - 1))) {
	    return true;
	}

	if (i < (moves.size() - 1) && moves.get(i).canMove(moves.get(i + 1))) {
	    return true;
	}
	
	return false;
    }
}
