package wender.ag06;

import java.util.ArrayList;
import java.util.List;

public class BoardSquare {
    
    String[] colLeter = {"a","b", "c", "d", "e", "f", "g", "h"};
    
    private int row;
    private int col;
    private Integer number;
    
    public BoardSquare(int col, int row){
	this.row = row;
	this.col = col;
	
	number = (row * 8) + col;
    }
    
    public BoardSquare(int number){
	this.number = number;
	
	row = (int) number/8;
	col = number % 8;
    }
    
    public String toString(){
	return colLeter[col] + (row+1);
    }

    public int getRow() {
	return row;
    }

    public void setRow(int row) {
	this.row = row;
    }

    public int getCol() {
	return col;
    }

    public void setCol(int col) {
	this.col = col;
    }

    public int getNumber() {
	return number;
    }

    public void setNumber(int number) {
	this.number = number;
    }
    
    public boolean canMove(BoardSquare dest){
	int rows = Math.abs(this.row - dest.row);
	
	if(rows == 1 || rows == 2 ){
	    int cols = Math.abs(this.col - dest.col);
	    
	    if(rows == 1){
		return cols == 2;
	    }else{
		return cols == 1;
	    }
	}
	
	return false;
    }
    
    public static void main(String[] args) {
	teste();
    }
    
    public static void teste(){
	List<BoardSquare> squares = new ArrayList<>();
	
	squares.add(new BoardSquare(0, 0));
	squares.add(new BoardSquare(0, 7));
	squares.add(new BoardSquare(7, 0));
	squares.add(new BoardSquare(7, 7));
	squares.add(new BoardSquare(3, 3));
	squares.add(new BoardSquare(4, 3));
	squares.add(new BoardSquare(0, 3));
	
	BoardSquare dest = null;
	
	for(BoardSquare o : squares){
	    System.out.println("\nPossíveis destinos de " + o.toString());
	    
	    for(int i = 0; i < 64; i++){
		dest = new BoardSquare(i);
		
		if(o.canMove(dest)){
		    System.out.println(dest.toString());
		}
	    }
	}
    }
    
    @Override
    public boolean equals(Object obj) {
        
	if(obj instanceof BoardSquare){
	    return ((BoardSquare) obj).number == this.number;
	}
	
        return false;
    }
}
