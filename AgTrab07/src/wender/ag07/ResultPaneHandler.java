package wender.ag07;


public interface ResultPaneHandler {
    public Runnable getPopulacaoChangeRunnable();
    public void setCariotipo(Cariotipo cariotipo);
    public void clearResult();
}
