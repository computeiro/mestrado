package wender.ag07;

import wender.ag07.crossover.ICrossover;
import wender.ag07.mutacao.IMutacao;
import wender.ag07.populacao.IModuloPopulacional;
import wender.ag07.selecao.ISelecao;

public interface AgConf {
    public ISelecao getSelecao();
    public ICrossover getCrossover();
    public IMutacao getMutacao();
    public IModuloPopulacional getModuloPopulacional();
    public double getTaxaMutacao();
    public int getTamPopulacao();
    public int getQtdGeracoes();
}
