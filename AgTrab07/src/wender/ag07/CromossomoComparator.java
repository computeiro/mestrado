package wender.ag07;

import java.util.Comparator;

public class CromossomoComparator implements Comparator<Cromossomo> {

    @Override
    public int compare(Cromossomo o1, Cromossomo o2) {
	return o2.getAvaliacao().compareTo(o1.getAvaliacao());
    }

}
