package wender.ag07.crossover;

import wender.ag07.Cromossomo;

public interface ICrossover {
    public Cromossomo execute(Cromossomo c1, Cromossomo c2);
}
