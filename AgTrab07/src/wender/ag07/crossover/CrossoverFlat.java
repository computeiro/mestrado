package wender.ag07.crossover;

import wender.ag07.AgUtils;
import wender.ag07.Cromossomo;

public class CrossoverFlat implements ICrossover {

    @Override
    public Cromossomo execute(Cromossomo c1, Cromossomo c2) {
	double minX = Math.min(c1.getValue().getX(), c2.getValue().getX());
	double maxX = Math.max(c1.getValue().getX(), c2.getValue().getX());
	
	double minY = Math.min(c1.getValue().getY(), c2.getValue().getY());
	double maxY = Math.max(c1.getValue().getY(), c2.getValue().getY());
	
	return new Cromossomo(AgUtils.sorteia(minX, maxX), AgUtils.sorteia(minY, maxY));
    }
}
