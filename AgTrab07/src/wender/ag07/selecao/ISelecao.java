package wender.ag07.selecao;

import java.util.List;

import wender.ag07.Cromossomo;

public interface ISelecao {
    void init(List<Cromossomo> populacao);
    Cromossomo get();
}
