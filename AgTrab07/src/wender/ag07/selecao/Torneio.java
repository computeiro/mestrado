package wender.ag07.selecao;

import java.util.Collections;
import java.util.List;

import wender.ag07.Cromossomo;
import wender.ag07.CromossomoComparator;

public class Torneio implements ISelecao {
    private List<Cromossomo> populacao;
    private int maxRounds = 5;
    
    @Override
    public void init(List<Cromossomo> populacao) {
	this.populacao = populacao;
	Collections.sort(this.populacao, new CromossomoComparator());
    }

    @Override
    public Cromossomo get() {
	
	int round = 1;
	Cromossomo escolhido = sorteiaOponente();
	Cromossomo oponente;
	
	while(round < maxRounds){
	    oponente = sorteiaOponente();
	    
	    if(oponente.getAvaliacao() < escolhido.getAvaliacao()){
		escolhido = oponente;
	    }
	    
	    round ++;
	}
	

	return escolhido;
    }
    
    private Cromossomo sorteiaOponente(){
	return populacao.get((int) Math.random()); 
    }

}
