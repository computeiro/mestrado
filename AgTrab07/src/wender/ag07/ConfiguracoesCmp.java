package wender.ag07;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;

import wender.ag07.crossover.CrossoverAritimetico;
import wender.ag07.crossover.CrossoverFlat;
import wender.ag07.crossover.CrossoverLinear;
import wender.ag07.crossover.ICrossover;
import wender.ag07.mutacao.IMutacao;
import wender.ag07.mutacao.MutacaoAleatoria;
import wender.ag07.mutacao.MutacaoPersonalizada;
import wender.ag07.populacao.ElitismoPopulacional;
import wender.ag07.populacao.IModuloPopulacional;
import wender.ag07.populacao.SubstituiPopulacao;
import wender.ag07.selecao.ISelecao;
import wender.ag07.selecao.Rank;
import wender.ag07.selecao.Roleta;
import wender.ag07.selecao.Torneio;

public class ConfiguracoesCmp extends Composite {
    private ComboViewer cmbSelecao;
    private ComboViewer cmbCrossover;
    private ComboViewer cmbMutacao;
    private Spinner spTaxaMutacao;
    private Spinner spTamPopulacao;
    private Spinner spElitismo;
    private Spinner spQtdGeracoes;
    private Button btnExecutar;
    private Cariotipo cariotipo;

    ConfiguracoesCmp(Composite parent) {
	super(parent, SWT.NONE);
	setLayout(new GridLayout(3, false));

	Label lblSelecao = new Label(this, SWT.NONE);
	lblSelecao.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
	lblSelecao.setText("Seleção:");

	cmbSelecao = new ComboViewer(this, SWT.NONE);
	cmbSelecao.setContentProvider(ArrayContentProvider.getInstance());
	cmbSelecao.setLabelProvider(new LabelProvider() {
	    @Override
	    public String getText(Object element) {
		return ((OpcaoSelecao) element).label;
	    }
	});
	cmbSelecao.setInput(OpcaoSelecao.values());

	cmbSelecao.getCombo().setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
	cmbSelecao.getCombo().select(0);

	Label spamRef = new Label(this, SWT.None); // spam
	spamRef.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));

	Label lblCrossover = new Label(this, SWT.NONE);
	lblCrossover.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
	lblCrossover.setText("Crossover:");

	cmbCrossover = new ComboViewer(this, SWT.NONE);
	cmbCrossover.setContentProvider(ArrayContentProvider.getInstance());
	cmbCrossover.setLabelProvider(new LabelProvider() {
	    @Override
	    public String getText(Object element) {
		return ((OpcaoCrossover) element).label;
	    }
	});
	cmbCrossover.setInput(OpcaoCrossover.values());
	cmbCrossover.getCombo().setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
	cmbCrossover.getCombo().select(0);

	new Label(this, SWT.None); // spam

	Label lbMutacao = new Label(this, SWT.NONE);
	lbMutacao.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
	lbMutacao.setText("Mutação:");

	cmbMutacao = new ComboViewer(this, SWT.NONE);

	cmbMutacao.setContentProvider(ArrayContentProvider.getInstance());
	cmbMutacao.setLabelProvider(new LabelProvider() {
	    @Override
	    public String getText(Object element) {
		return ((OpcaoMutacao) element).label;
	    }
	});
	cmbMutacao.setInput(OpcaoMutacao.values());
	cmbMutacao.getCombo().setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
	cmbMutacao.getCombo().select(0);

	new Label(this, SWT.None); // spam

	Label lblTaxaDeMutao = new Label(this, SWT.NONE);
	lblTaxaDeMutao.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
	lblTaxaDeMutao.setText("Taxa de Mutação:");

	spTaxaMutacao = new Spinner(this, SWT.BORDER | SWT.RIGHT_TO_LEFT);
	spTaxaMutacao.setDigits(3);
	spTaxaMutacao.setIncrement(1);
	spTaxaMutacao.setMaximum(1000);
	spTaxaMutacao.setMinimum(0);
	spTaxaMutacao.setSelection(50);
	spTaxaMutacao.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));

	new Label(this, SWT.None); // spam

	Label lblTamanhoDaPopulao = new Label(this, SWT.NONE);
	lblTamanhoDaPopulao.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
	lblTamanhoDaPopulao.setText("Tamanho da População:");

	spTamPopulacao = new Spinner(this, SWT.BORDER | SWT.RIGHT_TO_LEFT);
	spTamPopulacao.setIncrement(1);
	spTamPopulacao.setMaximum(1000);
	spTamPopulacao.setMinimum(20);
	spTamPopulacao.setSelection(50);
	spTamPopulacao.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));

	new Label(this, SWT.None); // spam

	Label lblElitismo = new Label(this, SWT.NONE);
	lblElitismo.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
	lblElitismo.setText("Elitismo:");

	spElitismo = new Spinner(this, SWT.BORDER | SWT.RIGHT_TO_LEFT);
	spElitismo.setIncrement(1);
	spElitismo.setMaximum(10);
	spElitismo.setMinimum(0);
	spElitismo.setSelection(2);
	spElitismo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));

	new Label(this, SWT.None); // spam

	Label lblQuantidadeDeGeraes = new Label(this, SWT.NONE);
	lblQuantidadeDeGeraes.setText("Quantidade de Gerações:");
	lblQuantidadeDeGeraes.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));

	spQtdGeracoes = new Spinner(this, SWT.BORDER | SWT.RIGHT_TO_LEFT);
	spQtdGeracoes.setIncrement(1);
	spQtdGeracoes.setMaximum(99999);
	spQtdGeracoes.setMinimum(0);
	spQtdGeracoes.setSelection(100);
	spQtdGeracoes.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));

	new Label(this, SWT.None); // spam

	Label spamRef2 = new Label(this, SWT.None); // spam
	spamRef2.setLayoutData(new GridData(SWT.LEFT, SWT.LEFT, false, false));

	btnExecutar = new Button(this, SWT.None);
	btnExecutar.setLayoutData(new GridData(SWT.RIGHT, SWT.TOP, false, false, 1, 1));
	btnExecutar.setText("Executar!");

	btnExecutar.addListener(SWT.Selection, new Listener() {

	    @Override
	    public void handleEvent(Event event) {

		try {
		    validateForm();
		} catch (Exception e) {
		    MessageDialog.openWarning(getShell(), "Aviso", e.getMessage());
		}

		enableFormConf(false);

		try {
		    cariotipo.setAgConf(getConfiguracoes());
		    cariotipo.run();
		} catch (Exception e) {
		    e.printStackTrace();
		} finally {
		    enableFormConf(true);
		}

	    }

	});

	Label spamRef3 = new Label(this, SWT.None); // spam
	spamRef3.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
    }

    public void setButtonExecution(Cariotipo cariotipo) {
	this.cariotipo = cariotipo;
    }

    public enum OpcaoSelecao {
	Roleta(new Roleta(), "Roleta"), Rank(new Rank(), "Rank"), Torneio(new Torneio(), "Torneio");

	private String label;
	private ISelecao selecao;

	private OpcaoSelecao(ISelecao selecao, String label) {
	    this.selecao = selecao;
	    this.label = label;
	}
    }

    public enum OpcaoCrossover {
	Flat(new CrossoverFlat(), "Flat (Radcliffe, 1990)"), Linear(new CrossoverFlat(), "Linear (Wright, 1991)"), CrossoverFlat(new CrossoverAritimetico(), "Aritimético (Michalewicz, 1994)");

	private String label;
	private ICrossover crossover;

	private OpcaoCrossover(ICrossover crossover, String label) {
	    this.label = label;
	    this.crossover = crossover;
	}
    }

    public enum OpcaoMutacao {
	Aleatoria(new MutacaoAleatoria(), "Aletória"), Personalizada(new MutacaoPersonalizada(), "Personalizada");

	private IMutacao mutacao;
	private String label;

	private OpcaoMutacao(IMutacao mutacao, String label) {
	    this.mutacao = mutacao;
	    this.label = label;
	}
    }

    private void validateForm() {

    }

    private void enableFormConf(boolean enabled) {
	cmbSelecao.getCombo().setEnabled(enabled);
	cmbCrossover.getCombo().setEnabled(enabled);
	cmbMutacao.getCombo().setEnabled(enabled);
	spTaxaMutacao.setEnabled(enabled);
	spTamPopulacao.setEnabled(enabled);
	spElitismo.setEnabled(enabled);
	spQtdGeracoes.setEnabled(enabled);
	btnExecutar.setEnabled(enabled);
    }

    public AgConf getConfiguracoes() {
	Configuracoes agConf = new Configuracoes();

	OpcaoSelecao opcaoSelecao = (OpcaoSelecao) ((IStructuredSelection) cmbSelecao.getSelection()).getFirstElement();
	agConf.selecao = opcaoSelecao.selecao;

	OpcaoCrossover opcaoCrossover = (OpcaoCrossover) ((IStructuredSelection) cmbCrossover.getSelection()).getFirstElement();
	agConf.crossover = opcaoCrossover.crossover;

	OpcaoMutacao opcaoMutacao = (OpcaoMutacao) ((IStructuredSelection) cmbMutacao.getSelection()).getFirstElement();
	agConf.mutacao = opcaoMutacao.mutacao;

	int elitismo = Integer.parseInt(spElitismo.getText());

	if (elitismo > 0) {
	    agConf.moduloPopulacional = new ElitismoPopulacional(elitismo);
	} else {
	    agConf.moduloPopulacional = new SubstituiPopulacao();
	}

	agConf.taxaMutacao = Double.valueOf(spTaxaMutacao.getText().replace(",", "."));
	agConf.tamPopulacao = Integer.valueOf(spTamPopulacao.getText().replace(",", "."));
	agConf.qtdGeracoes = Integer.parseInt(spQtdGeracoes.getText().replace(",", "."));
	
	return agConf;
    }

    public class Configuracoes implements AgConf {
	private ISelecao selecao;
	private ICrossover crossover;
	private IMutacao mutacao;
	private IModuloPopulacional moduloPopulacional;
	private double taxaMutacao;
	private int tamPopulacao;
	private int qtdGeracoes;

	public ISelecao getSelecao() {
	    return selecao;
	}

	public ICrossover getCrossover() {
	    return crossover;
	}

	public IMutacao getMutacao() {
	    return mutacao;
	}

	public IModuloPopulacional getModuloPopulacional() {
	    return moduloPopulacional;
	}

	public double getTaxaMutacao() {
	    return taxaMutacao;
	}

	public int getTamPopulacao() {
	    return tamPopulacao;
	}

	public int getQtdGeracoes() {
	    return qtdGeracoes;
	}

    }

}
