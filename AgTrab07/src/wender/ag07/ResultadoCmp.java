package wender.ag07;

import java.awt.Color;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.experimental.chart.swt.ChartComposite;

public class ResultadoCmp extends Composite {
    private JFreeChart chartPopulacao;
    private JFreeChart chartIndividuo;
    private XYSeries serieInd;
    private XYSeries seriePopulacao;
    private final Object monitor = new Object();
    private Label lbMelhorResultado;

    public ResultadoCmp(Composite parent) {
	super(parent, SWT.None);
	setLayout(new GridLayout(1, true));
	
	lbMelhorResultado = new Label(this, SWT.None);
	lbMelhorResultado.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false));
	lbMelhorResultado.setText("Melhor Resultado: ");
	
	final XYSeriesCollection dataSetInd = new XYSeriesCollection();
	serieInd = new XYSeries("Avaliação individual");
	dataSetInd.addSeries(serieInd);

	chartIndividuo = createChartPanel("Melhor indivíduo", dataSetInd);
	ChartComposite cmpInd = new ChartComposite(this, SWT.None, chartIndividuo);
	cmpInd.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
	
	final XYSeriesCollection dataSetPop = new XYSeriesCollection();
	seriePopulacao = new XYSeries("Avaliação População");
	dataSetPop.addSeries(seriePopulacao);

	chartPopulacao = createChartPanel("Média da População", dataSetPop);
	ChartComposite cmpPop = new ChartComposite(this, SWT.None, chartPopulacao);
	cmpPop.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

    }
    
    public void updateCharts(final int geracao, final Cromossomo melhorIndividuo, final double mediaDaPopulacao){
	Display.getDefault().asyncExec(new Runnable() {
	    
	    @Override
	    public void run() {
		synchronized (monitor) {
		    serieInd.add(geracao, melhorIndividuo.getAvaliacao());
		    seriePopulacao.add(geracao, mediaDaPopulacao);
		    
		    StringBuilder msg = new StringBuilder();
		    msg.append("Melhor resultado: ")
		    .append(melhorIndividuo.getAvaliacao())
		    .append(" [ ").append(melhorIndividuo.getValue().getX())
		    .append(" ,").append(melhorIndividuo.getValue().getX()).append(" ]");
		    
		    lbMelhorResultado.setText(msg.toString());
		}
	    }
	});
    }

    public JFreeChart createChartPanel(String title, XYSeriesCollection dataset) {

	JFreeChart chart = ChartFactory.createXYLineChart(title,// title
		"geração", // x axis label
		"avaliação", // y axis label
		dataset, // data
		PlotOrientation.VERTICAL, false, // include legend
		true, // tooltips
		false // urls
		);

	chart.setBackgroundPaint(Color.white);

	final XYPlot plot = chart.getXYPlot();
	plot.setBackgroundPaint(Color.lightGray);
	plot.setDomainGridlinePaint(Color.white);
	plot.setRangeGridlinePaint(Color.white);
	plot.getRenderer().setSeriesPaint(0, new Color(0x00, 0x00, 0xFF));

	/*NumberAxis xAxis = new NumberAxis();
	xAxis.setTickUnit(new NumberTickUnit(50));
	plot.setDomainAxis(xAxis);*/
	
	return chart;
    }

    public void clear() {
	serieInd.clear();
	seriePopulacao.clear();
    }

}
