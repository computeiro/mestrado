package wender.ag07.mutacao;

import wender.ag07.AgUtils;
import wender.ag07.Cromossomo;

public class MutacaoPersonalizada implements IMutacao {

    @Override
    public void apply(Cromossomo c, double change) {
	double x = c.getValue().getX();
	double y = c.getValue().getY();
	
	double newX = x;
	double newY = y;
	
	if (change > Math.random()) {

	    if (x > 0) {
		newX = AgUtils.sorteia(x, 5.12);
	    } else {
		newX = AgUtils.sorteia(-5.11, x);
	    }
	}

	if (change > Math.random()) {

	    if (y > 0) {
		newY = AgUtils.sorteia(y, 5.12);
	    } else {
		newY = AgUtils.sorteia(-5.11, y);
	    }
	}
	
	if(x != newX || y != newY){
	    c.mutacao(x, y);
	}
    }

}
