package wender.ag07.mutacao;

import wender.ag07.Cromossomo;

public interface IMutacao {
    public void apply(Cromossomo c, double change);
}
