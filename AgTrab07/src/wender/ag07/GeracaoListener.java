package wender.ag07;

import java.util.List;

public interface GeracaoListener {
    public void update(List<Cromossomo> populacao, int geracao, int maxGeracoes);
}
