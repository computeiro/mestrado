package wender.ag07;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import wender.ag07.crossover.ICrossover;
import wender.ag07.mutacao.IMutacao;
import wender.ag07.populacao.IModuloPopulacional;
import wender.ag07.selecao.ISelecao;

public class Cariotipo implements Runnable {
    private List<Cromossomo> populacao;
    private int geracao;
    private AgConf agConf;
    private GeracaoListener geracaoListener;

    public Cariotipo(GeracaoListener geracaoListener) {
	this.geracaoListener = geracaoListener;
    }

    public void setAgConf(AgConf agConf) {
	this.agConf = agConf;
    }

    @Override
    public void run() {
	inicializaPopulacao();
	this.geracao = 1;

	ISelecao selecao = agConf.getSelecao();
	selecao.init(populacao);

	ICrossover crossover = agConf.getCrossover();
	IMutacao mutacao = agConf.getMutacao();
	IModuloPopulacional moduloPopulacional = agConf.getModuloPopulacional();

	while (geracao < agConf.getQtdGeracoes()) {
	    List<Cromossomo> novaPopulacao = new ArrayList<>();

	    for (int i = 0; i < agConf.getTamPopulacao(); i++) {
		Cromossomo c1 = selecao.get();
		Cromossomo c2 = selecao.get();

		Cromossomo f = crossover.execute(c1, c2);
		mutacao.apply(f, agConf.getTaxaMutacao());

		novaPopulacao.add(f);
	    }

	    populacao = moduloPopulacional.atualizaPopulacao(populacao, novaPopulacao);

	    if (geracaoListener != null) {
		
		synchronized (geracaoListener) {

		    List<Cromossomo> individuos = new ArrayList<>();
		    individuos.addAll(novaPopulacao);
		    individuos = Collections.unmodifiableList(individuos);
		    geracaoListener.update(individuos, geracao, agConf.getQtdGeracoes());
		}
	    }

	    geracao++;
	}

    }

    private void inicializaPopulacao() {
	populacao = new ArrayList<>();

	for (int i = 0; i < agConf.getTamPopulacao(); i++) {
	    populacao.add(new Cromossomo(AgUtils.sorteia(), AgUtils.sorteia()));
	}

    }

}
