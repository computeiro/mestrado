package wender.ag07.populacao;

import java.util.List;

import wender.ag07.Cromossomo;

public interface IModuloPopulacional {
    List<Cromossomo> atualizaPopulacao(List<Cromossomo> antiga, List<Cromossomo> nova);
}
