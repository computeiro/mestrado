package wender.ag07;

public class AgUtils {
    
    /**
     * 
     * @return número randomico entre min e max
     */
    public static double sorteia(double min, double max) {
	double teto = Math.abs(max);
	
	if(max > 0){
	    teto += 5.11;
	}
	
	double piso = Math.abs(min);
	
	if(min > 0){
	    piso += 5.11;
	}
	
	double valor = piso + Math.random() * (teto - piso);

	return valor - 5.11;
    }
    
    /**
     * 
     * @return número randomico entre -5.11 e +5.12
     */
    public static double sorteia() {
	double valor = Math.random() * 10.23;
	return valor - 5.11;
    }
}
