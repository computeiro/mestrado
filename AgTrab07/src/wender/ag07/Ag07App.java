package wender.ag07;

import java.awt.EventQueue;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.StatusLineManager;
import org.eclipse.jface.action.ToolBarManager;
import org.eclipse.jface.window.ApplicationWindow;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.jzy3d.analysis.SWTAnalysisLauncher;
import org.jzy3d.chart.Settings;

import wender.ag07.AgChart.XYZ;

public class Ag07App extends ApplicationWindow {
    // 80.70309402628959 -> -4.520000000000013, -4.520000000000013
    private ResultadoCmp resultadoCmp;
    private AgChart agChart;
    private ConfiguracoesCmp confCmp;
    private Cariotipo cariotipo;

    /**
     * Create the application window.
     */
    public Ag07App() {
	super(null);
	createActions();
	addToolBar(SWT.FLAT | SWT.WRAP);
	addMenuBar();
	addStatusLine();
    }

    /**
     * Create contents of the application window.
     * 
     * @param parent
     */
    @Override
    protected Control createContents(Composite parent) {
	CTabFolder tab = new CTabFolder(parent, SWT.BORDER);
	agChart = new AgChart();

	Settings.getInstance().setHardwareAccelerated(true);
	try {
	    SWTAnalysisLauncher.open(agChart);
	} catch (Exception e) {
	    e.printStackTrace();
	}

	CTabItem abaConf = new CTabItem(tab, SWT.BORDER);
	abaConf.setText("Configurações");

	confCmp = new ConfiguracoesCmp(tab);
	abaConf.setControl(confCmp);

	cariotipo = new Cariotipo(new GeracaoListenerImpl());

	CTabItem abaResultado = new CTabItem(tab, SWT.BORDER);
	abaResultado.setText("Desempenho");
	resultadoCmp = new ResultadoCmp(tab);
	abaResultado.setControl(resultadoCmp);

	confCmp.setButtonExecution(cariotipo);

	return tab;
    }

    private class GeracaoListenerImpl implements GeracaoListener {

	@Override
	public void update(final List<Cromossomo> populacao,final int geracao, final int maxGeracoes) {
	    
	    synchronized (cariotipo) {
		Display.getDefault().asyncExec(new Runnable() {
		    
		    @Override
		    public void run() {
			final List<XYZ> points = new ArrayList<>();
			Cromossomo melhorIndividuo = populacao.get(0);
			double somaAvaliacoes = populacao.get(0).getAvaliacao();

			for (int i = 1; i < populacao.size(); i++) {

			    if (melhorIndividuo.getAvaliacao() < populacao.get(i).getAvaliacao()) {
				melhorIndividuo = populacao.get(i);
			    }

			    somaAvaliacoes += populacao.get(i).getAvaliacao();

			    points.add(populacao.get(i).getValue());
			}

			resultadoCmp.updateCharts(geracao, melhorIndividuo, somaAvaliacoes / populacao.size());
			
			try{
    			EventQueue.invokeAndWait(new Runnable() {
    			    
    			    @Override
    			    public void run() {
    				agChart.setPoints(points);
    				
    			    }
    			});
			}catch(Exception e){
			    e.printStackTrace();
			}
		    }
		});
		
	    }

	}

    }

    /**
     * Create the actions.
     */
    private void createActions() {
	// Create the actions
    }

    /**
     * Create the menu manager.
     * 
     * @return the menu manager
     */
    @Override
    protected MenuManager createMenuManager() {
	MenuManager menuManager = new MenuManager("menu");
	return menuManager;
    }

    /**
     * Create the toolbar manager.
     * 
     * @return the toolbar manager
     */
    @Override
    protected ToolBarManager createToolBarManager(int style) {
	ToolBarManager toolBarManager = new ToolBarManager(style);
	return toolBarManager;
    }

    /**
     * Create the status line manager.
     * 
     * @return the status line manager
     */
    @Override
    protected StatusLineManager createStatusLineManager() {
	StatusLineManager statusLineManager = new StatusLineManager();
	return statusLineManager;
    }

    /**
     * Launch the application.
     * 
     * @param args
     */
    public static void main(String args[]) {
	try {
	    Ag07App window = new Ag07App();
	    window.setBlockOnOpen(true);
	    window.open();
	    Display.getCurrent().dispose();
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }

    /**
     * Configure the shell.
     * 
     * @param newShell
     */
    @Override
    protected void configureShell(Shell newShell) {
	super.configureShell(newShell);
	newShell.setMaximized(true);
	newShell.setMinimumSize(800, 600);
	newShell.setText("Minimização de função utilizando Algoritmos Genéticos");
    }

    /**
     * Return the initial size of the window.
     */
    @Override
    protected Point getInitialSize() {
	return new Point(450, 300);
    }

}
