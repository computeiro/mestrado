package wender.ag07;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.jzy3d.analysis.AbstractAnalysis;
import org.jzy3d.chart.factories.AWTChartComponentFactory;
import org.jzy3d.colors.Color;
import org.jzy3d.colors.ColorMapper;
import org.jzy3d.colors.colormaps.ColorMapRainbow;
import org.jzy3d.maths.Coord3d;
import org.jzy3d.maths.Range;
import org.jzy3d.plot3d.builder.Builder;
import org.jzy3d.plot3d.builder.Mapper;
import org.jzy3d.plot3d.builder.concrete.OrthonormalGrid;
import org.jzy3d.plot3d.primitives.Point;
import org.jzy3d.plot3d.primitives.Shape;
import org.jzy3d.plot3d.rendering.canvas.Quality;

public class AgChart extends AbstractAnalysis {
    private Shape surface;
    private final List<Point> points = new ArrayList<>();;
    private static final Mapper mapper;

    static {
	mapper = new Mapper() {
	    public double f(double x, double y) {
		return 20 + (x * x) + (y * y) - 10 * (Math.cos(2 * Math.PI * x) + Math.cos(2 * Math.PI * y));
	    }
	};
    }

    public static double f(double x, double y) {
	return mapper.f(x, y);
    }

    @Override
    public void init() {
	// Define a function to plot

	// Define range and precision for the function to plot
	Range range = new Range(-5.11, 5.12);
	int steps = 100;

	// Create the object to represent the function over the given range.
	surface = Builder.buildOrthonormal(new OrthonormalGrid(range, steps, range, steps), mapper);
	surface.setColorMapper(new ColorMapper(new ColorMapRainbow(), surface.getBounds().getZmin(), surface.getBounds().getZmax(), new Color(1, 1, 1, .5f)));
	surface.setFaceDisplayed(true);
	surface.setWireframeDisplayed(false);

	// Create a chart
	chart = AWTChartComponentFactory.chart(Quality.Advanced, getCanvasType());
	chart.getScene().getGraph().add(surface);

    }

    public void setPoints(final List<XYZ> newPoints) {
	synchronized (surface) {

	    for (Iterator<Point> ite = points.iterator(); ite.hasNext();) {
		surface.remove(ite.next());
	    }

	    points.clear();

	    for (Iterator<XYZ> ite = newPoints.iterator(); ite.hasNext();) {
		XYZ p = ite.next();
		Point point = new Point(new Coord3d(p.x, p.y, p.z), Color.BLACK, 5);
		points.add(point);
	    }

	    surface.add(points);
	}
    }

    public static class XYZ {
	private double x;
	private double y;
	private double z;

	public XYZ(double x, double y, double z) {
	    this.x = x;
	    this.y = y;
	    this.z = z;
	}

	public double getX() {
	    return x;
	}

	public double getY() {
	    return y;
	}

	public double getZ() {
	    return z;
	}

    }
}
