package wender.ag07;

import wender.ag07.AgChart.XYZ;

public class Cromossomo {
    private XYZ xyz;
    
    public Cromossomo(double x, double y){
	xyz = new XYZ(x, y, AgChart.f(x, y));
    }
    
    public Double getAvaliacao() {
	return xyz.getZ();
    }
    
    public XYZ getValue(){
	return xyz;
    }
    
    public void mutacao(double newX, double newY){
	xyz = new XYZ(newX, newY, AgChart.f(newX, newY));
    }
}
