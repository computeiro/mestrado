package jcubik;

import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

public class Cell extends JPanel {
    private static final long serialVersionUID = 1L;

    Cell(int color) {
	setBorder(BorderFactory.createLineBorder(Color.BLACK));
	setColor(color);
    }

    public void setColor(int color) {
	switch (color) {
	
        	case Cube.Blue:
        	    setBackground(Color.BLUE);
        	    break;
        
        	case Cube.Orange:
        	    setBackground(Color.ORANGE);
        	    break;
        
        	case Cube.Green:
        	    setBackground(Color.GREEN);
        	    break;
        
        	case Cube.Red:
        	    setBackground(Color.RED);
        	    break;
        
        	case Cube.Yellow:
        	    setBackground(Color.YELLOW);
        	    break;
        
        	default:
        	    setBackground(Color.WHITE);

	}
    }
}
