package jcubik;

import java.util.Stack;

public class Cube {
    public static final int White = 0;
    public static final int Blue = 1;
    public static final int Orange = 2;
    public static final int Green = 3;
    public static final int Red = 4;
    public static final int Yellow = 5;
    
    public static final int DOWN = 1;
    public static final int FRONT = 2;
    public static final int lEFT = 3;
    public static final int BACK = 4;
    public static final int RIGHT = 5;
    public static final int UPPER = 6;
    public static final int VERTICAL = 7;
    public static final int HORIZONTAL = 8;
    public static final int MIDDLE = 8;
    
    public static final int FL = 0;
    public static final int Dl = 1;
    
    private int[][] faces;
    private Stack<Integer> stack;
    private MotionListener listener;
    
    

    public static void main(String[] args) {
    }
    
    
    public Cube(){
	stack = new Stack<Integer>();
	faces = new int[6][];
	
	for(int i = 0; i < 6; i++){
	    faces[i] = new int[9];
	    
	    for(int j = 0; j < 9; j++){
		faces[i][j] = i;
	    }
	    
	}
    }
    
    public int[][] getFaces(){
	return faces;
    }
    
    public Cube down(){
	if(listener != null){
	    listener.beforeMotion();
	}
	int[] aux = faces[Blue];
	
	faces[Blue][6] = faces[Red][6]; 
	faces[Blue][7] = faces[Red][7]; 
	faces[Blue][8] = faces[Red][8]; 

	faces[Red][6] = faces[Green][6]; 
	faces[Red][7] = faces[Green][7]; 
	faces[Red][8] = faces[Green][8];
	
	faces[Green][6] = faces[Orange][6]; 
	faces[Green][7] = faces[Orange][7]; 
	faces[Green][8] = faces[Orange][8]; 

	faces[Orange][6] = aux[6]; 
	faces[Orange][7] = aux[7]; 
	faces[Orange][8] = aux[8]; 
	
	if(listener != null){
	    listener.afterMotion();
	}
	
	return this;
    }
    
    public void addListener(MotionListener listener){
	this.listener = listener;
    }
    
    public interface MotionListener{
	public void beforeMotion();
	public void afterMotion();
    }
    
}
