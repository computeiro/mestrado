package jcubik;

import java.awt.EventQueue;
import java.awt.GridLayout;
import java.awt.Label;

import javax.swing.JFrame;
import javax.swing.JPanel;

import jcubik.Cube.MotionListener;

public class JCubikApp {

    private JFrame frame;
    private Face[] faces;
    private Cube cube;

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
	EventQueue.invokeLater(new Runnable() {
	    public void run() {
		try {
		    JCubikApp window = new JCubikApp();
		    window.frame.setVisible(true);
		} catch (Exception e) {
		    e.printStackTrace();
		}
	    }
	});
    }

    /**
     * Create the application.
     */
    public JCubikApp() {
	initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
	faces = new Face[6];
	
	for(int i = 0; i < 6; i ++){
	    faces[i] = new Face(i);
	}
	
	frame = new JFrame();
	frame.setBounds(100, 0, 570, 600);
	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	
	JPanel content = new JPanel();
	content.setLayout(new GridLayout(4, 3, 0, 0));
	
	content.add(new Label());
	content.add(faces[Cube.Yellow]);
	content.add(new Label());
	
	content.add(faces[Cube.Orange]);
	content.add(faces[Cube.Blue]);
	content.add(faces[Cube.Red]);
	
	content.add(new Label());
	content.add(faces[Cube.White]);
	content.add(new Label());
	
	content.add(new Label());
	content.add(faces[Cube.Green]);
	content.add(new Label());
	
	frame.getContentPane().add(content);
	setCube(new Cube());
	cube.addListener(new CubeListener(this));
	
	frame.setVisible(true);
    }
    
    public void setCube(Cube cube){
	this.cube = cube;
	
	int[][] facesValues = cube.getFaces();
	
	for(int i = 0; i < 6; i++){
	    faces[i].setColors(cube.getFaces()[i]);
	}
    }
    
    private class CubeListener implements MotionListener{
	JCubikApp app;
	
	public CubeListener(JCubikApp app) {
	    this.app = app;
	}

	@Override
	public void beforeMotion() {
	    // TODO Auto-generated method stub
	    
	}

	@Override
	public void afterMotion() {
	    app.setCube(app.cube);
	}
	
    }
    
    

}
