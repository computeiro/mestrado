package jcubik;

import java.awt.GridLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class Face extends JPanel {
    private static final long serialVersionUID = 1L;
    
    private Cell[] cells;

    public static void main(String[] args) {
	JFrame jf = new JFrame();
	jf.setBounds(100, 100, 210, 210);
	jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	
	jf.getContentPane().add(new Face());
	jf.setVisible(true);
    }
    
    public Face(){
	this(Cube.White);
    }
    
    public Face(int color){
	cells = new Cell[9];
    	setLayout(new GridLayout(3, 3, 0, 0));
    	
    	for(int i = 0; i < 9; i++){
    	    cells[i] = new Cell(color);
    	    add(cells[i]);
    	}
    }
    
    public void setColors(int[] colors){
	
	for(int i = 0; i < colors.length; i++){
	    cells[i].setColor(colors[i]);
	}
	
    }
    
    public void teste(){
	Face f = new Face();
	f.setColors(new int[]{1,2});
    }
    
    
    
    
    

}
