package swt.wender.rna.trab03;

import geogebra.GeoGebraPanel;
import geogebra.common.plugin.GgbAPI;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JFrame;

import swt.wender.rna.trab03.swing.NeuronioApp;

public class HebbValues {
    public static final String[] colNames = { "x1", "x2", "\u0394w1", "\u0394w1", "\u0394b", "w1", "w2", "b", "y" };
    public static final int x1 = 0;
    public static final int x2 = 1;
    public static final int delta0 = 2;
    public static final int delta1 = 3;
    public static final int delta2 = 4;
    public static final int bias = 5;
    public static final int w1 = 6;
    public static final int w2 = 7;
    public static final int y = 8;
    public static final int QTD_PORTAS = 16;
    public static final int QTD_LINHAS = 4;

    private int[][][] tabelas;
    private Map<Integer, String> mapaNomes;
    private JFrame fGeoGebra;
    private GeoGebraPanel ggbPanel;

    public static void main(String[] args) {
	HebbValues hv = new HebbValues();
	hv.printValues();
    }

    public int[][][] getTabelas() {
	return tabelas;
    }

    public String getName(int index) {
	if (mapaNomes.containsKey(index)) {
	    return mapaNomes.get(index);
	}

	return "";
    }

    public HebbValues() {
	tabelas = new int[QTD_PORTAS][][];
	mapaNomes = new HashMap<>();
	mapaNomes.put(1, "NOR");// y= 1 0 0 0
	mapaNomes.put(6, "XOR");// y= 0 1 1 0
	mapaNomes.put(7, "NAND");// y= 1 1 1 0
	mapaNomes.put(8, "AND");// y= 0 0 0 1
	mapaNomes.put(14, "OR");// y= 0 1 1 1

	// Preenchendo o target
	for (int p = 0; p < QTD_PORTAS; p++) {
	    // @formatter:off
	    
	   tabelas[p] =  new int[][]{ 
	   //       x1, x2, d0, d1, d2, w0, w1, w2, y
		   {-1, -1,  0,  0,  0,  0,  0,  0, 0},
		   { 1, -1,  0,  0,  0,  0,  0,  0, 0},
		   {-1,  1,  0,  0,  0,  0,  0,  0, 0},
		   { 1,  1,  0,  0,  0,  0,  0,  0, 0}
	    };
	   // @formatter:on

	    for (short linha = 0; linha < QTD_LINHAS; linha++) {
		tabelas[p][linha][y] = ((p >> linha & 1) == 0 ? -1 : 1);

		tabelas[p][linha][delta0] = tabelas[p][linha][y];
		tabelas[p][linha][delta1] = tabelas[p][linha][x1] * tabelas[p][linha][y];
		tabelas[p][linha][delta2] = tabelas[p][linha][x2] * tabelas[p][linha][y];

		if (linha == 0) {
		    tabelas[p][linha][bias] = tabelas[p][linha][delta0];
		    tabelas[p][linha][w1] = tabelas[p][linha][delta1];
		    tabelas[p][linha][w2] = tabelas[p][linha][delta2];
		} else {
		    tabelas[p][linha][bias] = tabelas[p][linha - 1][bias] + tabelas[p][linha][delta0];
		    tabelas[p][linha][w1] = tabelas[p][linha - 1][w1] + tabelas[p][linha][delta1];
		    tabelas[p][linha][w2] = tabelas[p][linha - 1][w2] + tabelas[p][linha][delta2];
		}
	    }

	}

	initGeoGebra();
    }
    
    public void initGeoGebra(){
	// create frame (window)
	fGeoGebra = new JFrame();
	fGeoGebra.setLayout(new BorderLayout());
	fGeoGebra.setTitle("Gráfico da tabela verdades");

	// create GeoGebraPanel
	ggbPanel = new GeoGebraPanel();
	ggbPanel.buildGUI(); // needs to be called
	fGeoGebra.add(ggbPanel, BorderLayout.CENTER);

	// more frame related stuff
	fGeoGebra.setPreferredSize(new Dimension(800, 600));
	fGeoGebra.pack();
	fGeoGebra.setLocationRelativeTo(null);
    }

    public void printValues() {
	StringBuilder out = new StringBuilder();

	for (int p = 0; p < tabelas.length; p++) {
	    if (mapaNomes.containsKey(p)) {
		out.append(mapaNomes.get(p));
	    } else {
		out.append("Desconhecida");
	    }

	    for (int l = 0; l < tabelas[p].length; l++) {
		out.append("\n[ ");
		for (int c = 0; c < tabelas[p][l].length; c++) {
		    if (tabelas[p][l][c] > -1) {
			out.append(" ");
		    }

		    out.append(" ").append(tabelas[p][l][c]);

		    if (c + 1 < tabelas[p][l].length) {
			out.append(", ");
		    }
		}

		out.append(" ]");
	    }

	    out.append("\n\n");
	}

	System.out.println(out.toString());
    }

    public void showNeuron(final int index) {
	EventQueue.invokeLater(new Runnable() {
	    public void run() {
		try {
		    NeuronioApp neuronApp = new NeuronioApp();
		    int[] lastLine = tabelas[index][3];

		    neuronApp.setBias(lastLine[bias]);

		    neuronApp.setW1(lastLine[w1]);
		    neuronApp.setW2(lastLine[w2]);

		    neuronApp.setR1((index >> 0 & 1) == 0 ? -1 : 1);
		    neuronApp.setR2((index >> 1 & 1) == 0 ? -1 : 1);
		    neuronApp.setR3((index >> 2 & 1) == 0 ? -1 : 1);
		    neuronApp.setR4((index >> 3 & 1) == 0 ? -1 : 1);

		    neuronApp.setVisible();
		} catch (Exception e) {
		    e.printStackTrace();
		}
	    }
	});
    }

    public void showAxis(final int index) {
	EventQueue.invokeLater(new Runnable() {
	    public void run() {
		try {

		    fGeoGebra.setVisible(true);

		    // now do something with the API (evaluate a command)
		    GgbAPI api = ggbPanel.getGeoGebraAPI();

		    String[] pontos = { "A", "B", "C", "D" };

		    StringBuilder cmd = new StringBuilder();

		    for (int linha = 0; linha < 4; linha++) {
			cmd.setLength(0);
			cmd.append(pontos[linha]).append(" = ");
			cmd.append("(");
			cmd.append(tabelas[index][linha][x1]);
			cmd.append(",");
			cmd.append(tabelas[index][linha][x2]);
			cmd.append(")");


			api.evalCommand(cmd.toString());

			cmd.setLength(0);

			cmd.append("SetColor[").append(pontos[linha]).append(", ");

			if ((index >> linha & 1) == 1) {
			    cmd.append("\"Dark Green\"");
			} else {
			    cmd.append("\"Red\"");
			}

			cmd.append("]");

			api.evalCommand(cmd.toString());

		    }

		} catch (Exception e) {
		    e.printStackTrace();
		}
	    }
	});
    }
}
