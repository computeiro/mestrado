package swt.wender.rna.trab03;

public class Teste {
    public static void main(String[] args) {
	StringBuilder out = new StringBuilder();
	
	for(short s = 0; s < 16; s++){
	    out.append("\n").append(s).append(": ");
	    
	    for(int b = 0; b < 4; b++){
		out.append(" ").append( (s >> b & 1) == 0 ? -1 :  1);
	    }
	}
	
	System.out.println(out.toString());
    }
}
