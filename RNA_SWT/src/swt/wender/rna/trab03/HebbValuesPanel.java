package swt.wender.rna.trab03;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowData;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.Text;

import swt.wender.rna.resources.ImgMgr;

public class HebbValuesPanel extends Composite {
	private Label		lbTitle;
	private TableViewer	tvValues;
	private HebbValues	values;
	private int currentIndex;
	
	private Button btnFirst;
	private Button btnPrev;
	private Button btnNext;
	private Button btnLast;
	private Button btnViewNeuron;
	private Button btnViewAxis;
	private Text txCurrRegister;

	HebbValuesPanel(Composite parent) {
		super(parent, SWT.None);
		this.setLayout(new GridLayout(1, true));
		values = new HebbValues();

		lbTitle = new Label(this, SWT.None);
		FontData[] fD = lbTitle.getFont().getFontData();
		fD[0].setHeight(16);
		lbTitle.setFont(new Font(lbTitle.getDisplay(), fD[0]));

		lbTitle.setLayoutData(new GridData(SWT.FILL, SWT.TOP, false, false));

		tvValues = new TableViewer(this);
		tvValues.setLabelProvider(new LabelProviderImpl());
		tvValues.setContentProvider(ArrayContentProvider.getInstance());

		Table table = tvValues.getTable();
		table.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		table.setHeaderVisible(true);
		table.setLinesVisible(true);
		
		for (int c = 0; c < HebbValues.colNames.length; c++) {
			TableViewerColumn col = createTableViewerColumn(HebbValues.colNames[c]);
			col.getColumn().setAlignment(SWT.RIGHT);
			final int columnNumber = c;
			col.setLabelProvider(new ColumnLabelProvider() {
				@Override
				public String getText(Object element) {
					int[] linha = (int[]) element;
					return String.valueOf(linha[columnNumber]);
				}
			});
		}
		
		Composite buttomBar = new Composite(this, SWT.None);
		buttomBar.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, false));
		buttomBar.setLayout(new RowLayout(SWT.HORIZONTAL));
		
		btnFirst = new Button(buttomBar, SWT.None);
		btnFirst.setImage(ImgMgr.ICO_FIRST.getImage());
		btnFirst.addListener(SWT.Selection, new Listener() {
			
			@Override
			public void handleEvent(Event arg0) {
				if(currentIndex != 0){
					currentIndex = 0;
					updateTable();
				}
				
			}
		});
		
		btnPrev = new Button(buttomBar, SWT.None);
		btnPrev.setImage(ImgMgr.ICO_PREV.getImage());
		btnPrev.addListener(SWT.Selection, new Listener() {
			
			@Override
			public void handleEvent(Event arg0) {
				if(currentIndex > 0){
					currentIndex--;
					updateTable();
				}
				
			}
		});
		
		txCurrRegister = new Text(buttomBar, SWT.READ_ONLY | SWT.None | SWT.CENTER);
		RowData rowData = new RowData();
		rowData.width = 30;
		txCurrRegister.setLayoutData(rowData);
		
		btnNext = new Button(buttomBar, SWT.None);
		btnNext.setImage(ImgMgr.ICO_NEXT.getImage());
		btnNext.addListener(SWT.Selection, new Listener() {
			
			@Override
			public void handleEvent(Event arg0) {
				if(currentIndex < HebbValues.QTD_PORTAS ){
					currentIndex++;
					updateTable();
				}
				
			}
		});
		
		btnLast = new Button(buttomBar, SWT.None);
		btnLast.setImage(ImgMgr.ICO_LAST.getImage());
		btnLast.addListener(SWT.Selection, new Listener() {
			
			@Override
			public void handleEvent(Event arg0) {
				if(currentIndex < HebbValues.QTD_PORTAS){
					currentIndex = HebbValues.QTD_PORTAS -1;
					updateTable();
				}
				
			}
		});
		
		Label lbSpacer = new Label(buttomBar, SWT.None);
		RowData spRowData = new RowData();
		spRowData.width = 10;
		lbSpacer.setLayoutData(spRowData);
		
		btnViewNeuron = new Button(buttomBar, SWT.None);
		btnViewNeuron.setImage(ImgMgr.ICO_EYE.getImage());
		btnViewNeuron.addListener(SWT.Selection, new Listener() {
		    
		    @Override
		    public void handleEvent(Event event) {
			values.showNeuron(currentIndex);
		    }
		});
		
		
		btnViewAxis = new Button(buttomBar, SWT.None);
		btnViewAxis.setImage(ImgMgr.ICO_AXIS.getImage());
		btnViewAxis.addListener(SWT.Selection, new Listener() {
		    
		    @Override
		    public void handleEvent(Event event) {
			values.showAxis(currentIndex);
		    }
		});
		

		updateTable();
	}

	private TableViewerColumn createTableViewerColumn(String title) {
		final TableViewerColumn viewerColumn = new TableViewerColumn(tvValues, SWT.RIGHT);
		final TableColumn column = viewerColumn.getColumn();
		column.setText(title);
		column.setWidth(40);
		column.setResizable(true);
		column.setMoveable(true);
		return viewerColumn;
	}

	public void updateTable() {
		tvValues.setInput(values.getTabelas()[currentIndex]);
		tvValues.refresh();
		txCurrRegister.setText(String.valueOf(currentIndex + 1)); 
		
		lbTitle.setText("Porta Lógica " + values.getName(currentIndex));
	}

	public class LabelProviderImpl implements ITableLabelProvider {

		@Override
		public void addListener(ILabelProviderListener listener) {
			// TODO Auto-generated method stub

		}

		@Override
		public void dispose() {
			// TODO Auto-generated method stub

		}

		@Override
		public boolean isLabelProperty(Object element, String property) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public void removeListener(ILabelProviderListener listener) {
			// TODO Auto-generated method stub

		}

		@Override
		public Image getColumnImage(Object element, int columnIndex) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public String getColumnText(Object element, int columnIndex) {
			return String.valueOf(((int[]) element)[columnIndex]);
		}
	}
}
