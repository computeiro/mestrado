package swt.wender.rna.resources;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Image;

public enum ImgMgr {
    
//@formatter:off
	ICO_FIRST("first.png"), 
	ICO_PREV("prev.png"), 
	ICO_NEXT("next.png"), 
	ICO_LAST("last.png"),
	ICO_EYE("eye.png"),
	ICO_AXIS("axis.png");
//@formatter:on

    private String imgFileName;

    private ImgMgr(String imgFileName) {
	this.imgFileName = imgFileName;
    }

    public Image getImage() {
	return ImageDescriptor.createFromURL(ImgMgr.class.getResource(imgFileName)).createImage();
    }
}
