package wender.ag5.selecao;

import java.util.List;

import wender.ag5.Cromossomo;

public interface ISelecao {
    void init(List<Cromossomo> populacao);
    Cromossomo get();
}
