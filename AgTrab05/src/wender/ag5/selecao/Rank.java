package wender.ag5.selecao;

import java.util.List;

import wender.ag5.Cromossomo;

public class Rank implements ISelecao {
    private List<Cromossomo> populacao;
    
    @Override
    public void init(List<Cromossomo> populacao) {
	this.populacao = populacao;
    }

    @Override
    public Cromossomo get() {
	int index = (int) Math.random() * (populacao.size() -1);
	
	return populacao.get(index);
    }

}
