package wender.ag5.mutacao;

import wender.ag5.Cromossomo;

public class SwapMutation implements IMutacao {

    @Override
    public void apply(Cromossomo c, double change) {
	
	
	for(int i = 0; i < c.getOrdemPtos().size(); i++){

	    if( Math.random() > change){
		continue;
	    }
	    
	    int p1 = (int) ( Math.random() * (c.getOrdemPtos().size() -1));
	    int p2;
	    
	    do{
		p2 =  (int) (Math.random() * (c.getOrdemPtos().size() -1));
	    }while(p2 == p1);
	    
	    int aux = c.getOrdemPtos().get(p1);
	    c.getOrdemPtos().set(p1, c.getOrdemPtos().get(p2));
	    c.getOrdemPtos().set(p2, aux);
	    
	}
    }

}
