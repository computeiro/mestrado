package wender.ag5.mutacao;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import wender.ag5.Cariotipo;
import wender.ag5.Cromossomo;

public class PositionBasedMutation implements IMutacao {

    @Override
    public void apply(Cromossomo c, double change) {
	for(int i = 0; i < c.getOrdemPtos().size(); i++){
	   
	    if(Math.random() > change){
		continue;
	    }
	    
	    int from = (int) ( Math.random() * (c.getOrdemPtos().size() -1));
	    int to;
	    
	    do{
		to =  (int) (Math.random() * (c.getOrdemPtos().size() -1));
	    }while(to == from);
	    
	    int fromValue = c.getOrdemPtos().get(from);
	    c.getOrdemPtos().remove(from);
	    c.getOrdemPtos().add(to, fromValue);
	    
	}
    }
    
    public static void main(String[] args) {
    }

}
