package wender.ag5.mutacao;

import wender.ag5.Cromossomo;

public interface IMutacao {
    public void apply(Cromossomo c, double change);
}
