package wender.ag5;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.JComponent;

public class PointsPanel extends JComponent implements MouseListener {
    private ArrayList<Point2D> points;
    private final int DIAMETRO_PONTO = 10;
    private final int DISTANCIA_MINIMA = DIAMETRO_PONTO * 2 + 5;
    private static final Object PAINT_MONITOR = new Object();
    private boolean cliqueBloqueado;
    private List<Integer> melhorOrdem;

    public static void main(String[] args) {
	/*
	 * App app = new App(); app.setSize(800, 600); app.addMouseListener(app); app.setVisible(true);
	 */
    }

    public PointsPanel() {
	points = new ArrayList<Point2D>();
	addMouseListener(this);
    }

    @Override
    public void paint(Graphics g) {
	Graphics2D g2d = (Graphics2D) g;

	synchronized (PAINT_MONITOR) {
	    if (points.size() == 0) {
		return;
	    }

	    if (points.size() == 1) {
		g2d.fillOval((int) points.get(0).getX(), (int) points.get(0).getY(), DIAMETRO_PONTO, DIAMETRO_PONTO);
		return;
	    }

	    int i = 1;
	    int FC = DIAMETRO_PONTO /2; //Fator de Centralização da reta
	    
	    for (; i < points.size(); i++) {
		Point2D p1 = points.get(i - 1);
		Point2D p2 = points.get(i);

		g2d.fillOval((int) p2.getX(), (int) p2.getY(), DIAMETRO_PONTO, DIAMETRO_PONTO);
		g2d.draw(new Line2D.Double(p1.getX() + FC, p1.getY() + FC, p2.getX() + FC, p2.getY() + FC));
	    }

	    Point2D p1 = points.get(points.size() -1);
	    Point2D p2 = points.get(0);

	    g2d.draw(new Line2D.Double(p1.getX() +FC, p1.getY() +FC, p2.getX() +FC, p2.getY() +FC));
	    g2d.fillOval((int) p2.getX(), (int) p2.getY(), DIAMETRO_PONTO, DIAMETRO_PONTO);
	    
	    if(melhorOrdem != null){
		 int r = 1;
		    
		    for (; r < melhorOrdem.size(); r++) {
			Point2D mp1 = points.get( melhorOrdem.get(r-1));
			Point2D mp2 = points.get( melhorOrdem.get(r) );
			
			g2d.setPaint(Color.red);
			g2d.draw(new Line2D.Double(mp1.getX() + FC, mp1.getY() + FC, mp2.getX() + FC, mp2.getY() + FC));
		    }
		    
		    Point2D mp1 = points.get( melhorOrdem.get(r -1));
		    Point2D mp2 = points.get( melhorOrdem.get(0) );
		    
		    g2d.draw(new Line2D.Double(mp1.getX() + FC, mp1.getY() + FC, mp2.getX() + FC, mp2.getY() + FC));
	    }

	}
    }

    public ArrayList<Point2D> getPoints() {
	return points;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
	if(cliqueBloqueado){
	    return;
	}
	

	synchronized (PAINT_MONITOR) {
	    boolean colidiu = false;
	    ArrayList<Point2D> newList = new ArrayList<Point2D>();

	    for (Iterator<Point2D> iteP = points.iterator(); iteP.hasNext();) {

		Point2D p2 = iteP.next();

		if (AgUtils.calculaDistancia(p2, e.getPoint()) - DISTANCIA_MINIMA > 0) {
		    newList.add(p2);
		}

	    }

	    if (!colidiu) {
		newList.add(e.getPoint());
	    }

	    points = newList;

	    updatePoints();
	}
    }

    public void setPoints(ArrayList<Point2D> points) {
	this.points = points;
	updatePoints();
    }

    public void clear() {
	points = new ArrayList<Point2D>();
	melhorOrdem = null;
	updatePoints();
    }

    public void deleteLastPoint() {
	if (points.isEmpty()) {
	    return;
	}

	points.remove(points.size() - 1);
	updatePoints();
    }

    private void updatePoints() {
	this.repaint();
    }

    @Override
    public void mousePressed(MouseEvent e) {
	// TODO Auto-generated method stub

    }

    @Override
    public void mouseReleased(MouseEvent e) {
	// TODO Auto-generated method stub

    }

    @Override
    public void mouseEntered(MouseEvent e) {
	// TODO Auto-generated method stub

    }

    @Override
    public void mouseExited(MouseEvent e) {
	// TODO Auto-generated method stub

    }
    
    public void setCliqueBloqueado(boolean cliqueBloqueado){
	this.cliqueBloqueado = cliqueBloqueado;
    }
    
    public void updateResult(List<Integer> melhorOrdem){
	this.melhorOrdem = melhorOrdem;
	repaint();
    }

}
