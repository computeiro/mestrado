package wender.ag5.populacao;

import java.util.List;

import wender.ag5.Cromossomo;

public interface IModuloPopulacional {
    List<Cromossomo> atualizaPopulacao(List<Cromossomo> antiga, List<Cromossomo> nova);
}
