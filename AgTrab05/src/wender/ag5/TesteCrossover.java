package wender.ag5;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TesteCrossover {

    public static void main(String[] args) {
	Integer[] a1 = {16, 13, 8, 5, 2, 9, 4, 17, 0, 3, 15, 11, 1, 6, 12, 14, 7, 10};
	Integer[] a2 = {13, 10, 6, 9, 14, 7, 17, 11, 15, 5, 2, 8, 16, 12, 4, 0, 3, 1};
	
	
	List<Integer> ordemPtos = new ArrayList<Integer>(Arrays.asList(a1));
	List<Integer> ordemPtos2 = new ArrayList<Integer>(Arrays.asList(a2));
	
	int p1 = 3;
	int p2 = 9;
	
	
	List<Integer> novo = new ArrayList<Integer>(Arrays.asList(new Integer[ordemPtos.size()]));
	
	for(int i = p1; i < p2; i++){
	    novo.add(i, ordemPtos.get(i));
	}
	
	for(int i = 0; i < ordemPtos.size(); i++){
	  
	    if(i >= p1 && i < p2){
		continue;
	    }
	    
	    novo.set(i, pmx(novo, ordemPtos,  ordemPtos2, ordemPtos2.get(i) ));
	    
	}
	
	novo = novo.subList(0, ordemPtos.size());
	System.out.println(Cariotipo.asString(novo) );
    }
    
    private static int pmx(List<Integer> novo, List<Integer> p1, List<Integer> p2, int valor){
	int idx = novo.indexOf(valor);
	
	if(idx == -1){
	    return  valor;
	}else{
	    return pmx(novo, p2, p1, p1.get(idx));
	}
  	
    }
    
   
}
