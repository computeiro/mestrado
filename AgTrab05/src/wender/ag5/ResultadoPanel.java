package wender.ag5;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JPanel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.NumberTickUnit;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import wender.ag5.handlers.ResultPaneHandler;

public class ResultadoPanel extends JPanel {
    private static final long serialVersionUID = 1L;
    private ChartPanel chartPopulacao;
    private ChartPanel chartIndividuo;
    private XYSeries serieInd;
    private XYSeries seriePopulacao;
    private ResultPaneHandler resultPaneHandler;

    public ResultadoPanel() {
	GridBagLayout gridBagLayout = new GridBagLayout();
	gridBagLayout.columnWidths = new int[] { 0, 0 };
	gridBagLayout.rowHeights = new int[] { 0, 0, 0 };
	gridBagLayout.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
	gridBagLayout.rowWeights = new double[] { 1.0, 1.0, Double.MIN_VALUE };
	setLayout(gridBagLayout);

	final XYSeriesCollection dataSetInd = new XYSeriesCollection();
	serieInd = new XYSeries("Avaliação individual");
	dataSetInd.addSeries(serieInd);

	chartIndividuo = createChartPanel("Melhor indivíduo", dataSetInd);
	GridBagConstraints gbc_panel = new GridBagConstraints();
	gbc_panel.insets = new Insets(0, 0, 5, 0);
	gbc_panel.fill = GridBagConstraints.BOTH;
	gbc_panel.gridx = 0;
	gbc_panel.gridy = 0;
	add(chartIndividuo, gbc_panel);

	final XYSeriesCollection dataSetPop = new XYSeriesCollection();
	seriePopulacao = new XYSeries("Avaliação População");
	dataSetPop.addSeries(seriePopulacao);

	chartPopulacao = createChartPanel("Média da População", dataSetPop);
	GridBagConstraints gbc_panel_1 = new GridBagConstraints();
	gbc_panel_1.fill = GridBagConstraints.BOTH;
	gbc_panel_1.gridx = 0;
	gbc_panel_1.gridy = 1;
	add(chartPopulacao, gbc_panel_1);

	resultPaneHandler = new ResultPaneHandler() {
	    private Cariotipo cariotipo;

	    @Override
	    public Runnable getPopulacaoChangeRunnable() {
		return new Runnable() {
		    @Override
		    public void run() {
			serieInd.add(cariotipo.getGeracao(), cariotipo.getMelhorIndividuo().getAvaliacao());
			seriePopulacao.add(cariotipo.getGeracao(), cariotipo.getMediaPopulacao());

		    }
		};
	    }

	    public void setCariotipo(Cariotipo cariotipo) {
		this.cariotipo = cariotipo;
	    }

	    @Override
	    public void clearResult() {
		clear();
	    }

	};
    }

    public ResultPaneHandler getResultPaneHandler() {
	return resultPaneHandler;
    }

    public ChartPanel createChartPanel(String title, XYSeriesCollection dataset) {

	JFreeChart chart = ChartFactory.createXYLineChart(title,// title
		"geração", // x axis label
		"avaliação", // y axis label
		dataset, // data
		PlotOrientation.VERTICAL, false, // include legend
		true, // tooltips
		false // urls
		);

	chart.setBackgroundPaint(Color.white);

	final XYPlot plot = chart.getXYPlot();
	plot.setBackgroundPaint(Color.lightGray);
	plot.setDomainGridlinePaint(Color.white);
	plot.setRangeGridlinePaint(Color.white);
	plot.getRenderer().setSeriesPaint(0, new Color(0x00, 0x00, 0xFF));
	
	
	NumberAxis xAxis = new NumberAxis();
	xAxis.setTickUnit(new NumberTickUnit(50));
	plot.setDomainAxis(xAxis);

	return new ChartPanel(chart);

    }

    public void clear() {
	serieInd.clear();
	seriePopulacao.clear();
    }

    public static void main(String[] args) {
    }
}
