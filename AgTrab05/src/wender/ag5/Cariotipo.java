package wender.ag5;

import java.awt.EventQueue;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import wender.ag5.handlers.ResultPaneHandler;
import wender.ag5.mutacao.IMutacao;
import wender.ag5.populacao.IModuloPopulacional;
import wender.ag5.selecao.ISelecao;

public class Cariotipo {
    private AgConf conf;
    private ArrayList<Point2D> points;
    private List<Cromossomo> populacao;
    private int geracao;
    private Cromossomo melhorIndividuo;
    private double mediaPopulacao;

    public Cariotipo(AgConf conf) {
	this.conf = conf;
    }

    public int getGeracao() {
	return geracao;
    }

    public Cromossomo getMelhorIndividuo() {
	return melhorIndividuo;
    }

    public double getMediaPopulacao() {
	return mediaPopulacao;
    }

    public void executaEvoluacao(ArrayList<Point2D> points, ResultPaneHandler resultPaneHandler, Runnable finishHandler) {
	ExecutorEvolucao ee = new ExecutorEvolucao(points, resultPaneHandler, finishHandler);
	new Thread(ee).start();
    }

    public static String asString(List<Integer> list) {
	StringBuilder s = new StringBuilder();

	s.append("{");

	for (Iterator<Integer> ite = list.iterator(); ite.hasNext();) {
	    s.append(ite.next());

	    if (ite.hasNext()) {
		s.append(", ");
	    }
	}
	s.append("}");
	return s.toString();
    }

    private class ExecutorEvolucao implements Runnable {
	private ResultPaneHandler resultPaneHandler;
	private ArrayList<Point2D> points;
	private Runnable finishHandler;

	ExecutorEvolucao(ArrayList<Point2D> points, ResultPaneHandler resultPaneHandler, Runnable finishHandler) {
	    this.resultPaneHandler = resultPaneHandler;
	    this.points = points;
	    this.finishHandler = finishHandler;
	}

	@Override
	public void run() {

	    if (points.size() < 4) {
		throw new IllegalArgumentException("Por favor defina uma quantidade maior ou igual a 4 pontos.");
	    }

	    final StringBuilder log = new StringBuilder();

	    ArrayList<Integer> base = new ArrayList<>();

	    for (int i = 0; i < points.size(); i++) {
		base.add(i);
	    }

	    // Inicializando populacação
	    populacao = new ArrayList<>();

	    for (int i = 0; i < conf.getTamanhoPopulaca(); i++) {
		ArrayList<Integer> ordem = (ArrayList<Integer>) base.clone();
		Collections.shuffle(ordem);

		populacao.add(new Cromossomo(ordem, this.points));
	    }

	    ISelecao selecao = conf.getSelecao();
	    selecao.init(populacao);

	    IMutacao mutacao = conf.getTipoMutacao();
	    IModuloPopulacional modPop = conf.getModuloPopulacional();

	    for (geracao = 0; geracao < conf.getLimiteGeracao(); geracao++) {
		List<Cromossomo> novaPopulacao = new ArrayList<>();

		for (int p = 0; p < populacao.size(); p++) {
		    Cromossomo c1 = selecao.get();
		    Cromossomo c2 = selecao.get();

		    Cromossomo f = c1.crossover(c2);
		    //mutacao.apply(f, conf.getTaxaMutacao());

		    novaPopulacao.add(p, f);
		}

		populacao = modPop.atualizaPopulacao(populacao, novaPopulacao);

		double soma = populacao.get(0).getAvaliacao();
		melhorIndividuo = populacao.get(0);

		for (int i = 1; i < populacao.size(); i++) {

		    if (populacao.get(i).getAvaliacao() < melhorIndividuo.getAvaliacao()) {
			melhorIndividuo = populacao.get(i);
		    }

		    soma += populacao.get(i).getAvaliacao();
		}

		mediaPopulacao = soma / populacao.size();

		System.out.println("         Geração: " + geracao);
		System.out.println("Melhor indivíduo: " + melhorIndividuo.getAvaliacao());
		System.out.println("           Ordem: " + Cariotipo.asString(melhorIndividuo.getOrdemPtos()));
		System.out.println("           Média: " + mediaPopulacao);

		try {
		    EventQueue.invokeAndWait(resultPaneHandler.getPopulacaoChangeRunnable());
		} catch (Exception e) {
		    e.printStackTrace();
		}
	    }

	    System.out.println(log.toString());
	    
	    try {
		EventQueue.invokeAndWait(finishHandler);
	    } catch (Exception e) {
		e.printStackTrace();
	    }

	}

    }
}
