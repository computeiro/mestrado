package wender.ag5;



import java.awt.EventQueue;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JTabbedPane;


public class Ag5App {

    private JFrame frame;

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
	EventQueue.invokeLater(new Runnable() {
	    public void run() {
		try {
		    Ag5App window = new Ag5App();
		    window.frame.setVisible(true);
		} catch (Exception e) {
		    e.printStackTrace();
		}
	    }
	});
    }

    /**
     * Create the application.
     */
    public Ag5App() {
	initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
	frame = new JFrame();
	frame.setBounds(0, 0, 600, 300);
	frame.setTitle("Algoritmos Genéticos - Encontrando o mínimo global");
	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	GridBagLayout gridBagLayout = new GridBagLayout();
	gridBagLayout.columnWidths = new int[]{0, 0};
	gridBagLayout.rowHeights = new int[]{0, 0};
	gridBagLayout.columnWeights = new double[]{1.0, Double.MIN_VALUE};
	gridBagLayout.rowWeights = new double[]{1.0, Double.MIN_VALUE};
	frame.getContentPane().setLayout(gridBagLayout);
	
	JTabbedPane tabPane = new JTabbedPane(JTabbedPane.TOP);
	GridBagConstraints gbc_tabbedPane = new GridBagConstraints();
	gbc_tabbedPane.fill = GridBagConstraints.BOTH;
	gbc_tabbedPane.gridx = 0;
	gbc_tabbedPane.gridy = 0;
	frame.getContentPane().add(tabPane, gbc_tabbedPane);
	frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
	
	ResultadoPanel chartResult = new ResultadoPanel();
	final ConfiguracoesPanel panelConf = new ConfiguracoesPanel(chartResult.getResultPaneHandler());
	
	tabPane.addTab("Configurações", null, panelConf, null);
	tabPane.addTab("Resultado", null, chartResult, null);
	
	tabPane.setIconAt(0, new ImageIcon(Ag5App.class.getResource("resources/dna.png")));
	tabPane.setIconAt(1, new ImageIcon(Ag5App.class.getResource("resources/macaco.png")));
	
    }
    
    public enum TipoSelecao{
	Roleta("Roleta"), Torneio("Torneio"), Rank("Rank");
	
	private String label;
	
	TipoSelecao(String label){
	    this.label = label;
	}
	
	@Override
	public String toString() {
	    return label;
	}
    }
    
    public enum TipoMutacao{
	Swap("Swap"), Pbm("PMB");
	
	private String label;
	
	TipoMutacao(String label){
	    this.label = label;
	}
	
	@Override
	public String toString() {
	    return label;
	}
	
    }
}
