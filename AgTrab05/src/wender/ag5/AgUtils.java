package wender.ag5;

import java.awt.geom.Point2D;

public class AgUtils {
    public static double calculaDistancia(Point2D p1, Point2D p2) {
   	double dist = Math.sqrt(Math.pow(p1.getX() - p2.getX(), 2) + Math.pow(p1.getY() - p2.getY(), 2));
   	return dist;
    }
}
