package wender.ag5;

import java.awt.Point;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Cromossomo {
    private ArrayList<Integer> ordemPtos;
    private ArrayList<Point2D> points;
    private double avaliacao;

    public List<Integer> getOrdemPtos() {
	return ordemPtos;
    }

    public static void main(String[] args) {
	ArrayList<Point2D> points = new ArrayList<Point2D>();

	Point2D p0 = new Point(500, 500);
	Point2D p1 = new Point(0, 0);
	Point2D p2 = new Point(0, 40);
	Point2D p3 = new Point(40, 0);
	Point2D p4 = new Point(20, 20);
	Point2D p5 = new Point(40, 20);
	Point2D p6 = new Point(100, 100);
	Point2D p7 = new Point(500, 0);
	Point2D p8 = new Point(5, 200);
	Point2D p9 = new Point(500, 500);

	points.add(p0);
	points.add(p1);
	points.add(p2);
	points.add(p3);
	points.add(p4);
	points.add(p5);
	points.add(p6);
	points.add(p7);
	points.add(p8);
	points.add(p9);

	ArrayList<Integer> ordemPtos = new ArrayList<>(Arrays.asList(new Integer[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 }));
	ArrayList<Integer> ordemPtos2 = new ArrayList<>(Arrays.asList(new Integer[] { 9, 8, 7, 6, 5, 4, 3, 2, 1, 0 }));

	Cromossomo c1 = new Cromossomo(ordemPtos, points);
	Cromossomo c2 = new Cromossomo(ordemPtos2, points);

	Cromossomo f1 = new Cromossomo(ordemPtos, points);
	Cromossomo f2 = new Cromossomo(ordemPtos2, points);

	for (int i = 0; i < 200; i++) {
	    f1 = c1.crossover(c2);
	    f2 = c2.crossover(c1);

	    c1 = f1;
	    c2 = f2;
	}

    }

    public double getAvaliacao() {
	return avaliacao;
    }

    public Cromossomo(ArrayList<Integer> ordemPtos, ArrayList<Point2D> points) {
	this.ordemPtos = ordemPtos;
	this.points = points;
	this.avaliacao = 0;

	if (points.size() < 2 || ordemPtos.size() < 2) {
	    throw new IllegalArgumentException("Não é possível calcuar a distancia total do Cromossomo pois não há pontos definidos!");
	}

	int i = 1;
	for (; i < this.ordemPtos.size(); i++) {
	    int e1 = ordemPtos.get(i - 1);
	    int e2 = ordemPtos.get(i);

	    this.avaliacao += AgUtils.calculaDistancia(points.get(e1), points.get(e2));
	}

	this.avaliacao += AgUtils.calculaDistancia(points.get(i - 1), points.get(0));
    }

    public Cromossomo crossover(Cromossomo c2) {
	int p1 = 0;

	while (p1 == 0) {
	    p1 = (int) (Math.random() * (ordemPtos.size() - 2));
	}

	int p2 = p1;

	while (p1 == p2) {
	    p2 = (int) (Math.random() * (ordemPtos.size() - 2));
	}

	int aux = p1;
	p1 = Math.min(p1, p2);
	p2 = Math.max(aux, p2);

	System.out.println(p1);
	System.out.println(p2);


	ArrayList<Integer> newOrderList = new ArrayList<Integer>(Arrays.asList(new Integer[ordemPtos.size()]));
	
	for(int i = p1; i < p2; i++){
	    newOrderList.add(i, ordemPtos.get(i));
	}
	
	for(int i = 0; i < ordemPtos.size(); i++){
	  
	    if(i >= p1 && i < p2){
		continue;
	    }
	    
	    newOrderList.set(i, pmx(newOrderList, ordemPtos,  c2.ordemPtos, c2.ordemPtos.get(i) ));
	    
	}
	
	newOrderList = new ArrayList<Integer>(newOrderList.subList(0, ordemPtos.size()) );

	return new Cromossomo(newOrderList, this.points);
    }

    private static boolean repitiu(List<Integer> ordem) {
	for (int i = 0; i < ordem.size(); i++) {
	    for (int j = i + 1; j < ordem.size(); j++) {
		if (ordem.get(i) == ordem.get(j)) {
		    return true;
		}
	    }
	}

	return false;
    }

    private static int pmx(List<Integer> novo, List<Integer> p1, List<Integer> p2, int valor) {
	int idx = novo.indexOf(valor);

	if (idx == -1) {
	    return valor;
	} else {
	    return pmx(novo, p2, p1, p1.get(idx));
	}

    }

}
