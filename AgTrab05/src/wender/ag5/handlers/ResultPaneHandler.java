package wender.ag5.handlers;

import wender.ag5.Cariotipo;

public interface ResultPaneHandler {
    public Runnable getPopulacaoChangeRunnable();
    public void setCariotipo(Cariotipo cariotipo);
    public void clearResult();
}
