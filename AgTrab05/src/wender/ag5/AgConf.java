package wender.ag5;



import wender.ag5.mutacao.IMutacao;
import wender.ag5.populacao.IModuloPopulacional;
import wender.ag5.selecao.ISelecao;

public interface AgConf {
    ISelecao getSelecao();
    int getTamanhoPopulaca();
    double getTaxaMutacao();
    IMutacao getTipoMutacao();
    IModuloPopulacional getModuloPopulacional();
    int getLimiteGeracao();
}
