package wender.ag5;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

import wender.ag5.Ag5App.TipoMutacao;
import wender.ag5.Ag5App.TipoSelecao;
import wender.ag5.handlers.ResultPaneHandler;
import wender.ag5.mutacao.IMutacao;
import wender.ag5.mutacao.PositionBasedMutation;
import wender.ag5.mutacao.SwapMutation;
import wender.ag5.populacao.ElitismoPopulacional;
import wender.ag5.populacao.IModuloPopulacional;
import wender.ag5.populacao.SubstituiPopulacao;
import wender.ag5.selecao.ISelecao;
import wender.ag5.selecao.Rank;
import wender.ag5.selecao.Roleta;
import wender.ag5.selecao.Torneio;

public class ConfiguracoesPanel extends JPanel implements AgConf {
    private static final long serialVersionUID = 1L;

    private JComboBox<TipoSelecao> cmbSelecao;
    private JComboBox<TipoMutacao> cmbMutacao;
    private JSpinner spTxMutacao;
    private JSpinner spTamanhoPopulacao;
    private JSpinner spLimiteGeracao;
    private JSpinner spElitismo;
    private JLabel lbMaxGeracoes;
    private JLabel lbTipoMutacao;
    private PointsPanel pointsPanel;
    private JLabel label;
    private JPanel buttonsBar;
    private JButton btnLimparTudo;
    private JButton btnDeletarPonto;
    private JButton btnExecutar;
    private JLabel lbPointsPanel;
    private JLabel lbTamanhoPopulacao;
    private ResultPaneHandler resultPaneHandler;

    public ConfiguracoesPanel(ResultPaneHandler resultPaneHandler){
	this.resultPaneHandler = resultPaneHandler;
	
	GridBagLayout gbl_this = new GridBagLayout();
	gbl_this.columnWidths = new int[]{0, 0, 0, 0, 0};
	gbl_this.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
	gbl_this.columnWeights = new double[]{0.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};
	gbl_this.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
	this.setLayout(gbl_this);
	
	JLabel lblSelecao = new JLabel("Seleção:");
	GridBagConstraints gbc_lblSelecao = new GridBagConstraints();
	gbc_lblSelecao.insets = new Insets(0, 0, 5, 5);
	gbc_lblSelecao.anchor = GridBagConstraints.EAST;
	gbc_lblSelecao.gridx = 0;
	gbc_lblSelecao.gridy = 0;
	this.add(lblSelecao, gbc_lblSelecao);
	
	cmbSelecao = new JComboBox<TipoSelecao>();
	GridBagConstraints gbc_cmbSelecao = new GridBagConstraints();
	gbc_cmbSelecao.insets = new Insets(0, 0, 5, 5);
	gbc_cmbSelecao.fill = GridBagConstraints.HORIZONTAL;
	gbc_cmbSelecao.gridx = 1;
	gbc_cmbSelecao.gridy = 0;
	this.add(cmbSelecao, gbc_cmbSelecao);
	
	cmbSelecao.addItem(TipoSelecao.Roleta);
	cmbSelecao.addItem(TipoSelecao.Torneio);
	cmbSelecao.addItem(TipoSelecao.Rank);
	
	label = new JLabel("");
	GridBagConstraints gbc_label = new GridBagConstraints();
	gbc_label.insets = new Insets(0, 0, 5, 0);
	gbc_label.gridx = 3;
	gbc_label.gridy = 0;
	add(label, gbc_label);
	
	lbTipoMutacao = new JLabel("Tipo de Mutação:");
	GridBagConstraints gbc_lbTipoMutacao = new GridBagConstraints();
	gbc_lbTipoMutacao.anchor = GridBagConstraints.EAST;
	gbc_lbTipoMutacao.insets = new Insets(0, 0, 5, 5);
	gbc_lbTipoMutacao.gridx = 0;
	gbc_lbTipoMutacao.gridy = 1;
	add(lbTipoMutacao, gbc_lbTipoMutacao);
	
	cmbMutacao = new JComboBox<TipoMutacao>();
	GridBagConstraints gbc_comboBox = new GridBagConstraints();
	gbc_comboBox.insets = new Insets(0, 0, 5, 5);
	gbc_comboBox.fill = GridBagConstraints.HORIZONTAL;
	gbc_comboBox.gridx = 1;
	gbc_comboBox.gridy = 1;
	add(cmbMutacao, gbc_comboBox);
	
	cmbMutacao.addItem(TipoMutacao.Swap);
	cmbMutacao.addItem(TipoMutacao.Pbm);
	
	JLabel lblTxMutacao = new JLabel("Mutação (0,001 - 1):");
	GridBagConstraints gbc_lblTxMutacao = new GridBagConstraints();
	gbc_lblTxMutacao.anchor = GridBagConstraints.EAST;
	gbc_lblTxMutacao.insets = new Insets(0, 0, 5, 5);
	gbc_lblTxMutacao.gridx = 0;
	gbc_lblTxMutacao.gridy = 2;
	this.add(lblTxMutacao, gbc_lblTxMutacao);
	
	spTxMutacao = new JSpinner(new SpinnerNumberModel(0.005, 0, 1, 0.001));
	GridBagConstraints gbc_spTxMutacao = new GridBagConstraints();
	gbc_spTxMutacao.fill = GridBagConstraints.HORIZONTAL;
	gbc_spTxMutacao.insets = new Insets(0, 0, 5, 5);
	gbc_spTxMutacao.gridx = 1;
	gbc_spTxMutacao.gridy = 2;
	this.add(spTxMutacao, gbc_spTxMutacao);
	
	lbTamanhoPopulacao = new JLabel("Tamanho da população:");
	GridBagConstraints gbc_lbTamanhoPopulacao = new GridBagConstraints();
	gbc_lbTamanhoPopulacao.anchor = GridBagConstraints.EAST;
	gbc_lbTamanhoPopulacao.insets = new Insets(0, 0, 5, 5);
	gbc_lbTamanhoPopulacao.gridx = 0;
	gbc_lbTamanhoPopulacao.gridy = 3;
	add(lbTamanhoPopulacao, gbc_lbTamanhoPopulacao);
	
	spTamanhoPopulacao = new JSpinner(new SpinnerNumberModel(50, 5, 100, 1));
	GridBagConstraints gbc_spTamanhoPopulacao = new GridBagConstraints();
	gbc_spTamanhoPopulacao.fill = GridBagConstraints.HORIZONTAL;
	gbc_spTamanhoPopulacao.insets = new Insets(0, 0, 5, 5);
	gbc_spTamanhoPopulacao.gridx = 1;
	gbc_spTamanhoPopulacao.gridy = 3;
	add(spTamanhoPopulacao, gbc_spTamanhoPopulacao);
	
	lbMaxGeracoes = new JLabel("Limite de Gerações:");
	GridBagConstraints gbc_lbMaxGeracoes = new GridBagConstraints();
	gbc_lbMaxGeracoes.anchor = GridBagConstraints.EAST;
	gbc_lbMaxGeracoes.insets = new Insets(0, 0, 5, 5);
	gbc_lbMaxGeracoes.gridx = 0;
	gbc_lbMaxGeracoes.gridy = 4;
	add(lbMaxGeracoes, gbc_lbMaxGeracoes);
	
	spLimiteGeracao = new JSpinner(new SpinnerNumberModel(50, 1, 1000, 1));
	GridBagConstraints gcbLimGeracao = new GridBagConstraints();
	gcbLimGeracao.fill = GridBagConstraints.HORIZONTAL;
	gcbLimGeracao.insets = new Insets(0, 0, 5, 5);
	gcbLimGeracao.gridx = 1;
	gcbLimGeracao.gridy = 4;
	add(spLimiteGeracao, gcbLimGeracao);
	
	JLabel lblElitismo = new JLabel("Elitismo:");
	GridBagConstraints gbc_lblElitismo = new GridBagConstraints();
	gbc_lblElitismo.anchor = GridBagConstraints.EAST;
	gbc_lblElitismo.insets = new Insets(0, 0, 5, 5);
	gbc_lblElitismo.gridx = 0;
	gbc_lblElitismo.gridy = 5;
	this.add(lblElitismo, gbc_lblElitismo);
	
	spElitismo = new JSpinner(new SpinnerNumberModel(1, 0, 100, 1));
	GridBagConstraints gbc_spElitismo = new GridBagConstraints();
	gbc_spElitismo.insets = new Insets(0, 0, 5, 5);
	gbc_spElitismo.fill = GridBagConstraints.HORIZONTAL;
	gbc_spElitismo.gridx = 1;
	gbc_spElitismo.gridy = 5;
	this.add(spElitismo, gbc_spElitismo);
	
	buttonsBar = new JPanel();
	GridBagConstraints gbc_panel_1 = new GridBagConstraints();
	gbc_panel_1.anchor = GridBagConstraints.NORTH;
	gbc_panel_1.gridwidth = 3;
	gbc_panel_1.insets = new Insets(0, 0, 5, 5);
	gbc_panel_1.fill = GridBagConstraints.HORIZONTAL;
	gbc_panel_1.gridx = 0;
	gbc_panel_1.gridy = 6;
	add(buttonsBar, gbc_panel_1);
	GridBagLayout gbl_panel_1 = new GridBagLayout();
	gbl_panel_1.columnWidths = new int[]{0, 0, 0, 0};
	gbl_panel_1.rowHeights = new int[]{0, 0};
	gbl_panel_1.columnWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
	gbl_panel_1.rowWeights = new double[]{0.0, Double.MIN_VALUE};
	buttonsBar.setLayout(gbl_panel_1);
	
	btnLimparTudo = new JButton("Limpar");
	btnLimparTudo.setIcon(new ImageIcon(Ag5App.class.getResource("resources/broomWarning.png")));
	GridBagConstraints gbc_btnLimparTudo = new GridBagConstraints();
	gbc_btnLimparTudo.insets = new Insets(0, 0, 0, 5);
	gbc_btnLimparTudo.gridx = 0;
	gbc_btnLimparTudo.gridy = 0;
	buttonsBar.add(btnLimparTudo, gbc_btnLimparTudo);
	
	btnDeletarPonto = new JButton("Deletar ponto");
	btnDeletarPonto.setIcon(new ImageIcon(Ag5App.class.getResource("resources/broomArrow.png")));
	GridBagConstraints gbcDelPonto = new GridBagConstraints();
	gbcDelPonto.insets = new Insets(0, 0, 0, 5);
	gbcDelPonto.gridx = 1;
	gbcDelPonto.gridy = 0;
	buttonsBar.add(btnDeletarPonto, gbcDelPonto);
	
	btnExecutar = new JButton("Executar");
	btnExecutar.setIcon(new ImageIcon(Ag5App.class.getResource("resources/raio.png")));
	GridBagConstraints gbcExecutar = new GridBagConstraints();
	gbcExecutar.anchor = GridBagConstraints.SOUTH;
	gbcExecutar.gridx = 2;
	gbcExecutar.gridy = 0;
	buttonsBar.add(btnExecutar, gbcExecutar);
	
	lbPointsPanel = new JLabel("Clique no espaço abaixo para inserir os pontos para o roteamento!");
	GridBagConstraints gbc_lbPointsPanel = new GridBagConstraints();
	gbc_lbPointsPanel.gridwidth = 3;
	gbc_lbPointsPanel.insets = new Insets(0, 0, 5, 5);
	gbc_lbPointsPanel.gridx = 0;
	gbc_lbPointsPanel.gridy = 7;
	add(lbPointsPanel, gbc_lbPointsPanel);
	
	pointsPanel = new PointsPanel();
	pointsPanel.setBackground(Color.WHITE);
	GridBagConstraints gbc_panel = new GridBagConstraints();
	gbc_panel.gridwidth = 3;
	gbc_panel.insets = new Insets(0, 0, 0, 5);
	gbc_panel.fill = GridBagConstraints.BOTH;
	gbc_panel.gridx = 0;
	gbc_panel.gridy = 8;
	add(pointsPanel, gbc_panel);
	
	btnLimparTudo.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    pointsPanel.clear();
		}
	});
	
	btnDeletarPonto.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    pointsPanel.deleteLastPoint();
		}
	});
	
	btnExecutar.addActionListener(new ActionListener() {
	    
	    @Override
	    public void actionPerformed(ActionEvent e) {
		executa();
	    }
	});
    }

    @Override
    public int getLimiteGeracao() {
	return Integer.parseInt(spLimiteGeracao.getValue().toString());
    }

    @Override
    public ISelecao getSelecao() {
	TipoSelecao selecionado = (TipoSelecao) cmbSelecao.getSelectedItem();
	
	switch(selecionado){
		case Torneio:
		    return new Torneio();
		    
		case Rank:
		    return new Rank();
		 
		default:
		    return new Roleta();
		    
	}
    }
    
    @Override
    public IMutacao getTipoMutacao(){
	TipoMutacao selecionado = (TipoMutacao) cmbMutacao.getSelectedItem();
	
	if(selecionado == TipoMutacao.Swap){
	    return new SwapMutation();
	}
	
	return new PositionBasedMutation();
    }

    @Override
    public double getTaxaMutacao() {
	return (Double) spTxMutacao.getValue();
    }

    @Override
    public IModuloPopulacional getModuloPopulacional() {
	int individuosElite = (Integer) spElitismo.getValue();

	if (individuosElite == 0) {
	    return new SubstituiPopulacao();
	} else {
	    return new ElitismoPopulacional(individuosElite);
	}

    }

    @Override
    public int getTamanhoPopulaca() {
	return (int) spTamanhoPopulacao.getValue();
    }
    
    private void executa(){
	try{
	    resultPaneHandler.clearResult();
	    pointsPanel.updateResult(null);
	    
	    pointsPanel.setCliqueBloqueado(true);
	    btnExecutar.setEnabled(false);
	    btnDeletarPonto.setEnabled(false);
	    btnLimparTudo.setEnabled(false);
	    
	    final Cariotipo cariotipo = new Cariotipo(this);
	    resultPaneHandler.setCariotipo(cariotipo);
	    
	    Runnable finishHdl = new Runnable() {
	        
	        @Override
	        public void run() {
	            pointsPanel.updateResult(cariotipo.getMelhorIndividuo().getOrdemPtos());
	    	
	        }
	    };
	    
	   cariotipo.executaEvoluacao(pointsPanel.getPoints(), resultPaneHandler, finishHdl);
	    
	}catch(Exception e){
	    e.printStackTrace();
	    JOptionPane.showMessageDialog(this, e.getMessage(), "Falha ao executar", JOptionPane.WARNING_MESSAGE);
	}finally{
	    pointsPanel.setCliqueBloqueado(false);
	    btnExecutar.setEnabled(true);
	    btnDeletarPonto.setEnabled(true);
	    btnLimparTudo.setEnabled(true);
	}
    }
}
