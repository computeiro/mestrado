package wender.ag05;

public class Cromossomo {
    // Potência e Tempo de manutenção das turbinas
    public static final int[] POTENCIA_TURBINAS = new int[] { 20, 15, 35, 40, 15, 15, 10 };
    public static final int[] MANUTENCAO_TURBINAS = new int[] { 2, 2, 1, 1, 1, 1, 1 };
    public static final int EM_MANUTENCAO = 0;

    public static final int[] PD_PERIODO = new int[] { 80, 90, 65, 70 };

    public static final int GENES = POTENCIA_TURBINAS.length * PD_PERIODO.length;
    public static final double MAIOR_DESVIO;

    // 7 turbinas x 4 periodos
    private int[] value;
    private double avaliacao;
    private boolean valido;
    private boolean satisfaz;
    private int[] potenciaPeriodos;
    private int potenciaTotal;

    static {
	int soma = 0;

	for (int i = 0; i < POTENCIA_TURBINAS.length; i++) {
	    soma += POTENCIA_TURBINAS[i];
	}

	MAIOR_DESVIO = (soma / PD_PERIODO.length * (PD_PERIODO.length - 1)) + 1;
    }

    public static void main(String[] args) {
	
	int[] v = new int[] { 
		1, 0, 0, 0, 0, 0, 0, 
		0, 0, 0, 0, 1, 1, 0,
		0, 0, 0, 1, 0, 0, 1, 
		0, 1, 0, 0, 0, 0, 0, };
	
	Cromossomo c = new Cromossomo(v);
	
	System.out.println(c.dump());
    }


    public static Cromossomo gerarAleatorio() {
	int[] value = new int[GENES];

	for (int i = 0; i < GENES; i++) {
	    value[i] = (Math.random() > 0.5) ? 1 : 0;
	}

	return new Cromossomo(value);
    }

    public Cromossomo(int[] value) {
	this.value = value;


	potenciaPeriodos = new int[PD_PERIODO.length];

	int idxP = 0;
	potenciaPeriodos[0] = value[0] * POTENCIA_TURBINAS[0];
	potenciaTotal = 0;

	for (int i = 1; i < value.length; i++) {
	    if (i % POTENCIA_TURBINAS.length == 0) {
		idxP++;
	    }

	    double valor = value[i] * POTENCIA_TURBINAS[i % POTENCIA_TURBINAS.length];
	    potenciaPeriodos[idxP] += valor;
	    potenciaTotal += valor;
	}

	double media = potenciaTotal / PD_PERIODO.length;

	double desvioTotal = 0;
	for (int totalP : potenciaPeriodos) {
	    desvioTotal += Math.abs(totalP - media);
	}
	
	valido = atendeDemandaPorPeriodo();
	satisfaz = true;

	for (int i = 0; i < PD_PERIODO.length; i++) {
	    if (PD_PERIODO[i] > potenciaPeriodos[i]) {
		satisfaz = false;
		break;
	    }
	}

	avaliacao = (MAIOR_DESVIO - desvioTotal) * fatorValidadeSatisfacao(potenciaPeriodos);
    }

    public double fatorValidadeSatisfacao(int[] potenciaPeriodos) {
	
	if (!valido && !satisfaz) {
	    return -2;
	}

	if (!valido && satisfaz) {
	    return -1;
	}

	if (valido && !satisfaz) {
	    return 1;
	}

	return 2;
    }

    public String dump() {
	StringBuilder s = new StringBuilder();
	
	int idxPeriodo = 0;
	for (int gene = 0; gene < value.length; gene++) {
	    s.append(padl(value[gene] * POTENCIA_TURBINAS[gene % POTENCIA_TURBINAS.length], 2) );
	    s.append(" ");
	    
	    if ((gene + 1) % POTENCIA_TURBINAS.length == 0) {
		
		s.append(" Pg: ").append(padl(potenciaPeriodos[idxPeriodo], 3) );
		s.append(" Pd: ").append(padl(PD_PERIODO[idxPeriodo], 3) );
		s.append(" Pl: ").append(padl(potenciaPeriodos[idxPeriodo] -PD_PERIODO[idxPeriodo], 3));
		s.append("\n");
		
		idxPeriodo++;
	    }
	}
	
	s.append("\n  Valido: ").append(isValido());
	s.append("\nSatisfaz: ").append(isSatisfaz());
	return s.toString();
    }
    
    public String padl(Object o, int pad){
	StringBuilder s = new StringBuilder();
	s.append(o);
	
	while(s.length() < pad){
	    s.insert(0, " ");
	}
	
	return s.toString();
    }
    
    public boolean ehValido() {
	return valido;
    }

    private boolean atendeDemandaPorPeriodo() {

	proxima_turbina: 
	for (int t = 0; t < MANUTENCAO_TURBINAS.length; t++) {
	    int seqEmManutencao = 0;

	    for (int p = 0; p < PD_PERIODO.length && seqEmManutencao < MANUTENCAO_TURBINAS[t]; p++) {
		int idx = t + (p * MANUTENCAO_TURBINAS.length);

		if (value[idx] == EM_MANUTENCAO) {
		    seqEmManutencao++;
		    if (MANUTENCAO_TURBINAS[t] < seqEmManutencao) {
			break proxima_turbina;
		    }
		} else {
		    seqEmManutencao = 0;
		}
	    }

	    if (MANUTENCAO_TURBINAS[t] > seqEmManutencao) {
		return false;
	    }
	}

	return true;
    }

    public Cromossomo crossOverUmPonto(Cromossomo pai2) {
	int ponto = (int) (Math.random() * (value.length - 1));

	int[] r = new int[value.length];

	for (int i = 0; i < r.length; i++) {

	    if (i < ponto) {
		r[i] = this.value[i];
	    } else {
		r[i] = pai2.value[i];
	    }

	}

	return new Cromossomo(r);
    }

    public void mutacao(double change) {
	for (int i = 0; i < value.length; i++) {
	    if (change > Math.random()) {
		if (value[i] == 0) {
		    value[i] = 1;
		} else {
		    value[i] = 0;
		}
	    }
	}
    }
    
    public double getAvaliacao() {
  	return avaliacao;
    }


    public boolean isValido() {
        return valido;
    }


    public boolean isSatisfaz() {
        return satisfaz;
    }


    public int[] getPotenciaPeriodos() {
        return potenciaPeriodos;
    }
    
    public int[] getValue(){
	return value;
    }

}
