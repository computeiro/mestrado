package wender.ag05;

import java.awt.EventQueue;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

public class AgApp {

    private JFrame frame;

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
	EventQueue.invokeLater(new Runnable() {
	    public void run() {
		try {
		    AgApp window = new AgApp();
		    window.frame.setVisible(true);
		} catch (Exception e) {
		    e.printStackTrace();
		}
	    }
	});
    }

    /**
     * Create the application.
     */
    public AgApp() {
	initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
	frame = new JFrame();
	frame.setBounds(0, 0, 600, 300);
	frame.setTitle("Algoritmos Genéticos - Agendamento da Hidrelétrica");
	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	GridBagLayout gridBagLayout = new GridBagLayout();
	gridBagLayout.columnWidths = new int[]{0, 0};
	gridBagLayout.rowHeights = new int[]{0, 0};
	gridBagLayout.columnWeights = new double[]{1.0, Double.MIN_VALUE};
	gridBagLayout.rowWeights = new double[]{1.0, Double.MIN_VALUE};
	frame.getContentPane().setLayout(gridBagLayout);
	
	JTabbedPane tabPane = new JTabbedPane(JTabbedPane.TOP);
	GridBagConstraints gbc_tabbedPane = new GridBagConstraints();
	gbc_tabbedPane.fill = GridBagConstraints.BOTH;
	gbc_tabbedPane.gridx = 0;
	gbc_tabbedPane.gridy = 0;
	frame.getContentPane().add(tabPane, gbc_tabbedPane);
	frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
	
	ConfiguracoesPanel panelConf = new ConfiguracoesPanel();
	tabPane.addTab("Configurações", null, panelConf, null);
	
	
	JPanel panelRun = new ExecucaoPanel(panelConf);
	tabPane.addTab("Execução", null, panelRun, null);
	
	tabPane.setIconAt(0, new ImageIcon(AgApp.class.getResource("resources/dna.png")));
	tabPane.setIconAt(1, new ImageIcon(AgApp.class.getResource("resources/macaco.png")));
	
    }
    
    public enum TipoSelecao{
	Roleta("Roleta"), Torneio("Torneio");
	
	private String label;
	
	TipoSelecao(String label){
	    this.label = label;
	}
	
	@Override
	public String toString() {
	    return label;
	}
    }
}
