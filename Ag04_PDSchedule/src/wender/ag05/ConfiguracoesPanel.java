package wender.ag05;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import wender.ag05.AgApp.TipoSelecao;
import wender.ag05.populacao.ElitismoPopulacional;
import wender.ag05.populacao.ModuloPopulacional;
import wender.ag05.populacao.SubstituiPopulacao;
import wender.ag05.selecao.Roleta;
import wender.ag05.selecao.Selecao;
import wender.ag05.selecao.Torneio;

public class ConfiguracoesPanel extends JPanel implements AgConf {
    private static final long serialVersionUID = 1L;
    
    private JComboBox<TipoSelecao> cmbSelecao;
    private JSpinner spTxMutacao;
    private JSpinner spTamPopulacao;
    private JSpinner spElitismo;
    private JLabel lbMaxGeracoes;
    private JSpinner spLimiteGeracao;
    
    public ConfiguracoesPanel(){
	GridBagLayout gbl_this = new GridBagLayout();
	gbl_this.columnWidths = new int[]{0, 0, 0};
	gbl_this.rowHeights = new int[]{0, 0, 0, 0, 0, 0};
	gbl_this.columnWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
	gbl_this.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
	this.setLayout(gbl_this);
	
	JLabel lblSelecao = new JLabel("Seleção:");
	GridBagConstraints gbc_lblSelecao = new GridBagConstraints();
	gbc_lblSelecao.insets = new Insets(0, 0, 5, 5);
	gbc_lblSelecao.anchor = GridBagConstraints.EAST;
	gbc_lblSelecao.gridx = 0;
	gbc_lblSelecao.gridy = 0;
	this.add(lblSelecao, gbc_lblSelecao);
	
	cmbSelecao = new JComboBox<TipoSelecao>();
	GridBagConstraints gbc_cmbSelecao = new GridBagConstraints();
	gbc_cmbSelecao.insets = new Insets(0, 0, 5, 0);
	gbc_cmbSelecao.fill = GridBagConstraints.HORIZONTAL;
	gbc_cmbSelecao.gridx = 1;
	gbc_cmbSelecao.gridy = 0;
	this.add(cmbSelecao, gbc_cmbSelecao);
	
	cmbSelecao.addItem(TipoSelecao.Roleta);
	cmbSelecao.addItem(TipoSelecao.Torneio);
	
	JLabel lblTxMutacao = new JLabel("Mutação");
	GridBagConstraints gbc_lblTxMutacao = new GridBagConstraints();
	gbc_lblTxMutacao.anchor = GridBagConstraints.EAST;
	gbc_lblTxMutacao.insets = new Insets(0, 0, 5, 5);
	gbc_lblTxMutacao.gridx = 0;
	gbc_lblTxMutacao.gridy = 1;
	this.add(lblTxMutacao, gbc_lblTxMutacao);
	
	spTxMutacao = new JSpinner(new SpinnerNumberModel(0.01, 0, 1, 0.01));
	GridBagConstraints gbc_spTxMutacao = new GridBagConstraints();
	gbc_spTxMutacao.fill = GridBagConstraints.HORIZONTAL;
	gbc_spTxMutacao.insets = new Insets(0, 0, 5, 0);
	gbc_spTxMutacao.gridx = 1;
	gbc_spTxMutacao.gridy = 1;
	this.add(spTxMutacao, gbc_spTxMutacao);
	
	JLabel lbTamPopulacao = new JLabel("Tamanho da População:");
	GridBagConstraints gbc_lbTamPopulacao = new GridBagConstraints();
	gbc_lbTamPopulacao.anchor = GridBagConstraints.EAST;
	gbc_lbTamPopulacao.insets = new Insets(0, 0, 5, 5);
	gbc_lbTamPopulacao.gridx = 0;
	gbc_lbTamPopulacao.gridy = 2;
	this.add(lbTamPopulacao, gbc_lbTamPopulacao);
	
	spTamPopulacao = new JSpinner(new SpinnerNumberModel(10, 10, 100, 1));
	GridBagConstraints gbc_spTamPopulacao = new GridBagConstraints();
	gbc_spTamPopulacao.fill = GridBagConstraints.HORIZONTAL;
	gbc_spTamPopulacao.insets = new Insets(0, 0, 5, 0);
	gbc_spTamPopulacao.gridx = 1;
	gbc_spTamPopulacao.gridy = 2;
	this.add(spTamPopulacao, gbc_spTamPopulacao);
	
	lbMaxGeracoes = new JLabel("Limite de Gerações:");
	GridBagConstraints gbc_lbMaxGeracoes = new GridBagConstraints();
	gbc_lbMaxGeracoes.anchor = GridBagConstraints.EAST;
	gbc_lbMaxGeracoes.insets = new Insets(0, 0, 5, 5);
	gbc_lbMaxGeracoes.gridx = 0;
	gbc_lbMaxGeracoes.gridy = 3;
	add(lbMaxGeracoes, gbc_lbMaxGeracoes);
	
	spLimiteGeracao = new JSpinner(new SpinnerNumberModel(5, 1, 1000, 1));
	GridBagConstraints gbc_spinner = new GridBagConstraints();
	gbc_spinner.fill = GridBagConstraints.HORIZONTAL;
	gbc_spinner.insets = new Insets(0, 0, 5, 0);
	gbc_spinner.gridx = 1;
	gbc_spinner.gridy = 3;
	add(spLimiteGeracao, gbc_spinner);
	
	JLabel lblElitismo = new JLabel("Elitismo:");
	GridBagConstraints gbc_lblElitismo = new GridBagConstraints();
	gbc_lblElitismo.anchor = GridBagConstraints.EAST;
	gbc_lblElitismo.insets = new Insets(0, 0, 0, 5);
	gbc_lblElitismo.gridx = 0;
	gbc_lblElitismo.gridy = 4;
	this.add(lblElitismo, gbc_lblElitismo);
	
	spElitismo = new JSpinner(new SpinnerNumberModel(2, 0, 100, 1));
	GridBagConstraints gbc_spElitismo = new GridBagConstraints();
	gbc_spElitismo.fill = GridBagConstraints.HORIZONTAL;
	gbc_spElitismo.gridx = 1;
	gbc_spElitismo.gridy = 4;
	this.add(spElitismo, gbc_spElitismo);
	
	final ChangeListener elitismoXPopulacaoListener = new ChangeListener() {
	    
	    @Override
	    public void stateChanged(ChangeEvent e) {
		int qtdElitismo = (int) spElitismo.getValue();
		int qtdPopulacao =  (int)  spTamPopulacao.getValue();
		
		
		if(qtdElitismo >= qtdPopulacao){
		    spElitismo.setValue(qtdPopulacao -1);
		}
	    }
	};
	
	spElitismo.addChangeListener(elitismoXPopulacaoListener);
	spTamPopulacao.addChangeListener(elitismoXPopulacaoListener);
    }
    
    @Override
    public int getLimiteGeracao(){
	return Integer.parseInt(spLimiteGeracao.getValue().toString());
    }
    
    @Override
    public Selecao getSelecao() {
	TipoSelecao selecionado = (TipoSelecao) cmbSelecao.getSelectedItem(); 
	
	if(selecionado == TipoSelecao.Roleta){
	    return new Roleta();
	}else{
	    return new Torneio();
	}
    }

    @Override
    public double getTaxaMutacao() {
	return (Double) spTxMutacao.getValue() ;
    }

    @Override
    public ModuloPopulacional getModuloPopulacional() {
	int individuosElite = (Integer) spElitismo.getValue();
	
	if(individuosElite == 0){
	    return new SubstituiPopulacao();
	}else{
	    return new ElitismoPopulacional(individuosElite);
	}
	    
    }

    @Override
    public int getTamanhoPopulacao() {
	return (Integer) spTamPopulacao.getValue();
    }
}
