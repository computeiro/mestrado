package wender.ag05;

import wender.ag05.populacao.ElitismoPopulacional;
import wender.ag05.populacao.ModuloPopulacional;
import wender.ag05.selecao.Roleta;
import wender.ag05.selecao.Selecao;

public class AgConfImpl implements AgConf {
    private Selecao selecao;
    private double taxaMutacao;
    private ModuloPopulacional modPop;
    private int tamPop;
    private int limGeracao;
    
    public static void main(String[] args) {
	Cariotipo car = new Cariotipo(new AgConfImpl());
	car.geracao(null);
    }
    
    public AgConfImpl(){
	this.selecao = new Roleta();
	this.taxaMutacao = 0.01;
	this.limGeracao = 15000;
	this.modPop = new ElitismoPopulacional(3);
	this.tamPop = 600;
    }
    
    
    @Override
    public Selecao getSelecao() {
	return selecao;
    }

    @Override
    public double getTaxaMutacao() {
	return taxaMutacao;
    }

    @Override
    public ModuloPopulacional getModuloPopulacional() {
	return modPop;
    }

    @Override
    public int getTamanhoPopulacao() {
	return tamPop;
    }

    @Override
    public int getLimiteGeracao() {
	return limGeracao;
    }

}
