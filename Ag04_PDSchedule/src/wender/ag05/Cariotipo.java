package wender.ag05;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import wender.ag05.populacao.PopulacaChangeListner;
import wender.ag05.selecao.CromossomoComparator;
import wender.ag05.selecao.Selecao;

public class Cariotipo {
    private AgConf conf;
    private List<Cromossomo> populacao;
    private List<Cromossomo> novaPopulacao;

    public Cariotipo(AgConf conf) {
	this.conf = conf;
    }

    public void inicializaPopulacao() {
	populacao = new ArrayList<>();

	for (int i = 0; i < conf.getTamanhoPopulacao(); i++) {
	    populacao.add(Cromossomo.gerarAleatorio());
	}
    }

    public void geracao(PopulacaChangeListner listener) {
	if (populacao == null) {
	    inicializaPopulacao();
	}

	Selecao sel = conf.getSelecao();
	sel.init(populacao);
	
	
	for (int g = 0; g < conf.getLimiteGeracao(); g++) {
	    novaPopulacao = new ArrayList<Cromossomo>();

	    for (int c = 0; c < populacao.size(); c++) {
		Cromossomo c1 = sel.get();
		Cromossomo c2 = sel.get();
		Cromossomo f = c1.crossOverUmPonto(c2);
		f.mutacao(conf.getTaxaMutacao());
		novaPopulacao.add(f);
	    }

	    populacao = conf.getModuloPopulacional().atualizaPopulacao(populacao, novaPopulacao);

	    Collections.sort(populacao, new CromossomoComparator());
	    
	    System.out.println("\n\nGeração: " + g);

	    Cromossomo ultimo = populacao.get(populacao.size() - 1);
	    System.out.println("\nUltimo:");
	    System.out.println(ultimo.dump());
	    
	    Cromossomo primeiro = populacao.get(0);
	    System.out.println("\nPrimeiro da geração:");
	    System.out.println(primeiro.dump());
	    
	    if(primeiro.isSatisfaz() && primeiro.isValido()){
		break;
	    }
	}

    }
    
    public static void main(String[] args) {
    }
}
