package wender.ag05;

import wender.ag05.populacao.ModuloPopulacional;
import wender.ag05.selecao.Selecao;

public interface AgConf {
    Selecao getSelecao();
    double getTaxaMutacao();
    ModuloPopulacional getModuloPopulacional();
    int getTamanhoPopulacao();
    int getLimiteGeracao();
}
