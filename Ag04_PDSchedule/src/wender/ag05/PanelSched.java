package wender.ag05;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import java.awt.Font;

public class PanelSched extends JPanel {
    ImageIcon IMG_OK = new ImageIcon(this.getClass().getResource("resources/ok.png"));
    ImageIcon IMG_MANUTENCAO = new ImageIcon(this.getClass().getResource("resources/cancel.png"));

    public static void main(String[] args) {
	JFrame frame = new JFrame();
	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	int[] value = new int[] { 
		0, 0, 0, 0, 0, 0, 0, 
		0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 1, 0, 0, 0, 
		0, 0, 0, 0, 0, 0, 1
	};

	Cromossomo c = new Cromossomo(value);

	frame.setContentPane(new PanelSched(c));
	frame.setSize(600, 300);
	frame.setVisible(true);

    }

    public PanelSched(Cromossomo cromossomo) {
	Font fontLabel = new Font("Tahoma", Font.BOLD, 11);

	GridBagLayout gridBagLayout = new GridBagLayout();
	gridBagLayout.columnWidths = new int[] { 0, 0, 0, 0 };
	gridBagLayout.rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
	gridBagLayout.columnWeights = new double[] { 0.0, 0.0, 0.0, Double.MIN_VALUE };
	gridBagLayout.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
	setLayout(gridBagLayout);

	JLabel lblPerodos = new JLabel("Períodos");
	lblPerodos.setFont(fontLabel);
	GridBagConstraints gbc_lblPerodos = new GridBagConstraints();
	gbc_lblPerodos.anchor = GridBagConstraints.WEST;
	gbc_lblPerodos.insets = new Insets(0, 0, 5, 5);
	gbc_lblPerodos.gridx = 0;
	gbc_lblPerodos.gridy = 0;
	add(lblPerodos, gbc_lblPerodos);

	for (int p = 0; p < Cromossomo.PD_PERIODO.length; p++) {
	    JLabel label = new JLabel(String.valueOf(p +1));
	    label.setFont(fontLabel);
	    GridBagConstraints layout = new GridBagConstraints();
	    layout.insets = new Insets(0, 0, 0, 5);
	    layout.gridx = 0;
	    layout.gridy = p + 1;
	    add(label, layout);
	}
	
	for(int v = 0; v < cromossomo.getValue().length; v++){
	    if(v % Cromossomo.MANUTENCAO_TURBINAS.length == 0){
		JLabel label = new JLabel("T " + (v +1) );
		label.setFont(fontLabel);
		GridBagConstraints layout = new GridBagConstraints();
		layout.insets = new Insets(0, 0, 0, 5);
		layout.gridx = v / Cromossomo.MANUTENCAO_TURBINAS.length + 1;
		layout.gridy = 0;
		add(label, layout);
	    }else{
		/*ImageIcon img = IMG_OK;
		
		if(cromossomo.getValue()[v] == Cromossomo.EM_MANUTENCAO){
		    img = IMG_MANUTENCAO;
		}
		
		JLabel label = new JLabel("", img, JLabel.CENTER);
		label.setFont(fontLabel);
		GridBagConstraints layout = new GridBagConstraints();
		layout.insets = new Insets(0, 0, 0, 5);
		layout.gridx = v % Cromossomo.MANUTENCAO_TURBINAS.length + 1;
		layout.gridy = v / Cromossomo.MANUTENCAO_TURBINAS.length + 1 ;
		add(label, layout);*/
	    }
	}
    }
}
