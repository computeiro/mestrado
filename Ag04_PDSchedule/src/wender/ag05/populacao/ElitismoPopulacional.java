package wender.ag05.populacao;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import wender.ag05.Cromossomo;
import wender.ag05.selecao.CromossomoComparator;

public class ElitismoPopulacional implements ModuloPopulacional {
    private int individuosElite;
    private Comparator<Cromossomo> comparator;
    
    @Override
    public List<Cromossomo> atualizaPopulacao(List<Cromossomo> antiga, List<Cromossomo> nova) {
	
	if(individuosElite > 0){
	
	    Collections.sort(antiga, comparator);
	    Collections.sort(nova, comparator);
	    
	    int size = nova.size();

	    for (int i = 0; i < individuosElite; i++) {
		nova.set(i, antiga.get(size - i - 1));
	    }
	}
	
	return nova;
    }
    
    public ElitismoPopulacional(int individuosElite){
	this.individuosElite = individuosElite;
	this.comparator = new CromossomoComparator();
    }
}
