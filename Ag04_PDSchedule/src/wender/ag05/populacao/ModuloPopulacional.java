package wender.ag05.populacao;

import java.util.List;

import wender.ag05.Cromossomo;

public interface ModuloPopulacional {
    List<Cromossomo> atualizaPopulacao(List<Cromossomo> antiga, List<Cromossomo> nova);
}
