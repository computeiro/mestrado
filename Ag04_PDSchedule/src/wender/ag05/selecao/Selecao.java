package wender.ag05.selecao;

import java.util.List;

import wender.ag05.Cromossomo;

public interface Selecao {
    void init(List<Cromossomo> populacao);
    Cromossomo get();
}
