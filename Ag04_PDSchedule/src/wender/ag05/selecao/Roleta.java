package wender.ag05.selecao;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import wender.ag05.Cromossomo;

public class Roleta implements Selecao {
    private List<Cromossomo> populacao;
    private double somaAvaliacoes;
    private Comparator<Cromossomo> comparator;
    
    public Roleta(){
	comparator = new CromossomoComparator();
    }
    

    @Override
    public void init(List<Cromossomo> populacao) {
	this.populacao = populacao;
	somaAvaliacoes = 0;
	
	for(Cromossomo c : populacao){
	    somaAvaliacoes += c.getAvaliacao();
	}
	
	Collections.sort(this.populacao, comparator);
    }

    @Override
    public Cromossomo get() {
	double limite = Math.random() * Math.abs( this.somaAvaliacoes );
	double aux = 0;
	int i = 0;
	
	for( ; (i < populacao.size() && aux < limite); i++){
	    aux += populacao.get(i).getAvaliacao();
	}
	
	i--;
	return populacao.get(i);
    }

}
