package wender.ag05.selecao;

import java.util.List;

import wender.ag05.Cromossomo;

public class Torneio implements Selecao {
    private List<Cromossomo> populacao;
    private int maxRounds = 5;
    
    @Override
    public void init(List<Cromossomo> populacao) {
	this.populacao = populacao;
    }

    @Override
    public Cromossomo get() {
	
	int round = 1;
	Cromossomo escolhido = sorteiaOponente();
	Cromossomo oponente;
	
	while(round < maxRounds){
	    oponente = sorteiaOponente();
	    
	    if(oponente.getAvaliacao() < escolhido.getAvaliacao()){
		escolhido = oponente;
	    }
	    
	    round ++;
	}

	return escolhido;
    }
    
    private Cromossomo sorteiaOponente(){
	return populacao.get((int) Math.random()); 
    }

}
