package wender.ag05.selecao;

import java.util.Comparator;

import wender.ag05.Cromossomo;

public class CromossomoComparator implements Comparator<Cromossomo> {

    @Override
    public int compare(Cromossomo o1, Cromossomo o2) {
	double a1 = o1.getAvaliacao();
	double a2 = o2.getAvaliacao();

	if (a1 > a2) {
	    return -1;
	}

	if (a1 < a2) {
	    return 1;
	}

	return 0;
    }

}
